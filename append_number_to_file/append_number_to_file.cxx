// Adding iteration numbers to file names to prevent accidental
// overwriting.
//
// Shayan Hoshyari

#include <iostream>
#include <sstream>
#include <fstream>

// =======================================================
//
//                   Function declarations
//
// =======================================================

// Given a file name such as out.dat, this function will return a
// number [i], where [i] is the smallest integer such that the file
// out.[i].dat does not exist on the disk.
int IO_latest_number_on_disk_for_filename(const std::string fname);

// Given a file name out.dat or out.[j].dat, and the number [no],
// returns the file name out.[no].dat.
std::string IO_enumerate_filename(const std::string fname, const int no);

// Given a filename out.dat returns f1=out no=-1 f2=dat
// Given a filename out.i.dat returns f1=out no=i f2=dat
void IO_butcher_filename(const std::string fname, std::string &f1, int &no, std::string &f2);

// Given a filename out.dat returns -1
// Given a filename out.i.dat returns i
int IO_butcher_filename(const std::string fname);

// Given f1=out no=i and f2=dat return out.i.dat
std::string IO_glue_filename(const std::string f1, const int no, const std::string f2);

// =======================================================
//
//                   Function definitions
//
// =======================================================


void
IO_butcher_filename(const std::string fname, std::string &f1, int &no, std::string &f2)
{
  // Find a dot
  std::size_t dot_id = fname.find(".");

  // No dot found
  if (dot_id == std::string::npos)
    {
      f1 = fname;
      no = -1;
      f2 = "";
      return;
    }

  // Dot found read number after dot.
  int digit = -1;
  {
    // Try to read the digit
    std::stringstream ss(fname.substr(dot_id + 1));
    ss >> digit;
    // make sure do not fail
    if (ss.fail())
      digit = -1;
  }

  // No digit found
  if (digit < 0)
    {
      // print 'bad %s is not a number' % candid`
      f1 = fname.substr(0, dot_id);
      f2 = fname.substr(dot_id + 1);
      no = -1;
      return;
    }

  // Everything does exist and is fine.
  {
    no = digit;
    f1 = fname.substr(0, dot_id);
    std::string noandf2 = fname.substr(dot_id+1);
    std::size_t dot_id2 = noandf2.find(".");

    // std::cout << "***: " << noandf2 << std::endl;
    // std::cout << "***: " << dot_id2 << std::endl;

    // clang-format off
    if( dot_id2 == std::string::npos )  f2 = "";
    else                           f2 = noandf2.substr(dot_id2+1);
    // clang-format on
  }
}

int
IO_butcher_filename(const std::string fname)
{
  std::string f1, f2;
  int no;
  IO_butcher_filename(fname, f1, no, f2);
  return no;
}

std::string
IO_glue_filename(const std::string f1, const int no, const std::string f2)
{
  std::stringstream ss;
  ss << f1;
  // clang-format off
  if( no < 0 )  ss << ".0";
  else          ss << "." << no;
  if (f2 != "") ss << "." << f2;
  // clang-format on
  return ss.str();
}

std::string
IO_enumerate_filename(const std::string fname, const int no)
{
  std::string f1, f2;
  int currentno;
  IO_butcher_filename(fname, f1, currentno, f2);
  return IO_glue_filename(f1, no, f2);
}

static bool
does_file_exist(const std::string &fname)
{
  std::ifstream f(fname.c_str());
  return f.good();
}


int
IO_latest_number_on_disk_for_filename(const std::string fname)
{
  std::string f1, f2;
  int no;
  IO_butcher_filename(fname, f1, no, f2);

  if (no < 0) no = 0;
  while (true)
    {
      std::cout << "*****: " << IO_glue_filename(f1, no, f2) << "\n";
      if (!does_file_exist(IO_glue_filename(f1, no, f2)))
	return no;
      ++no;
    }
}


// =======================================================
//
//                   Test Code
//
// =======================================================


void test_butcher(std::string fname)
{
  std::string f1, f2;
  int no;
  
  IO_butcher_filename(fname,f1, no, f2);
  std::cout << f1 << " " << no << " " << f2 << "\n";
}

void test_enumerate(std::string fname)
{
  std::cout << IO_enumerate_filename(fname, 123) << "\n";
}

void test_latest_on_disk(std::string fname)
{
  std::cout << IO_latest_number_on_disk_for_filename(fname) << "\n";
}

int main()
{
  

  ///
  test_butcher("The_handle_of_axe.123123.99999.The_sky_is_nice");
  test_butcher("The_handle_of_axe.");
  test_butcher("The_handle_of_axe.1");
  test_butcher("The_handle_of_axe");
  test_butcher(".The_handle_of_axe");
  std::cout << "\n";

  ///
  test_enumerate("The_handle_of_axe.123123.99999.The_sky_is_nice");
  test_enumerate("The_handle_of_axe.");
  test_enumerate("The_handle_of_axe.1");
  test_enumerate("The_handle_of_axe");
  test_enumerate(".The_handle_of_axe");
  std::cout << "\n";

  ///
  test_latest_on_disk("molask");
  test_latest_on_disk("molask.3.jpg");
  test_latest_on_disk("molask.jpg");
  

  return 0;
}
