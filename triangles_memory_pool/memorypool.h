#define VOID int

/* Set correct size for pointer alignment calculations */
#if defined(_M_X64) || defined(__amd64__)
#define ULONG_PTR unsigned long long
#else
#define ULONG_PTR unsigned long
#endif

#define REAL double

/* A type used to allocate memory.  firstblock is the first block of items.  */
/*   nowblock is the block from which items are currently being allocated.   */
/*   nextitem points to the next slab of free memory for an item.            */
/*   deaditemstack is the head of a linked list (stack) of deallocated items */
/*   that can be recycled.  unallocateditems is the number of items that     */
/*   remain to be allocated from nowblock.                                   */
/*                                                                           */
/* Traversal is the process of walking through the entire list of items, and */
/*   is separate from allocation.  Note that a traversal will visit items on */
/*   the "deaditemstack" stack as well as live items.  pathblock points to   */
/*   the block currently being traversed.  pathitem points to the next item  */
/*   to be traversed.  pathitemsleft is the number of items that remain to   */
/*   be traversed in pathblock.                                              */
/*                                                                           */
/* alignbytes determines how new records should be aligned in memory.        */
/*   itembytes is the length of a record in bytes (after rounding up).       */
/*   itemsperblock is the number of items allocated at once in a single      */
/*   block.  itemsfirstblock is the number of items in the first block,      */
/*   which can vary from the others.  items is the number of currently       */
/*   allocated items.  maxitems is the maximum number of items that have     */
/*   been allocated at once; it is the current number of items plus the      */
/*   number of records kept on deaditemstack.                                */

// Shayan's notes:
struct memorypool
{
  VOID **firstblock, **nowblock;
  VOID *nextitem;
  
  // Since dead objects are well *dead*! The first sizeof(void*) bytes of them
  // is used to construct a deadobject stack.
  VOID *deaditemstack;
  
  VOID **pathblock;
  VOID *pathitem;
  int alignbytes;
  int itembytes;
  int itemsperblock;
  int itemsfirstblock;
  long items, maxitems;
  int unallocateditems;
  int pathitemsleft;
};

/********* Memory management routines begin here                     *********/
/**                                                                         **/

/**
 * Set all of a pool's fields to zero.
 */
void poolzero (struct memorypool *pool);

/**
 * Deallocate all items in a pool.
 */
void poolrestart (struct memorypool *pool);

/**
 * Initialize a pool of memory for allocation of items.
 */
void poolinit (struct memorypool *pool, int bytecount, int itemcount,
               int firstitemcount, int alignment);

/**
 * Free to the operating system all memory taken by a pool.
 */
void pooldeinit (struct memorypool *pool);

/**
 * Allocate space for an item.
 */
VOID *poolalloc (struct memorypool *pool);

/**
 * Deallocate space for an item.
 */
void pooldealloc (struct memorypool *pool, VOID *dyingitem);

/**
 * Prepare to traverse the entire list of items.
 */
void traversalinit (struct memorypool *pool);

/**
 * Find the next item in the list.
 */
VOID *traverse (struct memorypool *pool);


/**                                                                         **/
/********* Memory management routines end here                       *********/

