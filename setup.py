from setuptools import setup, find_packages

setup(name='molly',
	version = "0.1.0",
	packages = find_packages(include = [ "molly", "molly.*" ]),

	author = "Shayan Hoshyari",
	description = "",

	url="",

	project_urls=dict(),

	license = "PRIVATE",

	classifiers=[
	],

	python_requires=">=3.7",


	entry_points = {
		'console_scripts': [
			# 'banach=Banach.Scripts.main:main_cmd',
		]
	},

)
