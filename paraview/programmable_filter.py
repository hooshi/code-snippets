#from __future__ import print_function

import paraview.simple as pvsim

import sys

def method1():
  # 4
  # https://kitware.github.io/paraview-docs/latest/python/paraview.simple.ProgrammableSource.html
  # https://lorensen.github.io/VTKExamples/site/Python/CompositeData/MultiBlockDataSet/
  pvsim.ProgrammableSource(
    OutputDataSetType='vtkMultiblockDataSet',
    Script="""
  import logging
  logging.basicConfig(filename='cheat.txt', level=logging.DEBUG)
  logging.error(str(dir(self)))
  logging.error(str(self.GetOutputDataSetType()))
  OutputPort = self.GetOutput()
  logging.error(str(OutputPort))
    """
  )

  pvsim.ProgrammableSource(
    OutputDataSetType='vtkMultiblockDataSet',
    Script="""
  import vtk
  colors = vtk.vtkNamedColors()

  # PART 1 Make some Data.
  # Make a tree.
  root = self.GetOutput()

  branch = vtk.vtkMultiBlockDataSet()
  root.SetBlock(0, branch)

  # Make some leaves.
  leaf1 = vtk.vtkSphereSource()
  leaf1.SetCenter(0, 0, 0)
  leaf1.Update()
  branch.SetBlock(0, leaf1.GetOutput())
  branch.GetMetaData(0).Set( vtk.vtkCompositeDataSet.NAME(), 'Block0' ) 

  leaf2 = vtk.vtkSphereSource()
  leaf2.SetCenter(1.75, 2.5, 0)
  leaf2.SetRadius(1.5)
  leaf2.Update()
  branch.SetBlock(1, leaf2.GetOutput())
  branch.GetMetaData(1).Set( vtk.vtkCompositeDataSet.NAME(), 'Block1' ) 

  leaf3 = vtk.vtkSphereSource()
  leaf3.SetCenter(4, 0, 0)
  leaf3.SetRadius(2)
  leaf3.Update()
  root.SetBlock(1, leaf3.GetOutput())
  root.GetMetaData(1).Set( vtk.vtkCompositeDataSet.NAME(), 'Block1_' ) 
  """
  )
  # extract blocks does the job

def method2():
  a = pvsim.ProgrammableSource()
  pvsim.ProgrammableFilter(
  Input=a,
  OutputDataSetType='vtkMultiblockDataSet',
Script="""
  import vtk
  colors = vtk.vtkNamedColors()

  # PART 1 Make some Data.
  # Make a tree.
  root = self.GetOutput()

  branch = vtk.vtkMultiBlockDataSet()
  root.SetBlock(0, branch)

  # Make some leaves.
  leaf1 = vtk.vtkSphereSource()
  leaf1.SetCenter(0, 0, 0)
  leaf1.Update()
  branch.SetBlock(0, leaf1.GetOutput())
  branch.GetMetaData(0).Set( vtk.vtkCompositeDataSet.NAME(), 'Block0' ) 

  leaf2 = vtk.vtkSphereSource()
  leaf2.SetCenter(1.75, 2.5, 0)
  leaf2.SetRadius(1.5)
  leaf2.Update()
  branch.SetBlock(1, leaf2.GetOutput())
  branch.GetMetaData(1).Set( vtk.vtkCompositeDataSet.NAME(), 'Block1' ) 

  leaf3 = vtk.vtkSphereSource()
  leaf3.SetCenter(4, 0, 0)
  leaf3.SetRadius(2)
  leaf3.Update()
  root.SetBlock(1, leaf3.GetOutput())
  root.GetMetaData(1).Set( vtk.vtkCompositeDataSet.NAME(), 'Block1_' ) 
  """
)

# if len(sys.argv)==0:
#   method2()
# else:
#   method1()
