#
# Load the barTender animations in Paraview.  Or just generate
# videos without launching the Paraview GUI, using the python
# interface (easier to use the python shipped with Paraview --
# pvpython).
#
# Works with all MSYS+bash, Linux bash, and the garbage couple
# cmd/powershell.
#
# 1) Add the bin directory of Paraview to your path.
# 2) Set the environment var MEDITDEV to point to bin directory of MECHEDIT.
# 3) Modify the end of this script to your liking.
# 4) Within dancingRobot's root directory run
#    pvpython scripts/generate_animation.py
# 4') Or launch paraview from barTender's root directory from cmd or
#     bash, then from paraview's internal python script, click on run script,
#     and then click  on  scripts/generate_animation.py.
#
# Author: Shayan Hoshyari
#

import os
import glob
import numpy as np

# regular expression for settings properties as group
import re

# My dear friend paraview!
from paraview.simple import *
import paraview.servermanager 

# =================================================================
#    PREDEFINED CAMERA POSITIONS
# =================================================================
ANGLES = {}

# A FAIRLY GOOD VIEW, ADD MORE BELOW
ANGLES[0] = dict(
CameraPosition = [0.9515916286174316, 0.1426314176908292, -1.0045252341763398],
CameraFocalPoint = [0.04180975323709633, 0.13574231309888724, -0.03602918315795703],
CameraViewUp= [-0.008205868304457119, 0.9999661540170235, -0.00059543746318993],
CameraParallelScale= 0.343921248187,
ViewSize= [805, 541],
)

# =================================================================
#    UTILITY
# =================================================================
def remove_all():
    previous_sources = GetSources().keys()
    for k in previous_sources:
        print ("Deleting {}".format(k))
        proxy = FindSource(k[0])
        try:    Delete(proxy)
        except: pass

def print_camera():
    renderView1 = GetActiveViewOrCreate('RenderView')
    print('dict(')
    print('CameraPosition = {},'.format(renderView1.CameraPosition))
    print('CameraFocalPoint = {},'.format(renderView1.CameraFocalPoint))
    print('CameraViewUp= {},'.format(renderView1.CameraViewUp))
    print('CameraParallelScale= {},'.format(renderView1.CameraParallelScale))
    print('ViewSize= {},'.format(renderView1.ViewSize))
    print(')')

    
def set_camera(camera):
    render_view = GetActiveViewOrCreate('RenderView')
    if(camera is None):
        render_view.ResetCamera()
        return
    render_view.CameraPosition = camera['CameraPosition']
    render_view.CameraFocalPoint = camera['CameraFocalPoint']
    render_view.CameraViewUp= camera['CameraViewUp']
    render_view.CameraParallelScale= camera['CameraParallelScale']
    if('ViewSize' in camera): render_view.ViewSize= camera['ViewSize']

def set_source_properties(source_name, **options):
    render_view = GetActiveViewOrCreate('RenderView')
    proxy = FindSource(source_name)
    proxy_display = Show(proxy, render_view)
    if('RepresentationType' in options):
        proxy_display.SetRepresentationType(options['RepresentationType'])
    if('DiffuseColor' in options):
        proxy_display.DiffuseColor = options['DiffuseColor']
    if('Opacity' in options):
        proxy_display.Opacity = options['Opacity']

def set_source_properties_group(name_pattern, **options):
    source_names = GetSources().keys()
    for (source_name, _) in source_names:
        if re.search(name_pattern, source_name):
            set_source_properties(source_name, **options)


def set_animation_length(val):
    animationScene = GetAnimationScene()
    animationScene.PlayMode = 'Real Time'
    animationScene.Duration = val

def save_animation(fname, frame_rate, length):
    set_animation_length(int(length))
    UpdatePipeline() 
    render_view = GetActiveViewOrCreate('RenderView')
    nx=render_view.ViewSize[0]
    ny=render_view.ViewSize[1]
    SaveAnimation(fname,FrameRate=frame_rate, FrameWindow=[0, int(frame_rate*length)], ImageResolution =[2*nx, 2*ny])


def generate_vtk(inFileName, output='__prv', stride=40):
    if not os.path.exists(output):
        os.makedirs(output)
    command = "%s/multiParaviewViz tmp/barTender.config %s %d %s > garbage.txt "%(os.environ["MEDITDEV"], inFileName, stride, output)
    print(command)
    os.system(command)

def load_vtk(filename, out_name, total_time=16):
    files = glob.glob('{}-*.vtk'.format(filename))
    files.sort() # This took me a while to realize. :( On windows it is already sorted.
    input_mesh = LegacyVTKReader(FileNames=files)
    #tvals = np.linspace(0,float(total_time),len(files))
    #input_mesh.SetPropertyWithName('TimestepValues',tvals)
    render_view = GetActiveViewOrCreate('RenderView')
    RenameSource(out_name, input_mesh)
    Show()
    return input_mesh

def load_base(pathToBase, prefix):
    base_mesh = OpenDataFile(pathToBase)
    RenameSource('{}-base'.format(prefix), base_mesh)
    Show()
    return base_mesh
    
def load_animation(inFolder, namePrefix):
    objects = []
    objects.append( load_vtk('{}/deformable-0'.format(inFolder), '{}-lowerArm'.format(namePrefix)) )
    objects.append( load_vtk('{}/deformable-1'.format(inFolder), '{}-upperArm'.format(namePrefix)) )

    objects.append( load_vtk('{}/rigid-0'.format(inFolder), '{}-cup'.format(namePrefix)) )
    objects.append( load_vtk('{}/rigid-1'.format(inFolder), '{}-wristMotor'.format(namePrefix)) )
    objects.append( load_vtk('{}/rigid-2'.format(inFolder), '{}-elbowMotor'.format(namePrefix)) )
    objects.append( load_vtk('{}/rigid-3'.format(inFolder), '{}-elbowConnector'.format(namePrefix)) )
    objects.append( load_vtk('{}/rigid-4'.format(inFolder), '{}-shoulderConnector'.format(namePrefix)) )
    objects.append( load_vtk('{}/rigid-5'.format(inFolder), '{}-shoulderMotor'.format(namePrefix)) )
    
    objects.append( load_base('base/base.obj', namePrefix) )

    
    # Colors ....

    #HAND 
    set_source_properties_group('{}-shoulderConnector'.format(namePrefix),DiffuseColor=[1,0,0.56])

    set_source_properties_group('{}-wristMotor'.format(namePrefix),DiffuseColor=[0.7,0.1,0.1])
    set_source_properties_group('{}-elbowMotor'.format(namePrefix),DiffuseColor=[0.1,0.1,0.7])
    set_source_properties_group('{}-elbowConnector'.format(namePrefix),DiffuseColor=[0.7,0.1,0.7])
    set_source_properties('{}-shoulderMotor'.format(namePrefix),DiffuseColor=[0.5,0.5,0.9])
    set_source_properties('{}-cup'.format(namePrefix),DiffuseColor=[0.9,0.9,0.9])
    
    
    # 3-D printed
    set_source_properties_group('{}-(lower|upper)Arm'.format(namePrefix),DiffuseColor=[0.1,0.7,0.2])

    # BASE
    set_source_properties('{}-base'.format(namePrefix),DiffuseColor=[0.35,0.35,0.35])

    Render()
    
    return objects

# =======================================================
#         DIRTY WORK
# =======================================================
def process_case(case, group_name='RESULT', gen_vtk=True, save_anim=True,  length=9):
    if gen_vtk:
        generate_vtk(case+'.u', case+'_paraview', stride=40)
    load_animation(case+'_paraview',group_name)
    if save_anim:
        for viewid in range(len(ANGLES.keys())):
            set_camera(ANGLES[viewid])
            Render()
            save_animation('{}_{}.avi'.format(case, viewid), frame_rate=50, length=length)

remove_all()

#case='D:/Models/mechEdit/DJRobot/resultsFinal/2018_12_14_investigateReferenceAndInitCon/sampleOptimized'
case='results_dump/2019_01_10_refAndNoOpt/noopt_nosmooth'
#generate_vtk(case+'.u', case+'_paraview', stride=40)
#load_animation(case+'_paraview','sampleOpt')
process_case(case, group_name='RESULT', gen_vtk=True, save_anim=True,  length=8)
