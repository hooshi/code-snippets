import logging
import logging.handlers
from functools import lru_cache

class HashHelper:
    def __init__(self, v):
        self.hash = id(v)
    def __hash__(self):
        return self.hash

class MollyLogger(logging.Logger):
    VERBOSE_DEBUG = logging.DEBUG - 1
    WORKERPROC = logging.ERROR + 2
    TESTRUNNER = logging.ERROR + 3

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def make_root(self):
        self.handlers.clear()
        self.setLevel(1) # set something small
        self.console_handler : logging.StreamHandler = logging.StreamHandler()
        self.addHandler(self.console_handler)
        self.console_handler.setLevel("DEBUG")
        # self.console_handler = ConsoleHandler()
        # self.renderable_state : RenderableState = None
        # self.renderable_state = RenderableState(self.console_handler)

    def verbose_debug(self, *args, **kwargs):
        self.log(self.VERBOSE_DEBUG, *args, **kwargs)

    def add_log_file(self, path, level="DEBUG", rotating=False):
        if rotating:
            file_handler = logging.handlers.RotatingFileHandler(
                filename = path,
                mode='a', # nothing other than this is supported.
                maxBytes=1e8,
                backupCount=5,
            )
        else:
            file_handler = logging.FileHandler(filename=path, mode='w')
        file_handler.setFormatter(logging.Formatter("[%(name)s.%(levelname)s.%(asctime)s] %(message)s", "%y/%d/%m-%H:%M:%S"))
        file_handler.setLevel(level)
        logger.addHandler(file_handler)
        return file_handler

    def hash(self, v):
        return HashHelper(v)

    # be careful any call to this will be remained and cached
    # in memory (up to 50 most recent calls)
    @lru_cache(50)
    def info_once(self, *args, **kwargs):
        args = [a for a in args if not isinstance(a, HashHelper)]
        self.info(*args, **kwargs)

    @lru_cache(50)
    def warn_once(self, *args, **kwargs):
        args = [a for a in args if not isinstance(a, HashHelper)]
        self.warning(*args, **kwargs)

    @lru_cache(50)
    def error_once(self, *args, **kwargs):
        args = [a for a in args if not isinstance(a, HashHelper)]
        self.error(*args, **kwargs)
        
    def capture_warnings(self):
        logging.captureWarnings(True)
        logging.getLogger('py.warnings').parent = self


logging.setLoggerClass(MollyLogger)
logger : MollyLogger = logging.getLogger('molly')
logger.make_root()
logging.addLevelName(logger.VERBOSE_DEBUG, "VERBOSE_DEBUG")
logging.addLevelName(logger.WORKERPROC, "WORKERPROC")
logging.addLevelName(logger.TESTRUNNER, "TESTRUNNER")


class ShimHandler(logging.Handler):
    def __init__(self, parent : logging.Logger, lvl):
        super().__init__()
        self.parent = parent
        self.lvl = lvl

    def emit(self, record):
        self.parent.log(self.lvl, record.msg, *record.args)
