#!/usr/bin/env python3

from PyQt5 import QtWidgets

class ButtonListWidget(QtWidgets.QWidget):
    """ A panel where you can dump a bunch of buttons dynamically"""
    def __init__(self, parent=None, is_vertical=True):
        super().__init__(parent)
        
        # Add layout
        if(is_vertical): self.setLayout(  QtWidgets.QVBoxLayout() )
        else           : self.setLayout(  QtWidgets.QHBoxLayout() )

        # buttons
        self._buttons = {}
        self._radios = {}
        self._lists = {}
        self._labels = {}

    def add_button(self, handle, label):
        assert( not handle in self._buttons )
        self._buttons[handle] = b = QtWidgets.QPushButton(parent=None)
        b.setText(label)
        self.layout().addWidget( b )
        return b
    
    def add_radio(self, handle, label):
        assert( not handle in self._radios )
        self._radios[handle] = b = QtWidgets.QRadioButton(parent=None)
        b.setText(label)
        self.layout().addWidget( b )
        return b

    def add_list(self, handle):
        assert( not handle in self._lists )
        self._lists[handle] = b = QtWidgets.QListWidget(parent=None)
        self.layout().addWidget( b )
        return b

    def add_label(self, handle, text):
        assert( not handle in self._labels )
        self._labels[handle] = b = QtWidgets.QLabel(parent=None)
        self.layout().addWidget( b )
        b.setText(text)
        return b

    def get_button(self, handle):
        if( handle in self._buttons): return self._buttons[handle]
        else:                         return None
        
    def get_radio(self, handle):
        if( handle in self._radios): return self._radios[handle]
        else:                         return None

    def get_list(self, handle):
        if( handle in self._lists): return self._lists[handle]
        else:                         return None

    def get_label(self, handle):
        if( handle in self._labels): return self._labels[handle]
        else:                        return None

    def remove_button(self, handle):
        b = self.get_button(handle)
        if( b is not None):
            self.layout().removeWidget(b)
            b.deleteLater()
            del self._buttons[handle]

    def remove_radio(self, handle):
        r = self.get_radio(handle)
        if( r is not None):
            self.layout().removeWidget(r)
            r.deleteLater()
            del self._radios[handle]

    def remove_list(self, handle):
        r = self.get_list(handle)
        if( r is not None):
            self.layout().removeWidget(r)
            r.deleteLater()
            del self._lists[handle]

    def remove_label(self, handle):
        r = self.get_label(handle)
        if( r is not None):
            self.layout().removeWidget(r)
            r.deleteLater()
            del self._labels[handle]

    def clear_radios(self):
        for h in copy.copy(self._radios):
            self.remove_radio(h)

    def clear_buttons(self):
        for h in copy.copy(self._buttons):
            self.remove_button(h)

    def clear_lists(self):
        for h in copy.copy(self._lists):
            self.remove_list(h)

    def clear_labels(self):
        for h in copy.copy(self._labels):
            self.remove_label(h)

    ## Add custom signals and slots for radios and buttons

## ====================================================
##                     Test code
## ====================================================
# Create an instance of the application window and run it
def run_min_example():
    import sys
    qt_app = QtWidgets.QApplication(sys.argv)

    bh = ButtonListWidget(is_vertical=True)
    b1 = bh.add_button("shashoo", "press me")
    bh.add_button("shashoo2", "press me 2")
    bh.add_button("shashoo3", "press me 3")
    bh.remove_button("shashoo3")
    b2 = bh.get_button("shashoo")
    b3 = bh.get_button("hello")
    print( b1 is b2 )
    print( b3 )
    
    bh.show()

    r1 = bh.add_radio("z", "shayan")
    r2 = bh.add_radio("w", "shayan2")
    r3 = bh.add_radio("y", "shayan3")
    r4 = bh.get_radio("y")
    print (r3 is r4)
    print (bh.get_radio("adfhjlajldf") )
    bh.remove_radio( "z" )
    
    sys.exit(qt_app.exec_())

if __name__ == '__main__':
    run_min_example()
