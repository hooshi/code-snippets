#!/usr/bin/env python
# -*- coding: utf-8 -*-

import vtk


def main():
    colors = vtk.vtkNamedColors()

    # Set the colors.
    colors.SetColor("AzimuthArrowColor", [255, 77, 77, 255])
    colors.SetColor("ElevationArrowColor", [77, 255, 77, 255])
    colors.SetColor("RollArrowColor", [255, 255, 77, 255])
    colors.SetColor("SpikeColor", [255, 77, 255, 255])
    colors.SetColor("BkgColor", [26, 51, 102, 255])

    # Create a rendering window, renderer and interactor.
    ren = vtk.vtkRenderer()
    renWin = vtk.vtkRenderWindow()
    renWin.AddRenderer(ren)
    iren = vtk.vtkRenderWindowInteractor()
    iren.SetRenderWindow(renWin)
    ## Paraview style navigation
    irenStyle = vtk.vtkInteractorStyleTrackballCamera()
    iren.SetInteractorStyle(irenStyle)


    # Draw the arrows.
    pd = vtk.vtkPolyData()
    ca = vtk.vtkCellArray()
    fp = vtk.vtkPoints()
    fp.InsertNextPoint(0, 1, 0)
    fp.InsertNextPoint(8, 1, 0)
    fp.InsertNextPoint(8, 2, 0)
    fp.InsertNextPoint(10, 0.01, 0)
    fp.InsertNextPoint(8, -2, 0)
    fp.InsertNextPoint(8, -1, 0)
    fp.InsertNextPoint(0, -1, 0)
    ca.InsertNextCell(7)
    ca.InsertCellPoint(0)
    ca.InsertCellPoint(1)
    ca.InsertCellPoint(2)
    ca.InsertCellPoint(3)
    ca.InsertCellPoint(4)
    ca.InsertCellPoint(5)
    ca.InsertCellPoint(6)
    pd.SetPoints(fp)
    pd.SetPolys(ca)

    pd2 = vtk.vtkPolyData()
    ca2 = vtk.vtkCellArray()
    fp2 = vtk.vtkPoints()
    fp2.InsertNextPoint(0, 1, 0)
    fp2.InsertNextPoint(8, 1, 0)
    fp2.InsertNextPoint(8, 2, 0)
    fp2.InsertNextPoint(10, 0.01, 0)
    ca2.InsertNextCell(4)
    ca2.InsertCellPoint(0)
    ca2.InsertCellPoint(1)
    ca2.InsertCellPoint(2)
    ca2.InsertCellPoint(3)
    pd2.SetPoints(fp2)
    pd2.SetLines(ca2)

    
    arrowIM = vtk.vtkImplicitModeller()
    arrowIM.SetInputData(pd)
    arrowIM.SetSampleDimensions(50, 20, 8)

    arrowCF = vtk.vtkContourFilter()
    arrowCF.SetInputConnection(arrowIM.GetOutputPort())
    arrowCF.SetValue(0, 0.2)

    arrowWT = vtk.vtkWarpTo()
    arrowWT.SetInputConnection(arrowCF.GetOutputPort())
    arrowWT.SetPosition(5, 0, 5)
    arrowWT.SetScaleFactor(0.85)
    arrowWT.AbsoluteOn()

    arrowT = vtk.vtkTransform()
    arrowT.RotateY(60)
    arrowT.Translate(-1.33198, 0, -1.479)
    arrowT.Scale(1, 0.5, 1)

    arrowTF = vtk.vtkTransformFilter()
    arrowTF.SetInputConnection(arrowWT.GetOutputPort())
    arrowTF.SetTransform(arrowT)

    arrowMapper = vtk.vtkDataSetMapper()
    # Intended command
    # arrowMapper.SetInputConnection(arrowTF.GetOutputPort())
    # Ablations
    #arrowMapper.SetInputConnection(arrowWT.GetOutputPort())    
    arrowMapper.SetInputConnection(arrowCF.GetOutputPort())    
    #arrowMapper.SetInputConnection(arrowIM.GetOutputPort())    
    arrowMapper.ScalarVisibilityOff()

    # more ablation
    #initialPolyMapper = vtk.vtkPolyDataMapper()
    #initialPolyMapper.SetInputData( pd )
    #
    initPolyTriangulated = vtk.vtkTriangleFilter()
    initPolyTriangulated.SetInputData( pd )
    initialPolyMapper = vtk.vtkDataSetMapper()
    initialPolyMapper.SetInputConnection( initPolyTriangulated.GetOutputPort() )


    # Draw the azimuth arrows.
    a1Actor = vtk.vtkLODActor()
    a1Actor.SetMapper(arrowMapper)
    a1Actor.GetProperty().SetColor(colors.GetColor3d("AzimuthArrowColor"))
    a1Actor.GetProperty().SetSpecularColor(colors.GetColor3d("White"))
    a1Actor.GetProperty().SetOpacity(0.5)
    a1Actor.GetProperty().SetSpecular(0.3)
    a1Actor.GetProperty().SetSpecularPower(20)
    a1Actor.GetProperty().SetAmbient(0.2)
    a1Actor.GetProperty().SetDiffuse(0.8)
    a1Actor.GetProperty().SetInterpolationToFlat()
    print( a1Actor )

    initialPolyActor = vtk.vtkActor()
    initialPolyActor.SetMapper( initialPolyMapper )
    initialPolyActor.GetProperty().SetColor(colors.GetColor3d("Blue"))
    initialPolyActor.GetProperty().SetSpecularColor(colors.GetColor3d("White"))

    ren.AddActor(a1Actor)
    ren.AddActor(initialPolyActor)
    ren.SetBackground(colors.GetColor3d("SlateGray"))
    renWin.SetSize(640, 480)

    # Render the image.

    cam1 = (ren.GetActiveCamera())
    ren.ResetCamera()
    cam1.Azimuth(150)
    cam1.Elevation(30)
    cam1.Dolly(1.5)
    ren.ResetCameraClippingRange()

    iren.Initialize()
    iren.Start()


if __name__ == "__main__":
    main()
