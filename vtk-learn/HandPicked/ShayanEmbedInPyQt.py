#!/usr/bin/env python

# viewer artifact fix
# https://stackoverflow.com/questions/51357630/vtk-rendering-not-working-as-expected-inside-pyqt
# upgrade to pyqt5
# https://stackoverflow.com/questions/48105646/embedding-vtk-object-in-pyqt5-window
import sys
import vtk

from PyQt5 import QtWidgets
import vtk.qt
vtk.qt.QVTKRWIBase = "QGLWidget"
from vtk.qt.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor

from button_list_widget import ButtonListWidget

def generate_some_actors():
    colors = vtk.vtkNamedColors()
    
    # Draw the arrows.
    pd = vtk.vtkPolyData()
    ca = vtk.vtkCellArray()
    fp = vtk.vtkPoints()
    fp.InsertNextPoint(0, 1, 0)
    fp.InsertNextPoint(8, 1, 0)
    fp.InsertNextPoint(8, 2, 0)
    fp.InsertNextPoint(10, 0.01, 0)
    fp.InsertNextPoint(8, -2, 0)
    fp.InsertNextPoint(8, -1, 0)
    fp.InsertNextPoint(0, -1, 0)
    ca.InsertNextCell(7)
    ca.InsertCellPoint(0)
    ca.InsertCellPoint(1)
    ca.InsertCellPoint(2)
    ca.InsertCellPoint(3)
    ca.InsertCellPoint(4)
    ca.InsertCellPoint(5)
    ca.InsertCellPoint(6)
    pd.SetPoints(fp)
    pd.SetPolys(ca)
    
    arrowIM = vtk.vtkImplicitModeller()
    arrowIM.SetInputData(pd)
    arrowIM.SetSampleDimensions(50, 20, 8)

    arrowCF = vtk.vtkContourFilter()
    arrowCF.SetInputConnection(arrowIM.GetOutputPort())
    arrowCF.SetValue(0, 0.2)

    arrowMapper = vtk.vtkDataSetMapper()
    arrowMapper.SetInputConnection(arrowCF.GetOutputPort())    
    arrowMapper.ScalarVisibilityOff()

    initPolyTriangulated = vtk.vtkTriangleFilter()
    initPolyTriangulated.SetInputData( pd )
    initialPolyMapper = vtk.vtkDataSetMapper()
    initialPolyMapper.SetInputConnection( initPolyTriangulated.GetOutputPort() )


    # Draw the azimuth arrows.
    a1Actor = vtk.vtkLODActor()
    a1Actor.SetMapper(arrowMapper)
    a1Actor.GetProperty().SetColor(colors.GetColor3d("Red"))
    a1Actor.GetProperty().SetSpecularColor(colors.GetColor3d("White"))
    a1Actor.GetProperty().SetOpacity(0.99)
    a1Actor.GetProperty().SetSpecular(0.3)
    a1Actor.GetProperty().SetSpecularPower(20)
    a1Actor.GetProperty().SetAmbient(0.2)
    a1Actor.GetProperty().SetDiffuse(0.8)
    a1Actor.GetProperty().SetInterpolationToFlat()

    initialPolyActor = vtk.vtkActor()
    initialPolyActor.SetMapper( initialPolyMapper )
    initialPolyActor.GetProperty().SetColor(colors.GetColor3d("Blue"))
    initialPolyActor.GetProperty().SetSpecularColor(colors.GetColor3d("White"))
    
    return (a1Actor, initialPolyActor)

class MyInteractorStyle(vtk.vtkInteractorStyleTrackballCamera):

    def __init__(self, parent=None):
        self.AddObserver("MiddleButtonPressEvent", self.middle_button_press_event)
        self.AddObserver("MiddleButtonReleaseEvent", self.middle_button_release_event)

    def middle_button_press_event(self, obj, event):
        print("Middle Button pressed")
        self.OnMiddleButtonDown()
        return

    def middle_button_release_event(self, obj, event):
        print("Middle Button released")
        self.OnMiddleButtonUp()
        return

class MainWindow(QtWidgets.QMainWindow):

    def __init__(self, parent=None):
        QtWidgets.QMainWindow.__init__(self, parent)

        self.setCentralWidget( QtWidgets.QFrame() )
        self.centralWidget().setLayout( QtWidgets.QHBoxLayout() )

        self.vtkWidget = QVTKRenderWindowInteractor(self.centralWidget())
        self.centralWidget().layout().addWidget(self.vtkWidget)

        self.ren = vtk.vtkRenderer()
        self.vtkWidget.GetRenderWindow().AddRenderer(self.ren)
        self.iren = self.vtkWidget.GetRenderWindow().GetInteractor()
        self.iren.SetInteractorStyle( MyInteractorStyle() )


        self.actors  = generate_some_actors()
        for a in self.actors: self.ren.AddActor(a)
        
        self.ren.ResetCamera()
        self.iren.Initialize()

        self.buttonsWidget = ButtonListWidget()
        self.buttonsWidget.add_button( '_button_toggle_opacity', 'toggle opacity')
        self.centralWidget().layout().addWidget( self.buttonsWidget )

        b = self.buttonsWidget.get_button('_button_toggle_opacity')
        b.clicked.connect(self.on_click_toggle_opacity)


        self._is_opaque = False


    def on_click_toggle_opacity(self):
        self._is_opaque = not self._is_opaque
        opacity = 0.3  if  self._is_opaque else 0.99
        print("opacity will be {}".format(opacity))
        self.actors[0].GetProperty().SetOpacity(opacity)
        self.vtkWidget.Render()


if __name__ == "__main__":

    app = QtWidgets.QApplication(sys.argv)

    window = MainWindow()
    window.show()

    sys.exit(app.exec_())
