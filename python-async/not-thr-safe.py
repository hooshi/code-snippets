import threading
import time

from molly.log import logger

def coroutine():
    i = 1
    while True:
        num = (yield)
        print(i)
        time.sleep(3)
        i += num

cor = coroutine()
next(cor)
stop = False

def _thread_job():
    while not stop:
        cor.send(1)
t = threading.Thread(target=_thread_job)
t.start()

try:
    for  i in range(20):
        cor.send(5)
except Exception as e:
    logger.info('Exception raised %r', e)
finally:
    stop = True
    t.join()