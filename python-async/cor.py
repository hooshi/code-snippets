# https://stackabuse.com/coroutines-in-python

import sys
import time

from molly.log import logger

def print_if_throws(func, *args, **kwargs):
    rethrow = kwargs.pop('rethrow', True)
    try:
        func(*args, **kwargs)
    except BaseException as e:
        logger.info("%s raised %r", func.__name__, e)
        if rethrow:
            raise

def bare_bones():
    i = 0
    logger.info("My first Coroutine!")
    while True:
        logger.info("I am gonna wait for my %dth value", i)
        value = (yield)
        logger.info("I received %s", value)
        i += 1
        yield "hello"

coroutine = bare_bones()
logger.info(coroutine)
logger.info("next returned %s", next(coroutine))
coroutine.send('Shayan')
logger.info("next returned %s", next(coroutine))
coroutine.send('Hoshyari')
logger.info("next returned %s", next(coroutine))
coroutine.send('Mulan')

print_if_throws(coroutine.close)
print_if_throws(coroutine.send, "shayan 2", rethrow = False)
