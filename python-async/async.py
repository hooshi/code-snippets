import asyncio
import threading

event_loop = asyncio.new_event_loop()
event_loop_thread = threading.Thread(target=event_loop.run_forever)

class asyncrange:
	class __asyncrange:
		def __init__(self, *args):
			self.__iter_range = iter(range(*args))

		async def __anext__(self):
			try:
				return next(self.__iter_range)
			except StopIteration as e:
				raise StopAsyncIteration(str(e))

	def __init__(self, *args):
		self.__args = args

	def __aiter__(self):
		return self.__asyncrange(*self.__args)

async def asyncfunc(a, b, c):
	print("thread-id:", threading.get_native_id(), "Asyncing ", a, b, c, " started")
	await asyncio.sleep(1)
	print("thread-id:", threading.get_native_id(), "Asyncing ", a, b, c, " ended")
	return "Shayan-"+str(c)

async def asyncloop():
	# ans = []
	#for c in range(3):
	#	ans.append(event_loop.create_task(asyncfunc(0, 0, c)))
	ans = asyncio.gather(*[asyncfunc(0, 0, i) for i in range(3)])
	# await fs
	# print(fs.result())
	# return fs.result()
	# return fs
	return ans

def main():
	asyncio.set_event_loop(event_loop)
	event_loop_thread.start()

	#r = asyncfunc(1, 2, 3)
	r = asyncloop()
	future1 = asyncio.run_coroutine_threadsafe(r, loop=event_loop)
	future2 = asyncio.run_coroutine_threadsafe(asyncfunc(100,100,100), loop=event_loop)
	future2 = asyncio.run_coroutine_threadsafe(asyncfunc(200,200,200), loop=event_loop)
	print("thread-id:", threading.get_native_id(), " ", r, " ", future1)
	print("thread-id:", threading.get_native_id(), " ", r, " ", future2)


if __name__ == '__main__':
	try:
		main()
		event_loop_thread.join()
	except KeyboardInterrupt:
		print("Ctrl+C pressed, exiting.")
		event_loop.call_soon_threadsafe(event_loop.stop)
		event_loop_thread.join()
