# https://wiki.python.org/moin/Generators
from molly.log import logger

def genx():
    for i in range(5):
        yield i

def examine(obj):
    logger.info("%r, bases: %s, %s", obj, type(obj).__bases__)

x = range(10)
examine(x)

x = iter(x)
examine(x)

x = genx()
examine(x)
