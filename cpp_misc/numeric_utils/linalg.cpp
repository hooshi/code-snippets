#include <cmath>
#include <iostream>

// From Contrib
#include "../debugging/assert.hpp"

#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include <Eigen/Geometry>
#include <Eigen/LU>


namespace linalg
{

Eigen::Map< Eigen::MatrixXd >
reshaped(double* v, const int m, const int n)
{
  return Eigen::Map< Eigen::MatrixXd >(v, m, n);
}

Eigen::Map<const Eigen::MatrixXd >
reshaped(const double* v, const int m, const int n)
{
  return Eigen::Map<const Eigen::MatrixXd >(v, m, n);
}

Eigen::VectorXd
flatten(const Eigen::MatrixXd & v)
{
  return Eigen::VectorXd::ConstMapType(v.data(), v.rows() * v.cols());
};

Eigen::MatrixXd
unflatten(const Eigen::VectorXd & v, const int m, const int n)
{
  return Eigen::MatrixXd::ConstMapType(v.data(), m, n);
};

Eigen::MatrixXd
normal_dist(const Eigen::MatrixXd & in)
{
  Eigen::MatrixXd out(in.rows(), in.cols());
  const double sqrtpiinv = 1. / std::sqrt(2 * 3.14);

  for(unsigned i = 0; i < in.rows(); ++i)
    {
      for(unsigned j = 0; j < in.cols(); ++j)
        {
          out(i, j) = sqrtpiinv * std::exp(-in(i, j) * in(i, j) / 2.);
        }
    }
  return out;
}


Eigen::Matrix3d
make_rotation(const Eigen::Matrix3d & A)
{
  Eigen::JacobiSVD<Eigen::Matrix3d> svd(A, Eigen::ComputeFullU | Eigen::ComputeFullV);
  Eigen::Matrix3d ans = svd.matrixU() * svd.matrixV().transpose();
  assert(ans.determinant() > 0);
  assert(std::abs(ans.determinant() - 1) < 1e-10);
  return ans;
};

Eigen::MatrixXd
randi(unsigned int i, unsigned int j)
{
  return normal_dist(Eigen::MatrixXd::Random(i, j));
}

Eigen::Matrix3d
create_coordinate_system(Eigen::Vector3d a)
{
  a.normalize();
  Eigen::ColPivHouseholderQR<Eigen::Vector3d> qr(a);
  Eigen::Matrix3d Q(qr.matrixQ());

  // make sure that the first axis is *a*
  if( Q.col(0).dot( a ) < 0) Q.col(0) *= -1;

  // make sure the determinant is one
  Q.col(2) = Q.col(0).cross( Q.col(1) ); 
 
  force_assert_msg((Q.col(0) - a).norm() < 1e-10 ,"NOT THOROUGHLY DEBUGGED");
  force_assert_msg( std::abs( Q.col(1).dot(Q.col(2)) ) < 1e-10 ,"NOT THOROUGHLY DEBUGGED");
  force_assert_msg( std::abs( Q.col(0).dot(Q.col(1)) ) < 1e-10 ,"NOT THOROUGHLY DEBUGGED");
  force_assert_msg( std::abs( Q.col(0).dot(Q.col(2)) ) < 1e-10 ,"NOT THOROUGHLY DEBUGGED");
  force_assert_msg( std::abs( Q.col(0).dot(Q.col(0)) -1 ) < 1e-10 ,"NOT THOROUGHLY DEBUGGED");
  force_assert_msg( std::abs( Q.col(1).dot(Q.col(1)) -1) < 1e-10 ,"NOT THOROUGHLY DEBUGGED");
  force_assert_msg( std::abs( Q.col(2).dot(Q.col(2)) -1) < 1e-10 ,"NOT THOROUGHLY DEBUGGED");
  force_assert_msg( std::abs( Q.determinant() -1 ) < 1e-10 ,"NOT THOROUGHLY DEBUGGED");

  if(0)
    {
      std::cout << "Q1.Q2: " << Q.col(1).dot(Q.col(2)) << std::endl;
      std::cout << "Q0.Q2: " << Q.col(0).dot(Q.col(2)) << std::endl;
      std::cout << "Q0.Q1: " << Q.col(0).dot(Q.col(1)) << std::endl;
      std::cout << "Q0.Q0: " << Q.col(0).squaredNorm() << std::endl;
      std::cout << "Q1.Q1: " << Q.col(1).squaredNorm() << std::endl;
      std::cout << "Q2.Q2: " << Q.col(2).squaredNorm() << std::endl;
      std::cout << "Qdet : " << Q.determinant() << std::endl;
      std::cout << "Vector A: " << a.transpose() << std::endl;
      std::cout << "MATRIX Q: " << Q << std::endl;
      std::cout.flush();
    }
  return Q;
}

Eigen::Matrix3d
create_coordinate_system(Eigen::Vector3d a, Eigen::Vector3d b)
{
  a.normalize();
  // should not be colinear
  assert( std::abs(std::pow(a.dot(b),2) - a.squaredNorm()*b.squaredNorm()) > 1e-10 );
  // Gram schmidt
  b = (a - a.dot(b)*a).normalized();
  Eigen::Matrix3d Q;
  Q.col(0) = a;
  Q.col(1) = b;
  Q.col(2) = a.cross(b);

  force_assert_msg( std::abs( Q.col(1).dot(Q.col(2)) ) < 1e-10 ,"NOT THOROUGHLY DEBUGGED");
  force_assert_msg( std::abs( Q.col(0).dot(Q.col(1)) ) < 1e-10 ,"NOT THOROUGHLY DEBUGGED");
  force_assert_msg( std::abs( Q.col(0).dot(Q.col(2)) ) < 1e-10 ,"NOT THOROUGHLY DEBUGGED");
  force_assert_msg( std::abs( Q.col(0).dot(Q.col(0)) -1 ) < 1e-10 ,"NOT THOROUGHLY DEBUGGED");
  force_assert_msg( std::abs( Q.col(1).dot(Q.col(1)) -1) < 1e-10 ,"NOT THOROUGHLY DEBUGGED");
  force_assert_msg( std::abs( Q.col(2).dot(Q.col(2)) -1) < 1e-10 ,"NOT THOROUGHLY DEBUGGED");
  force_assert_msg( std::abs( Q.determinant() -1 ) < 1e-10 ,"NOT THOROUGHLY DEBUGGED");

  if(0)
    {
      std::cout << "Q1.Q2: " << Q.col(1).dot(Q.col(2)) << std::endl;
      std::cout << "Q0.Q2: " << Q.col(0).dot(Q.col(2)) << std::endl;
      std::cout << "Q0.Q1: " << Q.col(0).dot(Q.col(1)) << std::endl;
      std::cout << "Q0.Q0: " << Q.col(0).squaredNorm() << std::endl;
      std::cout << "Q1.Q1: " << Q.col(1).squaredNorm() << std::endl;
      std::cout << "Q2.Q2: " << Q.col(2).squaredNorm() << std::endl;
      std::cout << "Qdet : " << Q.determinant() << std::endl;
      std::cout << "Vector A: " << a.transpose() << std::endl;
      std::cout << "MATRIX Q: " << Q << std::endl;
      std::cout.flush();
    }
  return Q;
}

void
compute_eigenvalues(const Eigen::MatrixXd &in, Eigen::VectorXcd &eigenvalues, Eigen::MatrixXcd &eigenvectors)
{
  force_assert(in.rows() == in.cols());
  Eigen::EigenSolver<Eigen::MatrixXd> eigensolver(in, true);
  eigenvalues = eigensolver.eigenvalues();
  eigenvectors = eigensolver.eigenvectors();
}

void
solve_lu(const Eigen::MatrixXd &LHS, const Eigen::VectorXd &RHS, Eigen::VectorXd &sol)
{ 
  Eigen::PartialPivLU<Eigen::MatrixXd> lu;
  lu.compute(LHS);
  sol = lu.solve( RHS );
}

Eigen::Matrix3d
quaternion_to_rotation(const Eigen::Vector4d& q)
{
  return Eigen::Quaterniond(q.data()).toRotationMatrix();
}

Eigen::Matrix<double, 4, 3> 
quaternion_to_4x3mat(const Eigen::Vector4d& q)
{
   Eigen::Matrix<double, 4, 3> twoQ;
   // clang-format off
   twoQ << q.w(),  q.z(), -q.y() , 
          -q.z(),  q.w(),  q.x() ,  
           q.y(), -q.x(),  q.w() ,  
          -q.x(), -q.y(), -q.z() ; 
   // clang-format on
   return twoQ*0.5;
}


} // End of namespace linalg

