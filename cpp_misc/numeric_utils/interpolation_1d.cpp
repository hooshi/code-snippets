#include <cassert>
#include <cstdarg>
#include <cstdio>
#include <iostream>
#include <cmath>

#include "interpolation_1d.hpp"

namespace interpolation
{

// ==============================================================
//                         GET BARYCENTRIC
// ==============================================================

void
get_barycentric(const double x, int & i, double & f, int i_low, int i_high)
{
  double s = std::floor(x);
  i = (int)s;
  if(i < i_low)
  {
    i = i_low;
    f = 0;
  }
  else if(i > i_high - 2)
  {
    i = i_high - 2;
    f = 1;
  }
  else
    f = (double)(x - s);
}

// ==============================================================
//                          LINEAR
// ==============================================================

void
linear_interp_weights(const double toverdt, const unsigned toverdtmax, unsigned indices[], double weights[])
{
  double f;
  int i;
  get_barycentric(toverdt, i, f, 0, toverdtmax);
  indices[0] = i;
  indices[1] = i + 1;
  weights[0] = 1 - f;
  weights[1] = f;
}

// ==============================================================
//                          CUBIC
// ==============================================================
void
cubic_interp_weights(const double f, double & wneg1, double & w0, double & w1, double & w2)
{
  const double f2(f * f);
  const double f3(f2 * f);

  wneg1 = -(1. / 3) * f + (1. / 2) * f2 - (1. / 6) * f3;
  w0 = 1. - f2 + (1. / 2) * (f3 - f);
  w1 = f + (1. / 2) * (f2 - f3);
  w2 = (1. / 6) * (f3 - f);
}


void
cubic_interp_deriv_weights(const double f, double & wneg1, double & w0, double & w1, double & w2)
{
  const double f2(f * f);

  wneg1 = -(1. / 3) + f - (1. / 2) * f2;
  w0 = -2 * f + (1. / 2) * (3 * f2 - 1.);
  w1 = 1 + (1. / 2) * (2 * f - 3 * f2);
  w2 = (1. / 6) * (3 * f2 - 1);
}


void
cubic_interp_deriv2_weights(const double f, double & wneg1, double & w0, double & w1, double & w2)
{
  wneg1 = 1. - f;
  w0 = -2 + 3 * f;
  w1 = +1 - 3 * f;
  w2 = f;
}

static void
_interpolate_cubic_utility( //
  const double toverdt,
  const unsigned toverdtmax,
  unsigned indices[],
  double weights[],
  void (*coeff_getter_function)(const double, double &, double &, double &, double &))
{
  assert(toverdtmax >= 4);
  double f;
  int i;
  get_barycentric(toverdt, i, f, 0, toverdtmax);

  if(i == 0)
  {
    coeff_getter_function(-(1 - f), weights[0], weights[1], weights[2], weights[3]);
    int indicesout[] = {i, i + 1, i + 2, i + 3};
    std::copy(indicesout, indicesout + 4, indices);
  }
  else if(i == toverdtmax - 2)
  {
    coeff_getter_function(1 + f, weights[0], weights[1], weights[2], weights[3]);
    int indicesout[] = {i - 2, i - 1, i, i + 1};
    std::copy(indicesout, indicesout + 4, indices);
  }
  else
  {
    coeff_getter_function(f, weights[0], weights[1], weights[2], weights[3]);
    int indicesout[] = {i - 1, i, i + 1, i + 2};
    std::copy(indicesout, indicesout + 4, indices);
  }
}

void
cubic_interp_weights(const double toverdt, const unsigned toverdtmax, unsigned indices[], double weights[])
{
  _interpolate_cubic_utility(toverdt, toverdtmax, indices, weights, cubic_interp_weights);
}

void
cubic_interp_deriv_weights(const double toverdt, const unsigned toverdtmax, unsigned indices[], double weights[])
{
  if(toverdt > toverdtmax)
  {
    // Constant profile after we pass the last point
    // Constant profile
    // clang-format off
    indices[0] = toverdtmax-1; weights[0]= 0;
    indices[1] = toverdtmax-1; weights[1]= 0;
    indices[2] = toverdtmax-1; weights[2]= 0;
    indices[3] = toverdtmax-1; weights[3]= 0;
    // clang-format on
  }
  else
  {
    _interpolate_cubic_utility(toverdt, toverdtmax, indices, weights, cubic_interp_deriv_weights);
  }
}

void
cubic_interp_deriv2_weights(const double toverdt, const unsigned toverdtmax, unsigned indices[], double weights[])
{
  if(toverdt > toverdtmax)
  {
    // Constant profile after we pass the last point
    // clang-format off
    indices[0] = toverdtmax-1; weights[0]= 0;
    indices[1] = toverdtmax-1; weights[1]= 0;
    indices[2] = toverdtmax-1; weights[2]= 0;
    indices[3] = toverdtmax-1; weights[3]= 0;
    // clang-format on
  }
  else
  {
    _interpolate_cubic_utility(toverdt, toverdtmax, indices, weights, cubic_interp_deriv2_weights);
  }
}

} // end of interpolation
