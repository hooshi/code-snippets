#ifndef CONTRIB_LINALG_IS_INCLUDED
#define CONTRIB_LINALG_IS_INCLUDED

#include <Eigen/Core>


namespace linalg
{

Eigen::Map<Eigen::MatrixXd>
reshaped(double *, const int, const int);

Eigen::Map<const Eigen::MatrixXd>
reshaped(const double * v, const int m, const int n);

template<int m, int n>
Eigen::Map<Eigen::MatrixXd>
reshapedf(double * v)
{
  return Eigen::Map<Eigen::Matrix<double, m, n>>(v);
}

template<int m, int n>
Eigen::Map<const Eigen::MatrixXd>
reshapedf(const double * v)
{
  return Eigen::Map<const Eigen::Matrix<double, m, n>>(v);
}

Eigen::VectorXd
flatten(const Eigen::MatrixXd & v);

Eigen::MatrixXd
unflatten(const Eigen::VectorXd & v, const int m, const int n);

Eigen::MatrixXd
normal_dist(const Eigen::MatrixXd & in);

Eigen::Matrix3d
make_rotation(const Eigen::Matrix3d & A);

Eigen::MatrixXd
randi(unsigned int i, unsigned int j);

Eigen::Matrix3d
create_coordinate_system(Eigen::Vector3d a);

Eigen::Matrix3d
create_coordinate_system(Eigen::Vector3d a, Eigen::Vector3d b);

//
// Quick and dirty eigen wrappers to prevent HUUUGE compile times
//

void
compute_eigenvalues(const Eigen::MatrixXd &in, Eigen::VectorXcd &eigenvalues, Eigen::MatrixXcd &eigenvectors);

void
solve_lu(const Eigen::MatrixXd &LHS, const Eigen::VectorXd &RHS, Eigen::VectorXd &sol);

Eigen::Matrix3d
quaternion_to_rotation(const Eigen::Vector4d& q);

Eigen::Matrix<double, 4, 3> 
quaternion_to_4x3mat(const Eigen::Vector4d& q);


} // End of namespace linalg



#endif /* CONTRIB_LINALG_IS_INCLUDED */
