#ifndef CONTRIB_INTERPOLATION_1D_IS_INCLUDED
#define CONTRIB_INTERPOLATION_1D_IS_INCLUDED

#include <vector>



namespace interpolation
{

void
get_barycentric(const double x, int & i, double & f, int i_low, int i_high);

void
cubic_interp_weights(const double f, double & wneg1, double & w0, double & w1, double & w2);

void
cubic_interp_deriv_weights(const double f, double & wneg1, double & w0, double & w1, double & w2);

void
cubic_interp_deriv2_weights(const double f, double & wneg1, double & w0, double & w1, double & w2);

void
linear_interp_weights( //
  const double toverdt, //
  const unsigned toverdtmax,
  unsigned indices[],
  double weights[]);

void
cubic_interp_weights( //
  const double toverdt, //
  const unsigned toverdtmax,
  unsigned indices[],
  double weights[]);

void
cubic_interp_deriv_weights( //
  const double toverdt, //
  const unsigned toverdtmax,
  unsigned indices[],
  double weights[]);

void
cubic_interp_deriv2_weights( //
  const double toverdt, //
  const unsigned toverdtmax,
  unsigned indices[],
  double weights[]);

template<typename Data_Type>
Data_Type
linear_interp(const std::vector<Data_Type> & vals, const double toverdt)
{
  unsigned indices[2];
  double weights[2];
  linear_interp_weights(toverdt, (unsigned)vals.size(), indices, weights);
  return vals[indices[0]] * weights[0] + vals[indices[1]] * weights[1];
}

template<typename Data_Type>
Data_Type
cubic_interp(const std::vector<Data_Type> & vals, const double toverdt)
{
  assert(vals.size() >= 4);
  unsigned indices[4];
  double weights[4];
  cubic_interp_weights(toverdt, (unsigned)vals.size(), indices, weights);
  return vals[indices[0]] * weights[0] //
         + vals[indices[1]] * weights[1] //
         + vals[indices[2]] * weights[2] //
         + vals[indices[3]] * weights[3];
}

template<typename Data_Type>
Data_Type
cubic_interp_deriv(const std::vector<Data_Type> & vals, const double toverdt)
{
  assert(vals.size() >= 4);
  unsigned indices[4];
  double weights[4];
  cubic_interp_deriv_weights(toverdt, (unsigned)vals.size(), indices, weights);
  return vals[indices[0]] * weights[0] //
         + vals[indices[1]] * weights[1] //
         + vals[indices[2]] * weights[2] //
         + vals[indices[3]] * weights[3];
}

template<typename Data_Type>
Data_Type
cubic_interp_deriv2(const std::vector<Data_Type> & vals, const double toverdt)
{
  assert(vals.size() >= 4);
  unsigned indices[4];
  double weights[4];
  cubic_interp_deriv2_weights(toverdt, (unsigned)vals.size(), indices, weights);
  return vals[indices[0]] * weights[0] //
         + vals[indices[1]] * weights[1] //
         + vals[indices[2]] * weights[2] //
         + vals[indices[3]] * weights[3];
}

} // end of interpolation


#endif /*CONTRIB_INTERPOLATION_1D_IS_INCLUDED*/
