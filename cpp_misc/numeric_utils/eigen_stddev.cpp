// https://gist.github.com/kevinhughes27/6356673


using namespace Eigen;

// these are the typedefs I like to work with
typedef Matrix< double, Dynamic, 1, ColMajor > EVector;
typedef Matrix< double, Dynamic, Dynamic, ColMajor > EMatrix;

// Mean
EVector mean = X.rowwise().sum() / X.rows();	

// Standard Deviation
EVector std = (X.rowwise() - mean.transpose()).array().pow(2).colwise().sum() / X.rows();

// Mean Center
EMatrix Xc = X.colwise() - mean;
