// Create and change directories
// Author: Shayan Hoshyari

#ifndef FOLDERTOOLSISINCLUDED
#define FOLDERTOOLSISINCLUDED

#include <string>


namespace foldertools
{


bool makedir(const char * name);

bool setdir(const char * name);

bool makeandsetdir(const char * name);


void pushd();

void popd();

void cwd(char[], const int);

std::string cwd();


} // end of foldertools


#endif /* FOLDERTOOLSISINCLUDED */
