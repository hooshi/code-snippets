#include <queue>
#include <memory>
#include <type_traits>
#include <cinttypes>

namespace {
  
using Int = std::int64_t;

template<typename T>
constexpr bool always_false_v = (!std::is_same<T,T>::value);

template<typename Element_Type_>
class Priority_queue_with_lazy_delete {
public:
  using Element_type = Element_Type_;
  using Internal_element_type = typename Traits<Element_type>::Internal_element_type;
  
  Int push( Element_type&& value ) {
    Internal_element_type ie = Traits<Element_type>::as_internal_element( std::forward(value) );
    Int index = m_versions.size();
    Int version = 0;
    m_queue.push( {version, index, std::move(ie)} );
    m_versions.push_back( version );
    return index;
  }

  Int push( const Element_type& value ) {
    Internal_element_type ie = Traits<Element_type>::as_internal_element( value );
    Int index = m_versions.size();
    Int version = 0;
    m_queue.push( {version, index, std::move(ie)} );
    m_versions.push_back( version );
    return index;
  }

  bool remove(const Int index) {
    ++m_versions[index];
  }

  std::pair<Int, Element_type> pop() {
    while(true){
      if( m_queue.empty() ) {
        return {-1, Element_type()};
      }
      Versioned_internal_element_type ve = m_queue.pop();
      if( ve.version == m_versions[ve.index] ) {
        return {ve.index, Traits<Element_type>::as_element(std::move(ve.element))};
      }
    }
  }
  
protected:
  struct Versioned_internal_element_type {
    Int version;
    Int index;
    Internal_element_type element;
  };
  std::queue<Int> m_versions;
  std::priority_queue<Versioned_internal_element_type> m_queue;
};

