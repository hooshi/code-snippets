skip_comments ( FILE* fl ) {
    char tmp[4096];
    bool is_there_more_to_read = false;
    assert_break ( fl );

    for ( ;; ) {
        const int c = fgetc(fl);

        if ( c ==EOF ) {
            is_there_more_to_read = false;
            break;
        } else if ( c == '#' ) {
            fgets ( tmp, 4096, fl );
            continue;
        } else if ( ( c != ' ' ) && ( c != '\n' ) && ( c != '\r' ) ) {
            int success = fseek ( fl, -1, SEEK_CUR );
            assert ( success == 0 );
            is_there_more_to_read = true;
            break;
        }
    }

    return is_there_more_to_read;
}

static bool
skip_comments ( std::ifstream& fl ) {
    str tmp;
    bool is_there_more_to_read = false;
    assert_break ( fl.is_open() );

    for ( ;; ) {
        const int c = fl.get();

        if ( fl.eof() || ( !fl.good() ) ) {
            is_there_more_to_read = false;
            break;
        } else if ( c == '#' ) {
            std::getline ( fl, tmp );
            continue;
        } else if ( ( c != ' ' ) && ( c != '\n' ) && ( c != '\r' ) ) {
            fl.unget();
            is_there_more_to_read = true;
            break;
        }
    }

    return is_there_more_to_read;
}

