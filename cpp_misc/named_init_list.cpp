// https://stackoverflow.com/questions/18731707/why-does-c11-not-support-designated-initializer-lists-as-c99

#include <string>
#include <iostream>

#define named_init(T, ...) ([&]{ T ${}; __VA_ARGS__; return $; }())


struct PrintInfoParams{
  std::string author = "anne man";
  bool is_serious = false;
};

void print_info(const std::string s, const PrintInfoParams p=PrintInfoParams()) {
  std::cout << s << " | by " << p.author << " who is " << p.is_serious << "\n";
}


int main() {
  print_info("Bonjour mon amis", named_init(
    PrintInfoParams,                                        
    $.author="Spider man investor pro",
    $.is_serious=true
  ));

  return 0;
}
