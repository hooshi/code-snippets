#ifndef ASSERT_IS_INCLUDED
#define ASSERT_IS_INCLUDED

#include <cassert>
#include <sstream>

#include "console_colors.hpp"

#define force_assert_msg(EXPR, MSG)                                                   \
  if(!(EXPR))                                                                         \
  {                                                                                   \
    std::ostringstream ss;                                                            \
    ss << std::scientific;                                                            \
    printf("Assertion in %s, %d \n", __func__, __LINE__);                             \
    ss.str("");                                                                       \
    ss << #EXPR << "\n";                                                              \
    ss << MSG;                                                                        \
    printf("%s %s %s\n", consolecolors::red(), ss.str().c_str(), consolecolors::reset()); \
    assert(EXPR);                                                                     \
    throw;                                                                            \
  }

#define force_assert(EXPR) force_assert_msg(EXPR, "")


#endif /* ASSERT_IS_INCLUDED */
