#include <cstdlib>
#include <cstdio>

namespace {

/**
 * This constructor creates a property that tracks an actual member of type T (field)
 * belonging to an object of type Object.
 * 
 * So much meta: https://stackoverflow.com/questions/670734/pointer-to-class-data-member
 */
template<typename Object_, typename Member_>
class PropertyTracker
{
public:
	using Object = Object_;
	using Member = Member_;
	using MemberPtr = Member(Object::*);

	PropertyTracker(MemberPtr member):
		_member(member)
	{
	}

	/**
	 * Implementing the property interface
	 */
	Member get(const Object &object) const {
		return object.*_member;
	}

	void set(Object &object, const Member &value)const  {
		object.*_member = value;
	}

private:
	MemberPtr _member;
};

template<typename Object, typename Member>
PropertyTracker<Object, Member> create_property_tracker(Member(Object::*member)) {
	return PropertyTracker<Object, Member>(member);
}

struct Apple {
	double seed;
};

} 



int main() {
	Apple apple;
	auto p = create_property_tracker(&Apple::seed);
	p.set(apple, 0.2);
	const double _seed = p.get(apple);
	printf("New corn piece: %f %f \n", apple.seed, _seed);
	return EXIT_SUCCESS;
}
