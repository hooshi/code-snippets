#include <iostream>

struct A {
  struct _1{};
  struct _2{};
  struct _3{};
};

struct B {
  struct _1{};
  struct _2{};
};

template <typename T>
struct is_A{
  constexpr static bool value = false;
};

template <>
struct is_A<A>{
  constexpr static bool value = true;
};

int main(){
  std::cout << is_A<A>::value << std::endl;
  std::cout << is_A<B>::value << std::endl;
  return 0;
}
