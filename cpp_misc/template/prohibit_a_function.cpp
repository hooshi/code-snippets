https://stackoverflow.com/questions/34745581/forbids-functions-with-static-assert .

https://git.corp.adobe.com/adobe-research/Lagrange/pull/252

// METHOD 1

template<typename ...>
struct SneakyBools
{
  constexpr static bool False = false;
  constexpr static bool True = false;
};

// Prevent bad calls
template<typename T>
SparseRange<T> range_sparse(T max, std::vector<T>&& active)  {
  static_assert(SneakyBools<T>::value, "Do not use range_sparse with an rvalue ref list");
}
template<typename MeshType>
SparseRange<typename MeshType::Index> range_facets(const MeshType& mesh,
                                                   typename MeshType::IndexList&& active)  {
    static_assert(SneakyBools<MeshType>::value, "Do not use range_facets with an rvalue ref list");
}
template<typename MeshType>
SparseRange<typename MeshType::Index> range_vertices(const MeshType& mesh,
                                                     typename MeshType::IndexList&& active)  {
  static_assert(SneakyBools<MeshType>::value, "Do not use range_vertices with an rvalue ref list");
}


// METHOD 2

// Prevent bad calls
template<typename T>
SparseRange<T> range_sparse(T max, std::vector<T>&& active) = delete;

template<typename MeshType>
SparseRange<typename MeshType::Index> range_facets(const MeshType& mesh,
                                                   typename MeshType::IndexList&& active) =delete;

template<typename MeshType>
SparseRange<typename MeshType::Index> range_vertices(const MeshType& mesh,
                                                     typename MeshType::IndexList&& active) =delete;
