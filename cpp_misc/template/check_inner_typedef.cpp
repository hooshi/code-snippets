#include <type_traits>
#include <iostream>
#include <cstdlib>

template<typename... Ts>
struct Void {using type = void;};

template<typename T, typename _>
struct has_types_helper : std::false_type {
    static_assert(std::is_same<_, void>::value, "second template argument is always void");
};

template<typename T>
struct has_types_helper<
        T,
        typename Void<
            typename T::A,
            typename T::B,
            typename T::C::D
            >::type
        > : public std::true_type {};

template<typename T>
using has_types = has_types_helper<T, void>;


struct _1 {
  struct A{};
  struct B{};
  struct C{ struct D{}; };
};

struct _2 {
  struct A{};
  struct C{ struct D{}; };
};


int main() {

  std::cout << has_types<_1>::value <<"\n";
  std::cout << has_types<_2>::value <<"\n";

  return EXIT_SUCCESS;
}
