#include <vector>
#include <cinttypes>
#include <cstdlib>
#include <iostream>

using Int = std::int64_t;

/**
 * A basic implementation of the disjoint-sets data structure, specialized for
 * the case where each element is an `Int` and the total set of elements
 * consists of sequential integers starting at `0`.
 */
class DisjointIntSets {
public:
	DisjointIntSets();

	DisjointIntSets(Int n);

	Int size() const;

	void clear();

	void reserve(Int n);

	Int parent(Int i) const;

	Int add();

	Int find(Int i);

	/**
	 * This function would be called `union`, except that it's already a
	 * keyword in C++.
	 */
	Int merge(Int i, Int j);

protected:
	Int& parent(Int i);

	Int link(Int i, Int j);

private:
	std::vector<Int> m_parent_map;
};

DisjointIntSets::DisjointIntSets() { }

DisjointIntSets::DisjointIntSets(Int n) :
	m_parent_map(n)
{
	for(Int i = 0; i < (Int)m_parent_map.size(); ++i) {
		m_parent_map.at(i) = i;
	}
}

Int DisjointIntSets::size() const {
	return m_parent_map.size();
}

void DisjointIntSets::clear() {
	m_parent_map.clear();
}

void DisjointIntSets::reserve(Int n) {
	m_parent_map.reserve(n);
}

Int DisjointIntSets::parent(Int i) const {
	return m_parent_map.at(i);
}

Int DisjointIntSets::add() {
	Int i = size();
	m_parent_map.push_back(i);
	assert(parent(i) == i); // assert element is in its own set
	return i;
}

Int DisjointIntSets::find(Int i) {
	Int j = parent(i);
	Int k = parent(j);
	while(j != k) {
		// perform path halving
		parent(i) = k;
		i = k;
		j = parent(i);
		k = parent(j);
	}
	return j;
}

/**
 * This function would be called `union`, except that it's already a
 * keyword in C++.
 */
Int DisjointIntSets::merge(Int i, Int j) {
	return link(find(i), find(j));
}

//----- protected:

Int& DisjointIntSets::parent(Int i) {
	return m_parent_map.at(i);
}

Int DisjointIntSets::link(Int i, Int j) {
	assert(parent(i) == i && parent(j) == j);
	if(i < j) {
		parent(j) = i;
		return i;
	} else {
		parent(i) = j;
		return j;
	}
}


int main()  {
  DisjointIntSets d(10);
  d.merge(3, 5);
  d.merge(4, 7);
  d.merge(5, 3);
  d.merge(7, 4);

  for(Int i = 0 ; i < (Int)d.size(); ++i){
    std::cout << d.find(i) << std::endl;// << " | " << d.parent(i) << std::endl;
  }
  

  return EXIT_SUCCESS;
}
