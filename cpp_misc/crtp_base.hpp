template<class Derived>
class CRTPBase {
public:
	inline const Derived& derived() const & {
		return static_cast<const Derived&>(*this);
	}

	inline Derived& derived() & {
		return static_cast<Derived&>(*this);
	}

	inline Derived&& derived() && {
		return std::move(static_cast<Derived&>(*this));
	}
};
