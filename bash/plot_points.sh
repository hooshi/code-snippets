#Handy little bash snippet.  Got a command that generates a bunch of x y data pairs (separated by a space) and want to make a graph?  Use this:

cat data.txt | gnuplot -p -e 'set term svg; set output "out.svg"; plot "/dev/stdin" with lines'
