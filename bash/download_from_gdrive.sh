## https://gist.github.com/iamtekeste/3cdfd0366ebfd2c0d805

function download_from_gdrive
{
    FILENAME="$2"
    FILEID="$1"

    FILEID2=$(wget --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate "https://docs.google.com/uc?export=download&id=${FILEID}" -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')
    
    wget --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=${FILEID2}&id=${FILEID}" -O "${FILENAME}"

    rm -rf /tmp/cookies.txt
}


# example
#FILEID="1KzOzZowfiI1USOCh8ztbtn9VDyYNBmKq"
#download_from_gdrive ${FILEID} raw_data/tmnist.zip
