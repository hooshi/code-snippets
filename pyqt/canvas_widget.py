#!/usr/bin/env python3

from PyQt5 import QtWidgets
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT

#
# Reduce functionality of NavigationToolbar2QT to reduce confusion
# Thanks to https://stackoverflow.com/questions/12695678/how-to-modify-the-navigation-toolbar-easily-in-a-matplotlib-figure-window
#
class MiniNavigationToolbar(NavigationToolbar2QT):
    # solution 1
    toolitems = [t for t in NavigationToolbar2QT.toolitems if
                 t[0] in ('Home', 'Pan', 'Zoom') ]
    # solution 2
    # def __init__(self, canvas, parent):
    #     self.toolitems = [t for t in self.toolitems if
    #              t[0] in ('Home', 'Pan', 'Zoom') ]
    #     super().__init__(canvas, parent)

#
# A widget to view a matplotlib figure
#
class CanvasWidget(QtWidgets.QWidget):
    """A place to draw your stuff, either using painter or matplotlib."""
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setLayout( QtWidgets.QVBoxLayout(self) )
        self._mpl_widget = None
        self._mpl_navigator = None
        
    def is_active(self):
        if (self._mpl_widget is not None):
            return True
        else:
            return False
        
    def clear_figure(self):
        if( self._mpl_widget ):
            self.layout().removeWidget( self._mpl_widget )
            self._mpl_widget.deleteLater()
            self._mpl_widget = None
            #
            self.layout().removeWidget( self._mpl_navigator )
            self._mpl_navigator.deleteLater()
            self._mpl_navigator = None
        
    def show_mpl(self, fig):
        self.clear_figure()
        #
        self._mpl_widget = FigureCanvasQTAgg(fig)
        self._mpl_widget.setSizePolicy( QtWidgets.QSizePolicy.Expanding,
                                        QtWidgets.QSizePolicy.Expanding)
        self._mpl_widget.updateGeometry()
        self.layout().addWidget( self._mpl_widget )
        #
        #self._mpl_navigator = NavigationToolbar2QT(self._mpl_widget, self)
        self._mpl_navigator = MiniNavigationToolbar(self._mpl_widget, self) 
        self.layout().addWidget(self._mpl_navigator)
        #
        l = self._mpl_navigator.layout()
        items = (l.itemAt(i) for i in range(l.count())) 
        for w in items:
            try: print(w.toolTip())
            except: pass


## ====================================================
##                     Test code
## ====================================================
# Create an instance of the application window and run it
def run_min_example():
    import sys
    import numpy as np
    import matplotlib.pyplot as plt
    
    qt_app = QtWidgets.QApplication(sys.argv)

    cw = CanvasWidget()
    # cw.show_svg('example/Zeichen_123.svg')
    cw.show_svg('temp/out_result_regularizer_step4_black.svg')
    cw.show()

    cw2 = CanvasWidget()
    plt.figure()
    x = np.linspace(1,10,40)
    y = np.sin(x)
    plt.plot(x,y,'-r')
    cw2.show_mpl(plt.gcf())
    cw2.show()
    
    sys.exit(qt_app.exec_())

if __name__ == '__main__':
    run_min_example()
