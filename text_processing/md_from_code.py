import re

def tokenize_text(text):
    """## Grabs a text and converts all instancts of
    /*{{{TITLE
    TEXT
    (optional whitespace) }}}*/
    and
    //{{{TITLE
    TEXT
    (optional whitespace) }}}//
    To a dictionary with dict[TITLE] = TEXT
    """
    
    ## Define the patterns
    ## .*? means match everything except \n but lazy
    ## + means 1 or more
    ## . means anything but \n
    ## * means zero or more
    ## () creates a capture group (will be in matches)
    ## (?:) creates non capture group
    ## \s is space or new line
    ## \n is new line
    TOKEN_PATTERNS = [
        r'//{{{(.*?)\n((?:.|\n)*?)(?:\s+)//}}}',
        r'/\*{{{(.*?)\n((?:.|\n)*?)(?:\s+)}}}\*/',
    ]

    ## Match everything
    matches = []
    for pattern in TOKEN_PATTERNS:
        p = re.compile(pattern)
        matches += p.findall(text)

    # print("number of matches: {}".format(len(matches)))

    ## Take care of empty cases and convert to dict
    tokens = {}
    for match in matches:
        assert len(match)==1 or len(match)==2, "invalid match={}".format(match)
        if len(match)==2:
            tokens[match[0]] = match[1]
        else:
            tokens[match[0]] = ''

    return tokens

def run_executable(filename):
    """Runs an executable and grabs its output"""
    import subprocess 
    ans = subprocess.getoutput([filename])
    return str(ans)

def merge_token_dictionaries(
    titles,
    dictionaries
):
    """Merges a series of dictionaries while prepending a title to the
       keys of each one
    """
    assert len(titles) == len(dictionaries)
    ans = {}
    for t, d in zip(titles, dictionaries):
        for k in d.keys():
            if k == '':  ans['{}'.format(t)] = d[k]
            else:  ans['{}/{}'.format(t,k)] = d[k]
    return ans


def replace_tokens(text, tokens):
    """Replaces all instances of {{{TITLE}}} in a text with tokens[TITLE]"""
    TOKEN_PATTERNS = [
        r'{{{(.*?)}}}',
    ]

    ## Define the substitution function
    def replacer(match):
        # Note: group() will also give you the enclosing $s
        section_name = match.groups()[0]
        print( section_name )
        assert section_name in tokens, "Could not find {} in the tokens".format(section_name)
        return tokens[section_name]

    ## Now perform the substitution
    for pattern in TOKEN_PATTERNS:
        p = re.compile(pattern)
        text = p.sub(replacer, text)

    return text



def main():
    """Generates the doc
    The only input is the location of the executable for the executable to run
    Assumes that b.json and the .cpp file are in CWD."""
    import sys
    assert len(sys.argv) == 2, 'Usage: generate_doc.py [example_executable_location]'

    text = None

    # Tokenize serialization.cpp, b.json, and the output of the executable
    with open('serialization.cpp') as fl: text = fl.read()
    tokens_serialization_cpp=tokenize_text(text)
    with open('b.json') as fl: text = fl.read()
    tokens_b_json={'':text}
    tokens_console={'':run_executable(sys.argv[1])}

    ## print(tokens_console)
    
    ## Merge the tokens
    tokens = merge_token_dictionaries(
        ['serialization.cpp', 'b.json', 'console'],
        [tokens_serialization_cpp, tokens_b_json, tokens_console],
    )
    ## print(tokens.keys())

    ## Read the doc, replace tokens and write it again.
    with open('serialization.in.md') as fl: text = fl.read()
    text = replace_tokens(text, tokens)
    with open('_serialization.md', 'w') as fl:
        fl.write('---\n')
        fl.write('layout: default\n')
        fl.write('---\n')
        fl.write('[comment]: <> ({})\n\n'.format('This file is automatically generated. Don\'t edit it'))
        fl.write(text)


if __name__ == '__main__':
    main()
