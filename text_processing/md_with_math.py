## From https://chaonan99.github.io/2016/how-to-add-equation-on-github-markdown-file/

import urllib.parse
import re

TOKEN_PATTERN = r'\$\$(.*?)\$\$'
INPUT = 'README.in.md'
OUTPUT = 'README.md'
# GENERATOR = 'http://latex.codecogs.com/gif.latex?'
GENERATOR = 'https://render.githubusercontent.com/render/math?math='

def convert_token_to_link(token):
	encoded_token = urllib.parse.quote(token)
	link = '<img src="{}{}" alt="{}"></img>'.format(GENERATOR, encoded_token, token)
	return link

def convert_all_tokens_in_text(text):
	def replacer(match):
		# Note: group() will also give you the enclosing $s
		token = match.groups()[0]
		print( token )
		return convert_token_to_link( token )
	p = re.compile(TOKEN_PATTERN)
	return p.sub(replacer, text)



def main():
	input = None
	with open(INPUT, 'r') as file:
		input = file.read()

	output = convert_all_tokens_in_text(input)
	with open(OUTPUT, 'w') as file:
		file.write(output)


if __name__ == '__main__':
	main()
