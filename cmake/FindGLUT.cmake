# Locate KNITRO
#
# This module defines:
#
# TO USE IT MUST SET
#
# GLUT_ROOT:   Must be the folder having GL/glut.h
# or  GLUT_DIR
#
# GLUT_FOUND:       TRUE if the library is available
# GLUT_INCLUDE_DIRS:  Directory containing header files of KNITRO
# GLUT_LIBRARIES:     Libraries to link against when using KNITRO
# GLUT_DLLS:          DLLs required to load the library (only on Windows)
#
# Shayan Hoshyari 21/09/2018
#
#

if(WIN32)

  find_library(GLUT_LIBRARIES
    NAMES freeglut  freeglutd
    PATH_SUFFIXES lib
    PATHS
    ${GLUT_ROOT}
    ${GLUT_DIR}
    )
  
  find_file(GLUT_DLLS
    NAMES freeglut.dll freeglutd.dll 
    PATH_SUFFIXES bin
    PATHS
    ${GLUT_ROOT}
    ${GLUT_DIR}
    )

  find_path(GLUT_INCLUDE_DIRS GL/freeglut.h
    PATH_SUFFIXES include
    PATHS
    ${GLUT_ROOT}
    ${GLUT_DIR}
    )

elseif(APPLE)
  message(FATAL_ERROR "NOT SUPPORTED YET")
else()

    find_library(GLUT_LIBRARIES
    NAMES glut 
    PATH_SUFFIXES lib
    PATHS
    ${GLUT_ROOT}
    ${GLUT_DIR}
    NO_DEFAULT_PATH
    ) # try what user has given
  find_library(GLUT_LIBRARIES
    NAMES glut 
    PATH_SUFFIXES lib
    ) # if failed, now try system dirs
  
  set(GLUT_DLLS "")

  find_path(GLUT_INCLUDE_DIRS GL/freeglut.h
    PATH_SUFFIXES include
    PATHS
    ${GLUT_ROOT}
    ${GLUT_DIR}
    )

endif()


find_package(PackageHandleStandardArgs REQUIRED)
find_package_handle_standard_args(GLUT
  REQUIRED_VARS GLUT_INCLUDE_DIRS GLUT_LIBRARIES)

