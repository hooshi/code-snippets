# Locate KNITRO
#
# This module defines:
#
# KNITRO_FOUND:         TRUE if the library is available
# KNITRO_INCLUDE_DIRS:  Directory containing header files of KNITRO
# KNITRO_LIBRARIES:     Libraries to link against when using KNITRO
# KNITRO_LIBRARY_DIRS:  The directories containing the libraries/dlls
# KNITRO_DLLS:          DLLs required to load the library (only on Windows)
#
# Adds the library DF::Knitro for linking.
#
# Or add ${KNITRO_LIBRARIES} for linking +
#        ${KNITRO_INCLUDE_DIRS} for include_dirs()
#        ${KNITRO_LIBRARY_DIRS} as a post build copy target
#
# Shayan Hoshyari 05/09/2018: This does not seem to find the new
# knitro 1101 I added the functionality
#
#

if(WIN32)
  set(KNITRO_DIR_GUESSES "C:/Program Files/Ziena/KNITRO 9.0.1/")

  find_library(KNITRO_LIBRARIES
    NAMES knitro1030 knitro1012 knitro901 knitro811
    knitro1101
    PATH_SUFFIXES lib
    PATHS
    ENV KNITRO_DIR
    ${KNITRO_DIR}
    ${KNITRO_DIR_GUESSES}
    )
  
  find_file(KNITRO_DLL_NITRO
    NAMES knitro1030.dll knitro1012.dll knitro901.dll knitro811.dll
    knitro1101.dll
    PATH_SUFFIXES lib
    PATHS
    ENV KNITRO_DIR
    ${KNITRO_DIR}
    ${KNITRO_DIR_GUESSES}
    )

  find_file(KNITRO_DLL_OMP
    NAMES libiomp5md.dll
    PATH_SUFFIXES lib
    PATHS
    ENV KNITRO_DIR
    ${KNITRO_DIR}
    ${KNITRO_DIR_GUESSES}
    )
elseif(APPLE)
  set(KNITRO_DIR_GUESSES "")

  # Find the shared library
  find_library(KNITRO_LIBRARIES
    NAMES knitro
    PATH_SUFFIXES lib
    PATHS
    ENV KNITRO_DIR
    ${KNITRO_DIR}
    ${KNITRO_DIR_GUESSES}
    )

  set(KNITRO_DLL_NITRO ${KNITRO_LIBRARIES})

  find_library(KNITRO_DLL_OMP
    NAMES iomp5
    PATH_SUFFIXES lib
    PATHS
    ENV KNITRO_DIR
    ${KNITRO_DIR}
    ${KNITRO_DIR_GUESSES}
    )
else()
  set(KNITRO_DIR_GUESSES "")

  # Find the shared library
  find_library(KNITRO_LIBRARIES
    NAMES knitro libknitro.so
    PATH_SUFFIXES lib
    PATHS
    ENV KNITRO_DIR
    ${KNITRO_DIR}
    ${KNITRO_DIR_GUESSES}
    )

  set(KNITRO_DLL_NITRO ${KNITRO_LIBRARIES})

  set(KNITRO_DLL_OMP "")
endif()

find_path(KNITRO_INCLUDE_DIR knitro.h
  PATH_SUFFIXES include
  PATHS
  ENV KNITRO_DIR
  ${KNITRO_DIR}
  ${KNITRO_DIR_GUESSES}
  )

find_path(KNITRO_CXX_INCLUDE_DIR KTRSolver.h
  PATH_SUFFIXES "C++/include"
  PATHS
  ENV KNITRO_DIR
  ${KNITRO_DIR}/examples
  ${KNITRO_DIR_GUESSES}/examples
  )

set(KNITRO_INCLUDE_DIRS ${KNITRO_INCLUDE_DIR};${KNITRO_CXX_INCLUDE_DIR})

find_package(PackageHandleStandardArgs REQUIRED)
find_package_handle_standard_args(KNITRO REQUIRED_VARS KNITRO_INCLUDE_DIRS KNITRO_LIBRARIES)

if(KNITRO_FOUND)
  get_filename_component(KNITRO_LIBRARY_DIRS ${KNITRO_LIBRARIES} DIRECTORY)
  get_filename_component(KNITRO_DLL_DIRS ${KNITRO_DLL_NITRO} DIRECTORY)

  list(APPEND KNITRO_LIBRARY_DIRS ${KNITRO_DLL_DIRS})
  list(REMOVE_DUPLICATES KNITRO_LIBRARY_DIRS)

  set(KNITRO_DLLS ${KNITRO_DLL_NITRO} ${KNITRO_DLL_OMP})

  add_library(DF::Knitro SHARED IMPORTED)
  set_target_properties(DF::Knitro PROPERTIES
    IMPORTED_IMPLIB "${KNITRO_LIBRARIES}"
    IMPORTED_LOCATION "${KNITRO_DLLS}"
    INTERFACE_INCLUDE_DIRECTORIES "${KNITRO_INCLUDE_DIRS}"
    )
endif()
