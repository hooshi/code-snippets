# Locate KNITRO
#
# This module defines:
#
# TO USE IT MUST SET
#
# HDF5_ROOT:   Must be the folder having HDF5
# or  HDF5_DIR
#
# HDF5_FOUND:       TRUE if the library is available
# HDF5_INCLUDE_DIRS:  Directory containing header files of KNITRO
# HDF5_LIBRARIES:     Libraries to link against when using KNITRO
# HDF5_DLLS:          DLLs required to load the library (only on Windows)
#
# Shayan Hoshyari 23/11/2018
#
#

if(WIN32)

  find_library(HDF5_CXX_LIBRARIES
    NAMES hdf5_cpp_D  hdf5_cpp 
    PATH_SUFFIXES lib
    PATHS
    ${HDF5_ROOT}
    ${HDF5_DIR}
    NO_DEFUALT_PATHS
    )
  find_library(HDF5_C_LIBRARIES
    NAMES hdf5_D  hdf5 
    PATH_SUFFIXES lib
    PATHS
    ${HDF5_ROOT}
    ${HDF5_DIR}
    NO_DEFAULT_PATH
    )
  set(HDF5_LIBRARIES "${HDF5_C_LIBRARIES}\;${HDF5_CXX_LIBRARIES}")
  
  file(GLOB_RECURSE HDF5_DLLS
    ${HDF5_ROOT}/bin/*.dll
    ${HDF5_ROOT}/bin/*.pdb
    ${HDF5_DIR}/bin/*.dll
    ${HDF5_DIR}/bin/*.pdb
    )

  find_path(HDF5_INCLUDE_DIRS H5Cpp.h
    PATH_SUFFIXES include
    PATHS
    ${HDF5_ROOT}
    ${HDF5_DIR}
    NO_DEFAULT_PATH
    )

elseif(APPLE)
  message(FATAL_ERROR "NOT SUPPORTED YET")
else()

  find_library(HDF5_C_LIBRARIES
    NAMES hdf5 
    PATH_SUFFIXES lib
    PATHS
    ${HDF5_ROOT}
    ${HDF5_DIR}
    "/usr/lib/x86_64-linux-gnu/hdf5/serial/"
    ) 
  find_library(HDF5_CXX_LIBRARIES
    NAMES hdf5_cpp 
    PATH_SUFFIXES lib
    PATHS
    ${HDF5_ROOT}
    ${HDF5_DIR}
    "/usr/lib/x86_64-linux-gnu/hdf5/serial/"
    )
  set(HDF5_LIBRARIES "${HDF5_CXX_LIBRARIES}\;${HDF5_C_LIBRARIES}")
    
  set(HDF5_DLLS "")

  find_path(HDF5_INCLUDE_DIRS H5Cpp.h
    PATH_SUFFIXES include
    PATHS
    ${HDF5_ROOT}
    ${HDF5_DIR}
    "/usr/include/hdf5/serial/"
    )

endif()


find_package(PackageHandleStandardArgs REQUIRED)
find_package_handle_standard_args(HDF5
  REQUIRED_VARS HDF5_INCLUDE_DIRS HDF5_LIBRARIES)

