# Locate KNITRO
#
# This module defines:
#
# TO USE IT MUST SET
#
# GLEW_ROOT:   Must be the folder having GL/glut.h
# or  GLEW_DIR
#
# GLEW_FOUND:       TRUE if the library is available
# GLEW_INCLUDE_DIRS:  Directory containing header files of KNITRO
# GLEW_LIBRARIES:     Libraries to link against when using KNITRO
# GLEW_DLLS:          DLLs required to load the library (only on Windows)
#
# Shayan Hoshyari 21/09/2018
#
#

if(WIN32)

  find_library(GLEW_LIBRARIES
    NAMES glew32 glew32d
    PATH_SUFFIXES lib
    PATHS
    ${GLEW_ROOT}
    ${GLEW_DIR}
    )
  
  find_file(GLEW_DLLS
    NAMES glew32.dll glew32d.dll 
    PATH_SUFFIXES bin
    PATHS
    ${GLEW_ROOT}
    ${GLEW_DIR}
    )

  find_path(GLEW_INCLUDE_DIRS GL/glew.h
    PATH_SUFFIXES include
    PATHS
    ${GLEW_ROOT}
    ${GLEW_DIR}
    )

elseif(APPLE)
  message(FATAL_ERROR "NOT SUPPORTED YET")
else()

    find_library(GLEW_LIBRARIES
    NAMES GLEW GLEWd
    PATH_SUFFIXES lib
    PATHS
    ${GLEW_ROOT}
    ${GLEW_DIR}
    )

  set(GLEW_DLLS "")

  find_path(GLEW_INCLUDE_DIRS GL/glew.h
    PATH_SUFFIXES include
    PATHS
    ${GLEW_ROOT}
    ${GLEW_DIR}
    )

endif()


find_package(PackageHandleStandardArgs REQUIRED)
find_package_handle_standard_args(GLEW
  REQUIRED_VARS GLEW_INCLUDE_DIRS GLEW_LIBRARIES)

