
# TRY TO FIND THE LIBRARIES
set(CLAPACK_LIBS_LIST
  blas libf77 libi77 
)

set(CLAPACK_FOUND_LIBS TRUE)
set(CLAPACK_TARGETS )

foreach(LIB ${CLAPACK_LIBS_LIST})
	set(lib_target DF::CLAPACK::${LIB})
	add_library(${lib_target} STATIC IMPORTED)
	find_library(CLAPACK_LIB_${LIB}
		NAMES ${LIB}
		PATHS
		/usr/local/libs/
	)
	if(CLAPACK_LIB_${LIB})
		set_target_properties(${lib_target} PROPERTIES IMPORTED_LOCATION_RELEASE "${CLAPACK_LIB_${LIB}}")
		set_target_properties(${lib_target} PROPERTIES IMPORTED_LOCATION_RELWITHDEBINFO "${CLAPACK_LIB_${LIB}}")
	else()
		set(CLAPACK_FOUND_LIBS FALSE)
		message(STATUS "Cound not find CLAPACK RELEASE: ${LIB}")	
	endif()
	find_library(CLAPACKd_LIB_${LIB}
		NAMES ${LIB}
		PATHS
		/usr/local/libs/
	)
	if(CLAPACKd_LIB_${LIB})
		set_target_properties(${lib_target} PROPERTIES IMPORTED_LOCATION_DEBUG "${CLAPACKd_LIB_${LIB}}")
	else()
		set(CLAPACK_FOUND_LIBS FALSE)
		message(STATUS "Cound not find CLAPACK DEBUG: ${LIB}")
	endif()
	list(APPEND CLAPACK_TARGETS ${lib_target})
endforeach(LIB)

# print OOQP_LIBRARIES
if(CLAPACK_FOUND_LIBS)
	set(CLAPACK_FOUND TRUE)
else()
  message(STATUS "Cound not find CLAPACK")  
endif()
