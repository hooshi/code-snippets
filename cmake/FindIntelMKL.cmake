#adapted from https://github.com/hanjianwei/cmake-modules
#The MIT License (MIT)
#
#Copyright (c) 2014 Jianwei Han

#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.
# - Find Intel MKL
# Find the MKL libraries
#

include(FindPackageHandleStandardArgs)

find_path(INTEL_COMPOSER_ROOT compiler/include/omp.h
  PATHS ${INTEL_COMPOSER_ROOT})

# Find include dir
find_path( INTEL_MKL_INCLUDE_DIR mkl.h
  PATHS ${INTEL_COMPOSER_ROOT}
  PATH_SUFFIXES mkl/include)

# message("INTEL_MKL_INCLUDE_DIR: " ${INTEL_MKL_INCLUDE_DIR})

# Find libraries

# Handle suffix
set(_MKL_ORIG_CMAKE_FIND_LIBRARY_SUFFIXES ${CMAKE_FIND_LIBRARY_SUFFIXES})

if(WIN32)
  if(INTEL_MKL_STATIC)
    set(MKL_LIBRARY_PRESUFFIX "")
    set(CMAKE_FIND_LIBRARY_SUFFIXES .lib)
  else()
    # This one is buggy
    set(MKL_LIBRARY_PRESUFFIX _dll)
    set(CMAKE_FIND_LIBRARY_SUFFIXES .lib)
  endif()
else()
  if(INTEL_MKL_STATIC)
    set(MKL_LIBRARY_PRESUFFIX "")
    set(CMAKE_FIND_LIBRARY_SUFFIXES .a)
  else()
    set(MKL_LIBRARY_PRESUFFIX "")
    set(CMAKE_FIND_LIBRARY_SUFFIXES .so)
  endif()
endif()


# MKL is composed by four layers: Interface, Threading, Computational and RTL

######################### Interface layer #######################
set(INTEL_MKL_INTERFACE_LIBNAME  mkl_intel_lp64 )
#set(INTEL_MKL_INTERFACE_LIBNAME mkl_intel_ilp64) #use for very large matrices with indices larger than 2^31 - 1

find_library(INTEL_MKL_INTERFACE_LIBRARY
  NAMES ${INTEL_MKL_INTERFACE_LIBNAME}${MKL_LIBRARY_PRESUFFIX}
  PATHS ${INTEL_COMPOSER_ROOT}/mkl/lib/intel64/)


######################## Threading layer ########################
if(INTEL_MKL_MULTI_THREADED)
  set(INTEL_MKL_THREADING_LIBNAME mkl_intel_thread)
else()
  set(INTEL_MKL_THREADING_LIBNAME mkl_sequential)
endif()

find_library(INTEL_MKL_THREADING_LIBRARY
  NAMES ${INTEL_MKL_THREADING_LIBNAME}${MKL_LIBRARY_PRESUFFIX}
  PATHS ${INTEL_COMPOSER_ROOT}/mkl/lib/intel64/)

# message("INTEL_MKL_THREADING_LIBRARY: " ${INTEL_MKL_THREADING_LIBRARY})

####################### Computational layer #####################
find_library(INTEL_MKL_CORE_LIBRARY
  NAMES mkl_core${MKL_LIBRARY_PRESUFFIX}
  PATHS ${INTEL_COMPOSER_ROOT}/mkl/lib/intel64/)

# message("INTEL_MKL_CORE_LIBRARY: " ${INTEL_MKL_CORE_LIBRARY})

#find_library(INTEL_MKL_FFT_LIBRARY mkl_cdft_core# didnt find  cdft in 2016
#    PATHS ${INTEL_COMPOSER_ROOT}/mkl/lib/intel64/)
#find_library(INTEL_MKL_SCALAPACK_LIBRARY mkl_scalapack_core
#    PATHS ${INTEL_COMPOSER_ROOT}/mkl/lib/intel64/)

############################ RTL layer ##########################
if(WIN32)
  set(INTEL_MKL_RTL_LIBNAME libiomp5md)
else()
  #shayan! this should be iomp5 in linux!
  # whoever wrote the part for RTL layer has messed up!
  # it was also buggy in windows
  set(INTEL_MKL_RTL_LIBNAME iomp5)
endif()

# Shayan: This one can have or not have the suffix, it is tricky.
find_library(INTEL_MKL_RTL_LIBRARY
  NAMES
  ${INTEL_MKL_RTL_LIBNAME}${MKL_LIBRARY_PRESUFFIX}
  ${INTEL_MKL_RTL_LIBNAME}
  PATHS
  ${INTEL_COMPOSER_ROOT}/lib/intel64
  ${INTEL_COMPOSER_ROOT}/compiler/lib/intel64
 )

# message("INTEL_MKL_RTL_LIBRARY: " ${INTEL_MKL_RTL_LIBRARY})
# message("I want to find : " ${INTEL_MKL_RTL_LIBNAME}${MKL_LIBRARY_PRESUFFIX})
# message("or : " ${INTEL_MKL_RTL_LIBNAME})

set(INTEL_MKL_LIBRARY
  ${INTEL_MKL_INTERFACE_LIBRARY}
  ${INTEL_MKL_THREADING_LIBRARY}
  ${INTEL_MKL_CORE_LIBRARY}
  ${INTEL_MKL_RTL_LIBRARY})
# didnt find  ${INTEL_MKL_SCALAPACK_LIBRARY} and ${MKL_FFT_LIBRARY} in 2016

set(INTEL_MKL_MINIMAL_LIBRARY
  ${INTEL_MKL_INTERFACE_LIBRARY}
  ${INTEL_MKL_THREADING_LIBRARY}
  ${INTEL_MKL_CORE_LIBRARY}
  ${INTEL_MKL_RTL_LIBRARY})

set(CMAKE_FIND_LIBRARY_SUFFIXES
  ${_MKL_ORIG_CMAKE_FIND_LIBRARY_SUFFIXES})

# Shayan: now find the dlls
if(WIN32)
  file(GLOB_RECURSE INTEL_MKL_DLLS
    ${INTEL_COMPOSER_ROOT}/redist/intel64/*.dll
    ${INTEL_COMPOSER_ROOT}/redist/intel64/*.pdb
    ${INTEL_COMPOSER_ROOT}/redist/intel64_win/mkl/*.dll
    ${INTEL_COMPOSER_ROOT}/redist/intel64_win/mkl/*.pdb
    )
endif()

find_package_handle_standard_args(INTEL_MKL
  DEFAULT_MSG
  INTEL_MKL_INCLUDE_DIR
  INTEL_MKL_LIBRARY
  INTEL_MKL_MINIMAL_LIBRARY)

if(INTEL_MKL_FOUND)
  set(INTEL_MKL_INCLUDE_DIRS ${INTEL_MKL_INCLUDE_DIR})
  set(INTEL_MKL_LIBRARIES ${INTEL_MKL_LIBRARY})
  set(INTEL_MKL_MINIMAL_LIBRARIES ${INTEL_MKL_LIBRARY})
endif()
