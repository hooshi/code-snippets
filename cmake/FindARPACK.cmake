# Locate ARPACK
#
# This module defines:
#
# TO USE IT MUST SET
#
# ARPACK_ROOT:   
# or  ARPACK_DIR
#
# ARPACK_FOUND:       TRUE if the library is available
# ARPACK_INCLUDE_DIRS:  Directory containing header files of KNITRO
# ARPACK_LIBRARIES:     Libraries to link against when using KNITRO
# ARPACK_DLLS:          DLLs required to load the library (only on Windows)
#
# Shayan Hoshyari 21/09/2018
#
#

if(WIN32)

  find_library(ARPACK_LIBRARIES
    NAMES arpack_x64.lib
    PATH_SUFFIXES lib
    PATHS
    ${ARPACK_ROOT}
    ${ARPACK_DIR}
    )
  
  find_file(ARPACK_DLLS
    NAMES arpack_x64.dll 
    PATH_SUFFIXES bin
    PATHS
    ${ARPACK_ROOT}
    ${ARPACK_DIR}
    )

  find_path(ARPACK_INCLUDE_DIRS lapackc.h
    PATH_SUFFIXES include
    PATHS
    ${ARPACK_ROOT}
    ${ARPACK_DIR}
    )

elseif(APPLE)
  message(FATAL_ERROR "NOT SUPPORTED YET")
else()
  
  find_library(ARPACK_LIBRARIES
    NAMES arpack
    PATH_SUFFIXES lib
    PATHS
    ${ARPACK_ROOT}
    ${ARPACK_DIR}
    )
  
  set(ARPACK_DLLS "")

  # I am ashamed of this hack !
  set(ARPACK_INCLUDE_DIRS "/usr/include/arpack++")
  

  endif()


  find_package(PackageHandleStandardArgs REQUIRED)
  find_package_handle_standard_args(ARPACK
    REQUIRED_VARS ARPACK_INCLUDE_DIRS ARPACK_LIBRARIES)

