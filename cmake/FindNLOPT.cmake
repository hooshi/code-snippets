# Locate NLOPT
#
# This module defines:
#
# TO USE IT MUST SET
#
# NLOPT_ROOT:   
# or  NLOPT_DIR
#
# RETURNS
# NLOPT_FOUND:       
# NLOPT_INCLUDE_DIRS: 
# NLOPT_LIBRARIES:    
# NLOPT_DLLS:          
#
# Shayan Hoshyari 21/09/2018
#
#

if(WIN32)

  find_library(NLOPT_LIBRARIES
    NAMES  nlopt
    PATH_SUFFIXES lib
    PATHS
    ${NLOPT_ROOT}
    ${NLOPT_DIR}
    )
  
  find_file(NLOPT_DLLS
    NAMES nlopt.dll nlopt.pdb 
    PATH_SUFFIXES bin
    PATHS
    ${NLOPT_ROOT}
    ${NLOPT_DIR}
    )

  find_path(NLOPT_INCLUDE_DIRS nlopt.h
    PATH_SUFFIXES include
    PATHS
    ${NLOPT_ROOT}
    ${NLOPT_DIR}
    )

elseif(APPLE)
  message(FATAL_ERROR "NOT SUPPORTED YET")
else()

  find_library(NLOPT_LIBRARIES
    NAMES  nlopt
    PATH_SUFFIXES lib
    PATHS
    ${NLOPT_ROOT}
    ${NLOPT_DIR}
    )
  
  set(NLOPT_DLLS "")

  find_path(NLOPT_INCLUDE_DIRS nlopt.h
    PATH_SUFFIXES include
    PATHS
    ${NLOPT_ROOT}
    ${NLOPT_DIR}
    )

endif()


find_package(PackageHandleStandardArgs REQUIRED)
find_package_handle_standard_args(NLOPT
  REQUIRED_VARS NLOPT_INCLUDE_DIRS NLOPT_LIBRARIES)

