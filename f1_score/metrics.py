import numpy as np

class Metrics:
    G = 0 # Good (something to use to refer to category 1)
    B = 1 # Bad  (something to use to refer to category 2)
    def __init__(self):
        self.reset()

    def reset(self):
        self.cnt = np.zeros((2, 2))

    def update(self, label, pred, th):
        n = len(label)
        for i in range(n):
            l = int(label[i])
            p = int(pred[i]>th)
            self.cnt[l, p] = self.cnt[l, p]+1

    def update_with_slack(self, label, pred, th):
        """For every G label, we accept their neighbours as being anything.

        """
        n = len(label)
        for i in range(n):
            inext = (i+1)%n;
            iprev = (i-1+n)%n;
            
            lnext = int(label[inext]); pnext = int(pred[inext]>th)
            lprev = int(label[iprev]); pprev = int(pred[iprev]>th)
            l = int(label[i]);         p = int(pred[i]>th)
            
            if (not l) and (lprev or lnext):
                self.cnt[p,p] = self.cnt[p,p] + 1
            elif l and (pprev or pnext or p):
                self.cnt[p,p] = self.cnt[p,p] + 1
            else:
                self.cnt[l, p] = self.cnt[l, p]+1

    def get_total(self):
        return int(self.cnt.sum())

    def get_precision(self):
        return self.cnt[1, 1]*1.0/np.sum(self.cnt[:, 1])

    def get_recall(self):
        return self.cnt[1, 1]*1.0/np.sum(self.cnt[1, :])

    def get_f1(self):
        f1 = 2.0/(1.0/self.get_precision()+1.0/self.get_recall())
        f1 = 0 if np.isnan(f1) else f1
        return f1

    def get_fp(self):
        return self.cnt[B,G]

    def get_fn(self):
        return self.cnd[G,B]

    def get_tp(self):
        return self.cnd[G,G]

    def get_tn(self):
        return self.cnd[B,B]

    def get_info(self):
        return "prec={:.3f}, recall={:.3f}, f1={:.3f}".format(
            self.get_precision(), self.get_recall(), self.get_f1())

class Stats:
    def __init__(self, thr, pr, rc, f1):
        self.thr = thr 
        self.pr =  pr
        self.rc =  rc
        self.f1 =  f1
        
class StatsArray:
    def __init__(self, stats_dict):
        self.thr = stats_dict['threshold']
        self.pr = stats_dict['precision']
        self.rc = stats_dict['recall']
        self.f1 = stats_dict['f1']

    def at_idx(self, idx):
        thr = self.thr[idx]
        pr = self.pr[idx]
        rc = self.rc[idx]
        f1 = self.f1[idx]
        return Stats(thr, pr, rc, f1)
    
    def best(self):
        idx = np.argmax(self.f1)
        return self.at_idx(idx)

    def at_thr(self, value):
        idx = np.searchsorted(self.thr, value, side='left')
        return self.at_idx(idx)
    
# =========================================================
# Test code
# =========================================================
def test(ytrue, yproba):
    
    precision = []
    recall = []
    f1 = []
    thresholds = []
    m = Metrics()
    for i in range(50):
        th = float(i)/50
        m.reset()
        m.update(ytrue, yproba, th)
        print('th={:.2f}, prec={:.3f}, recall={:.3f}, f1={:.3f}'.format(th, m.get_precision(), m.get_recall(), m.get_f1()))
        precision.append(m.get_precision())
        recall.append(m.get_recall())
        f1.append(m.get_f1())
        thresholds.append(th)

    stats = {}
    stats['threshold'] = np.array(thresholds)
    stats['precision'] = np.array(precision)
    stats['recall'] = np.array(recall)
    stats['f1'] = np.array(f1)

    return stats

if __name__ == '__main__':
    ytrue = [0,0,0,0,0,1,1,1,1]
    yproba = [0.2,0.3,0.9,0.1,0.4,0.8,0.2,0.6,0.8]
    test(ytrue, yproba)
