#include <fstream>
#include <cassert>
#include <cstring>
#include "hdf5_io.hpp"

namespace hdf5io
{

File::File(const std::string & fname, const File_mode open_mode, bool is_col_major)
: _file_mode(open_mode)
, _file()
, _written_data()
, _is_col_major(is_col_major)
{
  switch(_file_mode)
  {
  case FILE_MODE_READ: //
  case FILE_MODE_APPEND: //
    _file.reset( new H5::H5File(fname, H5F_ACC_RDWR) );
    break;
  case FILE_MODE_WRITE: //
    _file.reset( new H5::H5File(fname, H5F_ACC_TRUNC) );
    break;
  }
}

File::~File()
{
  if(_written_data.size())
  {
    const int bs = 256;

    std::vector<char> data_names(bs * _written_data.size());
    for(int i = 0; i < (int)_written_data.size(); ++i)
    {
      sprintf(data_names.data() + i * bs, "%s", _written_data[i].c_str());
    }

    int RANK = 2;
    std::array<hsize_t, 2> dim = check_col_major({_written_data.size(),bs});
    H5::DataSpace data_space(RANK, dim.data());
    H5::DataSet dataset = _file->createDataSet("/names", H5::PredType::NATIVE_CHAR, data_space);
    dataset.write(data_names.data(), H5::PredType::NATIVE_CHAR);
  }

  int RANK = 1;
  std::array<hsize_t, 1> dim = {1};
  H5::DataSpace data_space(RANK, dim.data());
  H5::DataSet dataset = _file->createDataSet("/is_col_major", H5::PredType::NATIVE_UCHAR, data_space);
  char tmp = _is_col_major;
  dataset.write(&tmp, H5::PredType::NATIVE_UCHAR);
}

void
File::write_vector(const std::string & dname, const double * data, const int size)
{
  int RANK = 1;
  std::array<hsize_t,1> dim = {hsize_t(size)};
  H5::DataSpace data_space(RANK, dim.data());
  H5::DataSet dataset = _file->createDataSet(dname, H5::PredType::NATIVE_DOUBLE, data_space);
  dataset.write(data, H5::PredType::NATIVE_DOUBLE);
  _written_data.push_back(dname);
}

void
File::write_matrix(const std::string& dname, const double *data, const int n_rows, const int n_cols)
{
  int RANK = 2;
  std::array<hsize_t,2> dim = check_col_major({hsize_t(n_cols),hsize_t(n_rows)});
  H5::DataSpace data_space(RANK, dim.data());
  H5::DataSet dataset = _file->createDataSet(dname, H5::PredType::NATIVE_DOUBLE, data_space);
  dataset.write(data, H5::PredType::NATIVE_DOUBLE);
  _written_data.push_back(dname);
}

std::array<hsize_t, 2> File::check_col_major(std::array<hsize_t, 2> in)
{
  if(_is_col_major)
    std::swap(in[0], in[1]);
  return in;
}


} // End of hdf5io



// test code
int main()
{
  hdf5io::File file("Shayan.h5", hdf5io::FILE_MODE_WRITE);

  std::vector<double> some_numbers = {12,3,4};
  std::vector<double> a_matrix = {12,3,4, 8, 7,6,1,2,8};
  file.write_vector("some_numbers", some_numbers.data(), int(some_numbers.size()));
  file.write_matrix("a_matrix", a_matrix.data(), 3,3);

  return 0;
}