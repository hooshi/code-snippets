// Basic HDF5 matrix and vector I/O
// Author: Shayan Hoshyari

#ifndef HDF5_IO_IS_INCLUDED
#define HDF5_IO_IS_INCLUDED

#include <memory>
#include <string>
#include <vector>

#include <H5Cpp.h>

namespace hdf5io
{

enum File_mode
{
  FILE_MODE_READ = 0,
  FILE_MODE_APPEND,
  FILE_MODE_WRITE
};

class File
{
public:
  File(const std::string & fname, const File_mode open_mode, bool is_col_major=true);
  ~File();

  void write_vector(const std::string & dname, const double *, const int size);
  void write_matrix(const std::string & dname, const double *, const int n_rows, const int n_cols);

private:
  File_mode _file_mode;
  std::unique_ptr<H5::H5File> _file;
  std::vector<std::string> _written_data;
  bool _is_col_major;
  std::array<hsize_t,2> check_col_major(std::array<hsize_t,2>);
};

} // end of hdf5io



#endif /*  HDF5_IO_IS_INCLUDED */