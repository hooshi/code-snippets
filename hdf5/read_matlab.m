names = char( hdf5read_me('Shayan.h5','/names') );
a = {};

for i=1:size(names,2)
a{end+1} = hdf5read_me('Shayan.h5',names(:,i)) ;
end


function out = hdf5read_me(fn, vn)
out = hdf5read(fn,vn);
n_rows = hdf5read(fn,sprintf('%s_nrows',vn));
out = reshape(out, n_rows, []);
end