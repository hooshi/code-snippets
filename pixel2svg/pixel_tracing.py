#!/usr/bin/env python3
"""png_bw_tools.py

Basic pre-processing for png images.
Convert png to binary colors.
Scale png.
Trace the polygon boundary out of the png image.

NOTE: NumPy indexing is like matrix indexing An image is processed
       exactly like a matrix.  READ THIS
       http://spencermortensen.com/articles/bezier-circle/

"""

from __future__ import print_function
import sys
import copy

import imageio
import numpy as np
import itertools

######################################################################
##                    Remove alpha mask
######################################################################
def remove_alpha(img):
    ## If alpha exists, remove it
    if( (img.ndim == 3) and (img.shape[2] == 4)):
        img2 = np.delete(img, 3, 2)
        mask = img[:,:,3] < 0.1
        img2[mask] = 255
        return img2
    else:
        return img

######################################################################
##                  Convert to binary image
######################################################################
def grayscale(img, ww=(0.299, 0.587, 0.114)):
    if( img.ndim == 3):
        img = remove_alpha(img)
        fimg = img.astype('float')
        gray = ww[0]*fimg[:,:,0] + ww[1]*fimg[:,:,1] + ww[2]*fimg[:,:,2]
        return gray.astype('uint8')
    else:
        return img


######################################################################
##                  Threshold the image
######################################################################
def threshold(img, thr=125):
    """Black is converted to 0 normally, i.e. value < thr if
    selfhould_invert == True, white will be converted to 0, i.e.
    value > thr

    """
    ## If the image is in color make it grayscale
    if (img.ndim > 2): img = grayscale(img)
    return (img > thr)

######################################################################
##                      Finding bounding box
######################################################################
def get_bbox(img, thr=125):
    ## only for graysale
    assert(img.ndim==2)

    ## False is considered inside the polygon
    ## Pretty strange(!) I know, maybe I should change it.
    a = np.where(img == False)
    bbox = (np.min(a[0]), np.max(a[0])), (np.min(a[1]), np.max(a[1]))
    return bbox

######################################################################
##                          Cropping
######################################################################
def crop(img, rows=(0,-1), cols=(0,-1)):
    return img[rows[0]:rows[1] , cols[0]:cols[1]]

def crop_bbox(img):
    bbox = get_bbox(img)
    img2 = crop(img, (bbox[0][0], bbox[0][1]+1),
               (bbox[1][0], bbox[1][1]+1) )
    return img2


######################################################################
##                     Inverse nearest neighbour
######################################################################
def inverse_nn(img, scale):
    if(scale==1): return img

    shape = list(img.shape)
    shape[0] = shape[0] / scale
    shape[1] = shape[1] / scale

    ans = np.zeros(shape, img.dtype)

    for ii in range(0, shape[0]):
        for jj in range(0, shape[1]):
            
            JJ = jj*scale;
            II = ii*scale

            pixel = crop(img, (II, II+scale), (JJ, JJ+scale))
            maxval = np.amax( pixel )
            ans[ii,jj] = maxval

    return ans         

######################################################################
##                     Read the image
######################################################################
def read_img(fname):
    return imageio.imread(fname)
    

######################################################################
##                     Extract boundary polygons
######################################################################

# +1 means reutrn CCW
DIRECTION_RIGHT = 0
DIRECTION_UP    = 1
DIRECTION_LEFT  = 2
DIRECTION_DOWN  = 3

def as_tuple(point):
    return (point[0], point[1])

class SeparatingEdge:
    # Vertices are just numpy vectors
    # normal is the inward pointing normal direction
    # tangent is the tangent pointing direction
    def __init__(self, origin, tangent, outward_normal):
        assert  (tangent >= 0) and (tangent <= 3)
        assert  (outward_normal >= 0) and (outward_normal <= 3)

        self._origin = np.array(origin, dtype=int).reshape(2)
        self._tangent = tangent
        self._outward_normal = outward_normal
        self._is_active = True
        self.obeys_right_hand_rule() # test

    def is_active(self):
        return self._is_active;
    def activate(self):
        self._is_active = True
    def deactivate(self):
        self._is_active = False

    # When obeys rotating tangent 90 CCW gives outward_normal
    def obeys_right_hand_rule(self):
        if( SeparatingEdge.rotate_ccw(self._outward_normal) == self._tangent ):
            return True
        elif( SeparatingEdge.rotate_ccw(self._tangent) == self._outward_normal ):
            return False
        else:
            assert(0)

    def origin(self):
        return self._origin
        
    def dest(self):
        tangent_vec = SeparatingEdge.dir_to_vec(self._tangent)
        return tangent_vec + self.origin()

    def str_info(self):
        return "{} {}".format(self.origin(), self.dest())

    @staticmethod
    def dir_to_vec(direction):
        answer = {
            DIRECTION_RIGHT : [0,1],
            DIRECTION_UP    : [-1,0],
            DIRECTION_LEFT  : [0,-1],
            DIRECTION_DOWN  : [1,0],
        }[direction]
        return np.array(answer)

    @staticmethod
    def rotate_ccw(direction):
        return (direction+1)%4

def dprint(*args):
    # print (args)
    pass
    
## Cell and vertex numbering is such that:
## 
##  v:(i,j)------v:(i,j+1)
##  |               |
##  | c:(i,j)       |
##  |               |
##  v:(i+1,j)----v:(i+1,j+1)
## Also, 0,0 is the top left corner. 
def extract_polygon(isin):
    boundary_edges = []
    vertex_to_edge_multi_map  = {}

    ## -------------------
    ##  Loop over all pixels.
    ##  For every pixel which is inside, get all its edges (CCW).
    ##  If an edge lies on the boundary, put it in the map.
    ## -------------------
    h = img.shape[0]
    w = img.shape[1]
    for (i,j) in itertools.product(range(h), range(w)):
        
        # We want the normal to point outwards. Therefore, only
        # process when the quads inside.
        isinme = isin[i,j] 
        if( not isinme ): continue

        ## Edges of the quad (left, down, right, up)
        quad_edges = [
            SeparatingEdge( [i,j], DIRECTION_DOWN, DIRECTION_LEFT ),
            SeparatingEdge( [i+1,j], DIRECTION_RIGHT, DIRECTION_DOWN ),
            SeparatingEdge( [i+1,j+1], DIRECTION_UP, DIRECTION_RIGHT ),
            SeparatingEdge( [i,j+1], DIRECTION_LEFT, DIRECTION_UP ),
        ]

        ## Are neighbours inside (left, down, right, up)
        isneighin = [False] * 4
        isneighin[0] = (isinme if j==0   else   isin[(i)  ,j-1] )
        isneighin[1] = (isinme if i==h-1 else   isin[(i+1),j  ] )
        isneighin[2] = (isinme if j==w-1 else   isin[(i)  ,j+1] )
        isneighin[3] = (isinme if i==0   else   isin[(i-1), j ] )

        # Add the separating boundary edges to the map
        # We want the normal to point outwards. Therefore,
        # add when me is out,
        def _process_edge(ie):
            ee = quad_edges[ie]
            #
            ige = len(boundary_edges)
            boundary_edges.append( ee )
            #
            origin = as_tuple(ee.origin())
            if (origin in vertex_to_edge_multi_map):
                vertex_to_edge_multi_map[origin].append(ige)
            else:
                vertex_to_edge_multi_map[origin] = [ige]
        #### outside neigbours share separating edges with us
        if ( not isneighin[0] ): _process_edge(0)
        if ( not isneighin[1] ): _process_edge(1)
        if ( not isneighin[2] ): _process_edge(2)
        if ( not isneighin[3] ): _process_edge(3)

    ## Make sure all edges are right hand rule obeyers
    for be in boundary_edges:
        assert( be.obeys_right_hand_rule() )

    ## Print all edges
    for vert in vertex_to_edge_multi_map.keys():
        dprint("vert", vert, ":")
        for ie in vertex_to_edge_multi_map[vert]:
            ee = boundary_edges[ie]
            dprint("   ", ee.origin(), ee.dest())
            assert( as_tuple(ee.origin()) == vert )

    ## -------------------
    ## Extract Closed Polygons
    ##  -------------------
    polygons = []
    def _next_be(be):
        dprint("Looking for next for {}".format(be.str_info()))
        end_vertex = as_tuple(be.dest())
        dprint("Edge ends at {}".format(end_vertex))
        if end_vertex not in vertex_to_edge_multi_map:
            dprint("End point has no emanating separating edges")
            return -1
        for inext_be in vertex_to_edge_multi_map[end_vertex]:
            next_be = boundary_edges[inext_be]
            if( next_be.is_active() ):
                dprint("Emanating Edge {} is active,  returning".format(next_be.str_info()))
                return inext_be
            dprint("Emanating Edge {} de-active going to next")
        dprint("Emanating Edge not found")
        return -1
            
    def _extract_contour(be):
        # If the boundary edge is off, then stop
        if( not be.is_active() ): return

        dprint("Starting from {}".format(be.str_info()))
        polygons.append([])
        polygons[-1].append( be.origin() );  be.deactivate()
        dprint("First point is {}".format(be.origin()))
        
        
        # Remember the start vertex
        start_vertex = as_tuple(be.origin())
        # Start tracing
        while True:
            # Find the next candidate
            ibe = _next_be(be)
            if( ibe == -1 ):
                dprint( "Open path at {}".format( be.origin() ) )
                polygons.pop()
                return
            be = boundary_edges[ibe]
            dprint("Next edge is {}".format(be.str_info()))
            polygons[-1].append( be.origin() );  be.deactivate()
            dprint("Next point is {}".format(be.origin()))
            if( as_tuple(be.dest()) == start_vertex ):
                polygons[-1] = np.array(polygons[-1])
                break;
       
    # start extracting
    for be in boundary_edges:  _extract_contour(be)
    
    
    ## -------------------
    ## Finish
    ##  -------------------
    return polygons

def is_polygon_a_hole(p):
    area = 0
    for i in range(p.shape[0]):
        im = i-1 if i > 0 else p.shape[0]-1
        area += (p[i,0] - p[im,0])*(p[i,1] + p[im,1])*0.5

    # at this point I don't really remember which is which
    # Trial and error
    return area <0

if __name__ == '__main__':
    from stitcher import Stitcher
    import sys
    import matplotlib.pyplot as plt
    
    fname = 'example.png'
    thr = 125
    if len(sys.argv) > 1: fname = sys.argv[1]
    if len(sys.argv) > 2: thr = int(sys.argv[2])
    img = read_img(fname)
    img = threshold(img, thr)
    plt.imshow(img, cmap=plt.get_cmap('Greys_r'))
    plt.show()
    
    polys = extract_polygon( (img==0) )
    dprint( polys )
    amax = -10000
    amin =  10000
    for poly in polys:
        poly[:,[1,0]] =   poly[:,[0,1]]
        amax = max(amax, np.amax(poly.flatten()))
        amin = min(amin, np.amin(poly.flatten()))
    st = Stitcher('stitched.svg', dim=(amax-amin)+10 )
    for poly in polys:
        poly = poly + amin
        if( is_polygon_a_hole(poly) ):
            dprint("hole")
            st.add_polygon( poly, rgb=(255,255,255) )
        else:
            dprint("no-hole")
            st.add_polygon( poly, rgb=(0,0,0) )
            
    st.finish()

