#!/usr/bin/env python

import os
import sys
import cairo
import rsvg
import numpy as np

## ====================================================
##       Stitcher, stitches svg files
## ====================================================

class Stitcher:
    def __init__(self, outputfile, dim=500, logfilename='stitch.log'):
        self._cairo_surface = cairo.SVGSurface(outputfile, dim, dim)
        self._cairo_ctx = cairo.Context(self._cairo_surface)
        self._logger = open('stitch.log', 'wb')

    def finish(self):
        self._cairo_surface.finish()
        self._logger.close()

    def logger(self):
        return self._logger

    def cairo_ctx(self):
        return  self._cairo_ctx
    
    def add_svg(self, inputfile):
        if (os.path.isfile(inputfile)):
            svg = rsvg.Handle(file=inputfile)
            svg.render_cairo(self._cairo_ctx)
        else:
            self.logger().write("Missing: {} \n".format(inputfile) )
            

    def add_polygon(self, points, rgb=(0,0,0)):
        points = np.array(points, dtype=float).reshape(-1,2)
        
        cr = self.cairo_ctx()        
        cr.set_source_rgb(rgb[0], rgb[1], rgb[2])
        cr.move_to(points[-1,0], points[-1,1])
        for p in points:
            cr.line_to(p[0], p[1])
        cr.fill()
        
            

def test():
    st = Stitcher('stitched.svg', dim=400)
    st.add_polygon( [0,0, 100,0, 100,100, 0,100] )
    st.finish()
    
if __name__ == '__main__':
    main()
