#!/usr/bin/env python3

# https://stackoverflow.com/questions/49978689/setting-specific-rgb-values-using-image-function-in-r?rq=1

import numpy as np
from PIL import Image


def remove_green(in_filename, out_filename):
    # Open image and make RGB and HSV versions
    RGBim = Image.open(in_filename).convert('RGB')
    HSVim = RGBim.convert('HSV')
    # Make numpy versions
    RGBna = np.array(RGBim)
    HSVna = np.array(HSVim)
    print(RGBna.shape)
    # Extract R, G and B
    R = RGBna[:,:,0]
    G = RGBna[:,:,1]
    B = RGBna[:,:,2]
    H = HSVna[:,:,0]
    S = HSVna[:,:,1]
    V = HSVna[:,:,2]
    
    H = H / 255 * 360
    S = S / 255 * 100
    V = V / 255 * 100

    def approx(V, value):
        return (np.abs(V - value)<10)

    # Find the damn green pixels
    # green = np.where( 
    #      ( approx(R, 173) & approx(G, 255) & approx(B, 195) )
    #      | ( approx(R, 174) & approx(G, 255) & approx(B, 193) )
    #      | ( approx(R, 177) & approx(G, 255) & approx(B, 194) )
    #      | ( approx(R, 171) & approx(G, 254) & approx(B, 191) )
    #      | ( approx(R, 67) & approx(G, 98) & approx(B, 75) )
    # )
    green = np.where( 
          ( approx(H, 134) & approx(S, 32) & approx(V, 98) )
          | ( approx(H, 138) & approx(S, 11) & approx(V, 97) )
          | ( approx(H, 145) & approx(S, 5) & approx(V, 96) )
          | ( approx(H, 114) & approx(S, 5) & approx(V, 95) )
    #      | ( approx(R, 177) & approx(G, 255) & approx(B, 194) )
    #      | ( approx(R, 171) & approx(G, 254) & approx(B, 191) )
    #      | ( approx(R, 67) & approx(G, 98) & approx(B, 75) )
     )

    # Make all green pixels black in original image
    RGBna[green] = [255,255,255]
    #RGBna[green] = [255,0,0]

    print(V)
    
    count = green[0].size
    print("Pixels matched: {}".format(count))
    Image.fromarray(RGBna).save(out_filename)


remove_green('out0550.png', 'out0550_greenremoval.png')
