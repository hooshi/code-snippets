#!/usr/bin/env python3
# coding: utf-8

#
# Generate a random word from a pre-specified set.
# Useful for learning a language.
#

import random
import datetime

# =============================================================
#                        RANDOM MEMBER
# =============================================================
def fix_seed( value ):
    random.seed( value )

def randomize_seed( ):
    random.seed( datetime.datetime.now() )
    
def rand_int( ub ):
    return random.randint(0  ,ub-1)

def rand_member( members ):
    rand_member_id = rand_int( len(members) )
    return members[ rand_member_id ]

# =============================================================
#              Certain value of list of tuples
# =============================================================
def list_get_col( list_2d, col_id ):
    return [m[col_id] for m in list_2d]

# =============================================================
#                        LANGUAGES
# =============================================================

LA_FRENCH = 0
LA_ENGLISH = 1

# =============================================================
#                        LISTS
# =============================================================

# https://www.thefrenchexperiment.com/learn-french/days-of-week
LI_days = [
    ('lundi', 'Monday'),
    ('mardi',  'Tuesday'),
    ('mercredi', 'Wednesday'),
    ('jeudi', 'Thursday'),
    ('vendredi', 'Friday'),
    ('samedi', 'Saturdy'),
    ('dimanche', 'Sunday'),
]

# https://www.rocketlanguages.com/french/time/months-in-french
LI_seasons = [
    ('Printemps', 'Spring'),
    ('Été', 'Summer'),
    ('Automne', 'Autumn'),
    ('Hiver', 'Winter'),
]

# https://www.rocketlanguages.com/french/time/months-in-french
LI_months = [
    ('décembre', 'December'),
    ('janvier', 'January'),
    ('février', 'February'),
    ('mars', 'March'),
    ('avril', 'April'),
    ('mai', 'May'),
    ('juin', 'June'),
    ('juillet', 'July'),
    ('août', 'August'),
    ('septembre', 'September'),
    ('octobre', 'October'),
    ('novembre', 'November'),
]

# =============================================================
#                             drivers
# =============================================================

def DR_random_number( ub=70 ):
    print( "NUMBER: ", rand_int( ub ) )


def DR_random_month( lang ):
    months = list_get_col(LI_months, lang)
    print( "MONTH: ", rand_member( months ) )

def DR_random_season( lang ):
    seasons = list_get_col(LI_seasons, lang)
    print( "MONTH: ", rand_member( seasons ) )

def DR_random_day( lang ):
    seasons = list_get_col(LI_days, lang)
    print( "DAY: ", rand_member( seasons ) )

# =============================================================
#                             MAIN
# =============================================================

#DR_random_number()
#DR_random_season( LA_FRENCH )
#DR_random_season( LA_ENGLISH )
#DR_random_month( LA_FRENCH )
#DR_random_month( LA_ENGLISH )
DR_random_day( LA_FRENCH )
#DR_random_day( LA_ENGLISH )
