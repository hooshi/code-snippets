    
import sys
import copy
import math
import pickle
import copy

import numpy as np

from PyQt5 import QtWidgets, QtCore

# ======================================================
#       All the buttons
# ======================================================
class _QHLine(QtWidgets.QFrame):
    def __init__(self):
        super(_QHLine, self).__init__()
        self.setFrameShape(QtWidgets.QFrame.HLine)
        self.setFrameShadow(QtWidgets.QFrame.Sunken)
        
class _Gizmos:
    def __init__(self, frontend):
        
        self.frontend = frontend
        
        # Something to hold things together
        self.central_widget = QtWidgets.QWidget()
        self.central_widget.setLayout(  QtWidgets.QVBoxLayout() )
        self.central_widget.layout().setContentsMargins(0,0,0,0)
        self.central_widget.layout().setSpacing( 0 )

        # Input box
        self.in_textbox = QtWidgets.QPlainTextEdit()
        self.central_widget.layout().addWidget( self.in_textbox )
        self.in_textbox.resize(280,120)

        # Output box
        self.output_textbox = QtWidgets.QPlainTextEdit()
        self.central_widget.layout().addWidget( self.output_textbox )
        self.output_textbox.resize(280,120)
        
        # Button - generate
        self.generate_button = QtWidgets.QPushButton()
        self.generate_button.setText("Trim")
        self.central_widget.layout().addWidget( self.generate_button )

        self.generate_button.clicked.connect(self.on_generate_clicked)


        
    def on_generate_clicked( self ):
        text = copy.copy( self.in_textbox.toPlainText() )
        text = text.replace("\t", " ")
        text = text.replace(",", "/")
        text = text.replace("\n", ", ")
        self.output_textbox.document().setPlainText(text)


# ======================================================
#       Frontend object
# ======================================================
class Frontend:

    def __init__(self):
        self.main_window = None
        # All the buttons
        self.gizmos = _Gizmos( self )

        # Set the main window
        self.main_window = QtWidgets.QMainWindow(parent=None)
        self.main_window.setCentralWidget( QtWidgets.QFrame() )
        self.main_window.centralWidget().setLayout( QtWidgets.QHBoxLayout() )
        
        # BUTTONS
        self.main_window.centralWidget().layout().addWidget( self.gizmos.central_widget)
        
        
    def show( self ):
        self.main_window.show()

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    frontend = Frontend()
    frontend.show()
    sys.exit(app.exec_())

