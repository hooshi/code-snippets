# REF: http://pages.cs.wisc.edu/~gfung/GeneralL1/L1_approx_bounds.pdf

import numpy as np
import matplotlib.pyplot as plt


def selu(x, alpha):
    return x + 1. / alpha * np.log( 1 + np.exp( - alpha * x ) ) 

eps = 1e-5
alpha = 1000

func_abs = lambda x: np.abs(x)
func_sqrt = lambda x: np.sqrt(x*x + eps)
func_selu = lambda x: selu(x,alpha) + selu(-x,alpha)

x = np.linspace(-0.1,0.1,300)
#plt.plot(x, func_abs(x)  - func_sqrt(x))
plt.plot(x, func_abs(x))
plt.plot(x, func_sqrt(x))
plt.plot(x, func_selu(x))


plt.show()
