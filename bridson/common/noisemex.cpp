#include <assert.h>
#include "mex.h"
#include "noise.h"

// Not thread-safe, but I don't think MATLAB is multithreaded.
static Noise2 noise2;
static Noise3 noise3;

// matlab call: 
// noise(x,y,z) for Noise3
// noise(x,y) for Noise2
// where x is a matrix of [1 x L] and y is a matrix of [1 x M] and z (if exists) is a matrix of [1 x N]
// The result is a matrix of [L x M x N] for Noise3 or [L x M] for Noise2
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
   // check argument counts
   if((nrhs>3)||(nrhs<2)) mexErrMsgTxt("Wrong number of input arguments: need (x y z) or (x y)");
   if(nlhs>1) mexErrMsgTxt("Too many output arguments (should just be noise values)");

   // check on arguments
   if(!mxIsDouble(prhs[0])) mexErrMsgTxt("Only can take real, double-precision, dense x");
   if(!mxIsDouble(prhs[1])) mexErrMsgTxt("Only can take real, double-precision, dense y");
   if((mxGetM(prhs[0])!=1)||(mxGetM(prhs[1])!=1)) mexErrMsgTxt("Expecting input vector/s of 1 row");
   if (nrhs==3) {
	   if(!mxIsDouble(prhs[2])) mexErrMsgTxt("Only can take real, double-precision, dense z");
	   if(mxGetM(prhs[2])!=1) mexErrMsgTxt("Expecting input vector/s of 1 row");
   }
   int L=1, M=1, N=1;
   L=mxGetN(prhs[0]);
   M=mxGetN(prhs[1]);
   if (nrhs==3) {
	   N=mxGetN(prhs[2]);
   }

   const double *x, *y, *z;
   x=mxGetPr(prhs[0]);
   y=mxGetPr(prhs[1]);
   if (nrhs==3) {
	   z=mxGetPr(prhs[2]);
   }

   // evaluate
   const int dims3[3]={L,M,N};
   const int dims2[2]={L,M};

   if (nrhs==3) plhs[0] = mxCreateNumericArray (3, dims3, mxDOUBLE_CLASS, mxREAL);
   if (nrhs==2) plhs[0] = mxCreateNumericArray (2, dims2, mxDOUBLE_CLASS, mxREAL);
   double *noisevalue=mxGetPr(plhs[0]);
   if (nrhs==3) {
	   for(int i=0; i<L; i++){
		   for(int j=0; j<M; j++){
				for(int k=0; k<N; k++){
					noisevalue[(i*M*N)+(j*N)+k]=noise3(x[i], y[j], z[k]);
				}
		   }
	   }
   } else if (nrhs==2) {
	   for(int i=0; i<L; i++){
		   for(int j=0; j<M; j++){
				noisevalue[(i*M)+j]=noise2(x[i], y[j]);
		   }
	   }	
   }
}

