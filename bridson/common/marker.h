#ifndef MARKER_H
#define MARKER_H

#include <algorithm>
#include <vector>

//===================================== Marker ===============================
// Essentially a bit vector with an efficient means for clearing all bits

struct Marker
{
   explicit Marker(unsigned int n=0)
      : flag(n,0), counter(1)
   {}

   void clear(void)
   {
      flag.clear();
      counter=1;
   }

   void resize(unsigned int n_)
   {
      flag.resize(n_, 0);
   }

   void reset_all(void)
   {
      ++counter;
      if(counter==0){ // if we hit wrap-around, reset everything
         for(unsigned int i=0; i<flag.size(); ++i)
            flag[i]=0;
         counter=1;
      }
   }

   bool operator[](unsigned int i) const
   { return flag[i]==counter; }

   void set(unsigned int i)
   { flag[i]=counter; }

   void swap(Marker &othermarker)
   {
      std::swap(flag, othermarker.flag);
      std::swap(counter, othermarker.counter);
   }

   protected:
   std::vector<unsigned int> flag;
   unsigned int counter;
};

// 2 dimensional array version (cf. Array2)
// Note: Array2<bool> doesn't work properly
struct Marker2
{
   int nx, ny;
   Marker underlying_marker;

   Marker2(void) : nx(0), ny(0) {}

   Marker2(int nx_, int ny_)
      : nx(nx_), ny(ny_), underlying_marker((unsigned int)(nx_*ny_))
   {}

   void clear(void)
   {
      nx=0; ny=0;
      underlying_marker.clear();
   }

   void resize(int nx_, int ny_)
   {
      nx=nx_;
      ny=ny_;
      underlying_marker.resize((unsigned int)(nx*ny));
   }

   void reset_all(void)
   { underlying_marker.reset_all(); }

   bool operator()(int i, int j) const
   {
      assert(i>=0 && i<nx && j>=0 && j<ny);
      return underlying_marker[i+j*nx];
   }

   void set(int i, int j)
   {
      assert(i>=0 && i<nx && j>=0 && j<ny);
      underlying_marker.set(i+j*nx);
   }

   void swap(Marker2 &othermarker)
   {
      std::swap(nx, othermarker.nx);
      std::swap(ny, othermarker.ny);
      std::swap(underlying_marker, othermarker.underlying_marker);
   }
};

// 3 dimensional array version (cf. Array3)
struct Marker3
{
   int nx, ny, nz;
   Marker underlying_marker;

   Marker3(void) : nx(0), ny(0), nz(0) {}

   Marker3(int nx_, int ny_, int nz_)
      : nx(nx_), ny(ny_), nz(nz_), underlying_marker((unsigned int)(nx_*ny_*nz_))
   {}

   void clear(void)
   {
      nx=0; ny=0; nz=0;
      underlying_marker.clear();
   }

   void resize(int nx_, int ny_, int nz_)
   {
      nx=nx_;
      ny=ny_;
      nz=nz_;
      underlying_marker.resize((unsigned int)(nx*ny*nz));
   }

   void reset_all(void)
   { underlying_marker.reset_all(); }

   bool operator()(int i, int j, int k) const
   {
      assert(i>=0 && i<nx && j>=0 && j<ny && k>=0 && k<nz);
      return underlying_marker[i+nx*(j+ny*k)];
   }

   void set(int i, int j, int k)
   {
      assert(i>=0 && i<nx && j>=0 && j<ny && k>=0 && k<nz);
      underlying_marker.set(i+nx*(j+ny*k));
   }

   void swap(Marker3 &othermarker)
   {
      std::swap(nx, othermarker.nx);
      std::swap(ny, othermarker.ny);
      std::swap(nz, othermarker.nz);
      std::swap(underlying_marker, othermarker.underlying_marker);
   }
};

#endif

