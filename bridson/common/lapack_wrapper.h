#ifndef LAPACK_WRAPPER_H
#define LAPACK_WRAPPER_H

// Simplified LAPACK wrapper (overloaded readable names, standard across platforms)
// as well as versions with stride==1 assumed.
// For the moment, no complex number support, and most routines have been dropped.

namespace LAPACK{

#ifdef __APPLE__
#include "vecLib/clapack.h"

inline int solve_general_system(int n, int nrhs, float *a, int lda, int *ipiv, float *b, int ldb, int &info)
{ return sgesv_(&n, &nrhs, a, &lda, ipiv, b, &ldb, &info); }

inline int solve_general_system(int n, int nrhs, double *a, int lda, int *ipiv, double *b, int ldb, int &info)
{ return dgesv_(&n, &nrhs, a, &lda, ipiv, b, &ldb, &info); }

inline int factor_general_matrix(int m, int n, float *a, int lda, int *ipiv, int &info)
{ return sgetrf_(&m, &n, a, &lda, ipiv, &info); }

inline int factor_general_matrix(int m, int n, double *a, int lda, int *ipiv, int &info)
{ return dgetrf_(&m, &n, a, &lda, ipiv, &info); }


/*
int sgetrs_(char *trans, int *n, int *nrhs, float *a, int *lda, int *ipiv, float *b, int *ldb, int *info);
int dgetrs_(char *trans, int *n, int *nrhs, double *a, int *lda, int *ipiv, double *b, int *ldb, int *info);

*/ 

inline int solve_symmetric_indefinite_system(char uplo, int n, int nrhs, float *a, int lda, int *ipiv,
                                             float *b, int ldb, float *work, int *lwork, int &info)
{ return ssysv_(&uplo, &n, &nrhs, a, &lda, ipiv, b, &ldb, work, lwork, &info); }

inline int solve_symmetric_indefinite_system(char uplo, int n, int nrhs, double *a, int lda, int *ipiv,
                                             double *b, int ldb, double *work, int *lwork, int &info)
{ return dsysv_(&uplo, &n, &nrhs, a, &lda, ipiv, b, &ldb, work, lwork, &info); }

/*
int ssytrf_(char *uplo, int *n, float *a, int *lda, int *ipiv, float *work, int *lwork, int *info);
int dsytrf_(char *uplo, int *n, double *a, int *lda, int *ipiv, double *work, int *lwork, int *info);
 
int ssytrs_(char *uplo, int *n, int *nrhs, float *a, int *lda, int *ipiv, float *b, int *ldb, int *info);
int dsytrs_(char *uplo, int *n, int *nrhs, double *a, int *lda, int *ipiv, double *b, int *ldb, int *info);
 
int ssytri_(char *uplo, int *n, float *a, int *lda, int *ipiv, float *work, int *info);
int dsytri_(char *uplo, int *n, double *a, int *lda, int *ipiv, double *work, int *info);
*/ 

inline int solve_symmetric_positive_system(char uplo, int n, int nrhs, float *a, int lda, float *b, int ldb, int &info)
{ return sposv_(&uplo, &n, &nrhs, a, &lda, b, &ldb, &info); }

inline int solve_symmetric_positive_system(char uplo, int n, int nrhs, double *a, int lda, double *b, int ldb, int &info)
{ return dposv_(&uplo, &n, &nrhs, a, &lda, b, &ldb, &info); }

/*
int spotrf_(char *uplo, int *n, float *a, int *lda, int *info);
int dpotrf_(char *uplo, int *n, double *a, int *lda, int *info);
 
int spotrs_(char *uplo, int *n, int *nrhs, float *a, int *lda, float *b, int *ldb, int *info);
int dpotrs_(char *uplo, int *n, int *nrhs, double *a, int *lda, double *b, int *ldb, int *info);
 
int spotri_(char *uplo, int *n, float *a, int *lda, int *info);
int dpotri_(char *uplo, int *n, double *a, int *lda, int *info);
*/ 

#endif // end of __APPLE__ version of this file

//#ifdef WIN32
#ifndef __APPLE__ // Linux and Windows

#include "acml.h"

inline void solve_general_system(int n, int nrhs, float *a, int lda, int *ipiv, float *b, int ldb, int &info)
{ sgesv_(&n, &nrhs, a, &lda, ipiv, b, &ldb, &info); }

inline void solve_general_system(int n, int nrhs, double *a, int lda, int *ipiv, double *b, int ldb, int &info)
{ dgesv_(&n, &nrhs, a, &lda, ipiv, b, &ldb, &info); }

inline void factor_general_matrix(int m, int n, float *a, int lda, int *ipiv, int &info)
{ sgetrf(m, n, a, lda, ipiv, &info); }

inline void factor_general_matrix(int m, int n, double *a, int lda, int *ipiv, int &info)
{ dgetrf(m, n, a, lda, ipiv, &info); }

//I had to use sgetri instead of sgetri_, because the AMD ACML LAPACK was having problems linking to
//sgetri_ (and other functions ending with underscore). My initial suspicion is they forgot to make it accessible, 
//or something along those lines. I left the signature of this function alone anyhow so it matches the Apple version.
inline void invert_general_matrix(int n, float *a, int lda, int *ipiv, int &info)
{ 
   factor_general_matrix(n, n, a, lda, ipiv, info);
   sgetri(n, a, lda, ipiv, &info); 
}

inline void invert_general_matrix(int n, double *a, int lda, int *ipiv, int &info)
{
   factor_general_matrix(n, n, a, lda, ipiv, info);
   dgetri(n, a, lda, ipiv, &info);  
}

/*
int sgetrs_(char *trans, int *n, int *nrhs, float *a, int *lda, int *ipiv, float *b, int *ldb, int *info);
int dgetrs_(char *trans, int *n, int *nrhs, double *a, int *lda, int *ipiv, double *b, int *ldb, int *info);
 
*/ 

inline void solve_symmetric_indefinite_system(char uplo, int n, int nrhs, float *a, int lda, int *ipiv,
                                             float *b, int ldb, float *work, int *lwork, int &info)
{ ssysv_(&uplo, &n, &nrhs, a, &lda, ipiv, b, &ldb, work, lwork, &info, 1); }

inline void solve_symmetric_indefinite_system(char uplo, int n, int nrhs, double *a, int lda, int *ipiv,
                                             double *b, int ldb, double *work, int *lwork, int &info)
{ dsysv_(&uplo, &n, &nrhs, a, &lda, ipiv, b, &ldb, work, lwork, &info, 1); }

/*
int ssytrf_(char *uplo, int *n, float *a, int *lda, int *ipiv, float *work, int *lwork, int *info);
int dsytrf_(char *uplo, int *n, double *a, int *lda, int *ipiv, double *work, int *lwork, int *info);
 
int ssytrs_(char *uplo, int *n, int *nrhs, float *a, int *lda, int *ipiv, float *b, int *ldb, int *info);
int dsytrs_(char *uplo, int *n, int *nrhs, double *a, int *lda, int *ipiv, double *b, int *ldb, int *info);
 
int ssytri_(char *uplo, int *n, float *a, int *lda, int *ipiv, float *work, int *info);
int dsytri_(char *uplo, int *n, double *a, int *lda, int *ipiv, double *work, int *info);
*/ 

inline void solve_symmetric_positive_system(char uplo, int n, int nrhs, float *a, int lda, float *b, int ldb, int &info)
{ sposv_(&uplo, &n, &nrhs, a, &lda, b, &ldb, &info, 1); }

inline void solve_symmetric_positive_system(char uplo, int n, int nrhs, double *a, int lda, double *b, int ldb, int &info)
{ dposv_(&uplo, &n, &nrhs, a, &lda, b, &ldb, &info, 1); }

/*
int spotrf_(char *uplo, int *n, float *a, int *lda, int *info);
int dpotrf_(char *uplo, int *n, double *a, int *lda, int *info);
 
int spotrs_(char *uplo, int *n, int *nrhs, float *a, int *lda, float *b, int *ldb, int *info);
int dpotrs_(char *uplo, int *n, int *nrhs, double *a, int *lda, double *b, int *ldb, int *info);
 
int spotri_(char *uplo, int *n, float *a, int *lda, int *info);
int dpotri_(char *uplo, int *n, double *a, int *lda, int *info);
*/ 


#endif //End of Windows and Linux version of this file


}; // end of namespace LAPACK

#endif
