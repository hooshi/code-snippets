#include "bfstream.h"
#include "image_io.h"

// SGI file IO ===============================================================

template<class fileT>
static bool read_sgi_uncompressed_scanline(bifstream &input, Array3f &img, int j, int channel, char *buf, int pixmin, float pixrange)
{
   fileT *buffer=(fileT*)buf;
   input.read(buffer, img.ni);
   if(input.fail()) return false;
   for(int i=0; i<img.ni; ++i) img(i,j,channel)=(buffer[i]-pixmin)/pixrange;
   return true;
}

template<class fileT>
static bool read_sgi_rle_scanline(bifstream &input, Array3f &img, int j, int channel, int start, int length, char *buf, int pixmin, float pixrange)
{
   fileT *buffer=(fileT*)buf;
   input.seek(start);
   input.read(buffer, length); 
   if(input.fail()) return false;

   int i=0, p=0, count, end;
   fileT pixel;
   while(p<length){
      pixel=buffer[p];
      ++p;
      count=pixel & 0x7f;
      if(count==0){
         return p==length;
      }
      if(pixel & 0x80){ // if we just should copy count pixels
         end=p+count;
         if(end>length || i+count>img.ni) return false;
         for(; p<end; ++i, ++p)
            img(i,j,channel)=(buffer[p]-pixmin)/pixrange;
      }else{ // get the next pixel and repeat count times
         if(p>=length) return false;
         pixel=buffer[p];
         ++p;
         float v=(pixel-pixmin)/pixrange;
         end=i+count;
         if(end>img.ni) return false;
         for(; i<end; ++i)
            img(i,j,channel)=v;
      }
   }
   return true;
}

bool read_sgi(Array3f &img, const char *filename_format, ...)
{
   va_list ap;
   va_start(ap, filename_format);
   bifstream input;
   input.vopen(filename_format, ap);
   va_end(ap);
   if(!input.good()) return false;

   assert(sizeof(short)==2 && sizeof(int)==4);
   input.set_big_endian();

   short magic;
   input>>magic;
   if(magic!=474) return false; // not an SGI image

   char st, b;
   input>>st>>b;
   int storage=st, bpc=b;
   if(storage<0 || storage>1 || bpc<1 || bpc>2)
      return false; // can't handle other filename_formats yet (or header is corrupted)

   unsigned short dim, xsize, ysize, zsize;
   input>>dim>>xsize>>ysize>>zsize;

   int pixmin, pixmax;
   input>>pixmin>>pixmax;

   input.skip(84);
   int colormap;
   input>>colormap;
   if(colormap!=0) return false; // can't handle colormaps

   input.skip(404);
   if(!input.good()) return false;

   img.resize(xsize, ysize, zsize);

   Array1c buffer(bpc*(2*xsize+2));
   float pixrange=(float)(pixmax-pixmin);

   if(storage==0){ // uncompressed
      if(bpc==1){
         for(int c=0; c<zsize; ++c) for(int j=0; j<ysize; ++j)
            if(!read_sgi_uncompressed_scanline<unsigned char>(input, img, j, c, buffer.data, pixmin, pixrange))
               return false;
      }else{ assert(bpc==2);
         for(int c=0; c<zsize; ++c) for(int j=0; j<ysize; ++j)
            if(!read_sgi_uncompressed_scanline<unsigned short>(input, img, j, c, buffer.data, pixmin, pixrange))
               return false;
      }
   }else{ // RLE
      Array1i rle_start(ysize*zsize), rle_length(ysize*zsize);
      input.read(&rle_start[0], ysize*zsize);
      input.read(&rle_length[0], ysize*zsize);
      if(!input.good()) return false;
      if(bpc==1){
         for(int c=0; c<zsize; ++c) for(int j=0; j<ysize; ++j)
            if(!read_sgi_rle_scanline<unsigned char>(input, img, j, c, rle_start[j+ysize*c], rle_length[j+ysize*c], buffer.data, pixmin, pixrange))
               return false;
      }else{ assert(bpc==2);
         for(int c=0; c<zsize; ++c) for(int j=0; j<ysize; ++j)
            if(!read_sgi_rle_scanline<unsigned short>(input, img, j, c, rle_start[j+ysize*c], rle_length[j+ysize*c], buffer.data, pixmin, pixrange))
               return false;
      }
   }

   return true;
}

bool write_sgi(const Array3f &img, bool high_precision, const char *filename_format, ...)
{
   va_list ap;
   va_start(ap, filename_format);
   bofstream output;
   output.vopen(filename_format, ap);
   va_end(ap);
   if(!output.good()) return false;

   assert(sizeof(short)==2 && sizeof(int)==4);
   output.set_big_endian();
   output<<(short)474<<(char)0<<(char)(high_precision?2:1);
   output<<(unsigned short)3<<(unsigned short)img.ni<<(unsigned short)img.nj<<(unsigned short)img.nk;
   output<<0<<(high_precision?65535:255);
   output.write_zero(492);

   // now write the actual data
   for(int channel=0; channel<img.nk; ++channel){
      for(int j=0; j<img.nj; ++j){
         for(int i=0; i<img.ni; ++i){
            float scaled_value=img(i,j,channel)*(high_precision?65536:256);
            if(scaled_value<0) scaled_value=0;
            else if(!high_precision && scaled_value>255) scaled_value=255;
            else if(high_precision && scaled_value>65535) scaled_value=65535;
	    if(!output.good()) return false;
            if(high_precision) output<<(unsigned short)scaled_value;
            else output<<(unsigned char)scaled_value;
         }
      }
   }
   return !output.fail();
}

static int quantize(float s, int pixmin, int pixrange)
{
   if(s>1) return pixmin+pixrange;
   else if(s<0) return pixmin;
   else return pixmin+s*pixrange;
}

static int find_run_length(const Array3f& img, int& i, int j, int k, int pixmin, int pixrange)
{
   // is this run worth compressing?
   int pixel=quantize(img(i,j,k), pixmin, pixrange);
   if(i+2<img.ni && pixel==quantize(img(i+1,j,k), pixmin, pixrange) && pixel==quantize(img(i+2,j,k), pixmin, pixrange)){
      // find the length of this run (up to appropriate max)
      int r=3;
      while(i+r<img.ni && r<127 && pixel==quantize(img(i+r,j,k), pixmin, pixrange))
         ++r;
      i+=r;
      return 2;
   }else{ // we should just copy pixels until a suitable run presents itself (greedy strategy - not optimal compression)
      int r=1;
      while(i+r<img.ni && r<127){
         pixel=quantize(img(i+r,j,k), pixmin, pixrange);
         if(i+r+2<img.ni && pixel==quantize(img(i+r+1,j,k), pixmin, pixrange) && pixel==quantize(img(i+r+2,j,k), pixmin, pixrange))
            break;
         ++r;
      }
      i+=r;
      return 1+r;
   }
}

static void compute_rle_runs(const Array3f& img, int* rle_start, int* rle_length, int bpc, int pixmin, int pixrange)
{
   int s=0;
   // first get lengths
   for(int k=0; k<img.nk; ++k) for(int j=0; j<img.nj; ++j){
      rle_length[s]=0;
      if(img.ni>0){
         int i=0;
         while(i<img.ni) rle_length[s]+=bpc*find_run_length(img, i, j, k, pixmin, pixrange);
      }
      ++s;
   }
   // then do starts via cumulative sum
   rle_start[0]=512+8*img.nj*img.nk; // after header
   for(s=1; s<img.nj*img.nk; ++s) rle_start[s]=rle_start[s-1]+rle_length[s-1];
}

template<class fileT>
static void make_run(const Array3f& img, int& i, int j, int k, int pixmin, int pixrange, char* raw_buf, int& loc)
{
   fileT* buf=(fileT*)raw_buf;
   // is this run worth compressing?
   int pixel=quantize(img(i,j,k), pixmin, pixrange);
   if(i+2<img.ni && pixel==quantize(img(i+1,j,k), pixmin, pixrange) && pixel==quantize(img(i+2,j,k), pixmin, pixrange)){
      // find the length of this run (up to appropriate max)
      int r=3;
      while(i+r<img.ni && r<127 && pixel==quantize(img(i+r,j,k), pixmin, pixrange))
         ++r;
      i+=r;
      buf[loc]=r;
      buf[loc+1]=pixel;
      loc+=2;
   }else{ // we should just copy pixels until a suitable run presents itself (greedy strategy - not optimal compression)
      buf[loc+1]=pixel;
      int r=1;
      while(i+r<img.ni && r<127){
         pixel=quantize(img(i+r,j,k), pixmin, pixrange);
         if(i+r+2<img.ni && pixel==quantize(img(i+r+1,j,k), pixmin, pixrange) && pixel==quantize(img(i+r+2,j,k), pixmin, pixrange))
            break;
         ++r;
         buf[loc+r]=pixel;
      }
      buf[loc]=0x80 | r;
      i+=r;
      loc+=1+r;
   }
}

template<class fileT>
static bool write_sgi_rle_scanline(bofstream &output, const Array3f &img, int j, int k, char *buf, int pixmin, int pixrange)
{
   if(img.ni>0){
      int i=0, loc=0;
      while(i<img.ni) make_run<fileT>(img, i, j, k, pixmin, pixrange, buf, loc);
      output.write(buf, sizeof(fileT)*loc);
   }
   return !output.fail();
}

bool write_sgi_compressed(const Array3f &img, bool high_precision, const char *filename_format, ...)
{
   va_list ap;
   va_start(ap, filename_format);
   bofstream output;
   output.vopen(filename_format, ap);
   va_end(ap);
   if(!output.good()) return false;

   // 512 byte header
   assert(sizeof(short)==2 && sizeof(int)==4);
   output.set_big_endian();
   output<<(short)474<<(char)1<<(char)(high_precision?2:1);
   output<<(unsigned short)3<<(unsigned short)img.ni<<(unsigned short)img.nj<<(unsigned short)img.nk;
   output<<0<<(high_precision?65535:255);
   output.write_zero(492);

   // figure out rle table
   Array1i rle_start(img.nj*img.nk+1), rle_length(img.nj*img.nk+1);
   compute_rle_runs(img, rle_start.data, rle_length.data, high_precision ? 2 : 1, 0, high_precision ? 65535 : 255);
   
   // and write the table
   output.write(rle_start.data, img.nj*img.nk);
   output.write(rle_length.data, img.nj*img.nk);
   if(!output.good()) return false;

   // now write the actual rle data
   Array1c buffer((high_precision ? 2 : 1)*(2*img.ni+2));
   if(!high_precision){
      for(int k=0; k<img.nk; ++k) for(int j=0; j<img.nj; ++j)
         if(!write_sgi_rle_scanline<unsigned char>(output, img, j, k, buffer.data, 0, 255))
            return false;
   }else{
      for(int k=0; k<img.nk; ++k) for(int j=0; j<img.nj; ++j)
         if(!write_sgi_rle_scanline<unsigned short>(output, img, j, k, buffer.data, 0, 65535))
            return false;
   }
   return !output.fail();
}

