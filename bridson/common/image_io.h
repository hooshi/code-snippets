#ifndef IMAGE_IO_H
#define IMAGE_IO_H

#include "array3.h"

// Images are represented here as 3d float arrays: the first two dimensions
// are x and y, and the third is for color channels.

// Returns false if read fails. Third dimension is number of channels in image.
bool read_sgi(Array3f &img, const char *filename_format, ...);

// Returns false if write fails.
bool write_sgi(const Array3f &img, bool high_precision, const char *filename_format, ...);

// Returns false if write fails.
bool write_sgi_compressed(const Array3f &img, bool high_precision, const char *filename_format, ...);

#endif
