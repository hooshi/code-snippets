#include "marchingsquares.h"
#include "array2.h"
#include "interp.h"

float interpval(float p1, float p2) {
   return p1 / (p1 - p2);
}

const int TOP = 0;
const int BOTTOM = 1;
const int LEFT = 2;
const int RIGHT = 3;

MarchingSquares::
MarchingSquares(Vec2f min_point_, float dx_, int nx_, int ny_, implicit_surface surface_): 
surface(surface_), min_point(min_point_), dx(dx_), nx(nx_), ny(ny_), function_call(true)
{
}

MarchingSquares::
MarchingSquares(Vec2f min_point_, float dx_, int nx_, int ny_, Array2f& grid_data_): 
grid_data(grid_data_), min_point(min_point_), dx(dx_), nx(nx_), ny(ny_), function_call(false)
{
}

void solve_square(float bl, float br, float tl, float tr, Vec2ui& edge1, Vec2ui& edge2) {
   //Determines the marching squares case based on
   //the cell's corner values

   //Returns edges indicating where, if any, the vertices go
   //0 = top, 1 = bottom, 2 = left, 3 = right
   edge1[0] = -1;
   edge1[1] = -1;
   edge2[0] = -1;
   edge2[1] = -1;
   unsigned int code = 0;
   if(bl <= 0)
      code |= 1;
   if(br <= 0)
      code |= 2;
   if(tl <= 0)
      code |= 8;
   if(tr <= 0)
      code |= 4; 

   switch(code){
      case 0:
         return;
      case 1: 
      case 14:
         edge1[0] = LEFT;
         edge1[1] = BOTTOM;
         return;
      case 2:
      case 13:
         edge1[0] = BOTTOM;
         edge1[1] = RIGHT;
         return;
      case 3:
      case 12:
         edge1[0] = LEFT;
         edge1[1] = RIGHT;
         return;
      case 4:
      case 11:
         edge1[0] = RIGHT;
         edge1[1] = TOP;
         return;
      case 5:
      case 10:
         edge1[0] = TOP;
         edge1[1] = LEFT;
         edge2[0] = BOTTOM;
         edge2[1] = RIGHT;
         return;
      case 6:
      case 9:
         edge1[0] = TOP;
         edge1[1] = BOTTOM;
         return;
      case 7:
      case 8:
         edge1[0] = TOP;
         edge1[1] = LEFT;
      case 15:
         return;
      default:
         return;
   }
}

unsigned int pick_edge(int position, int left, int right, int above, int below) {
   //pick the correct vertex index, based on the input cell face ID
   switch(position){
      case TOP: return above;
      case BOTTOM: return below;
      case LEFT: return left;
      case RIGHT: return right;
      default:
         printf("ERROR!");
         return 0;
   }
}

void MarchingSquares::
march_lines() {
    
   //Generate a data grid from the supplied function
   //so we don't have to query repeatedly
   Array2f grid(nx+1,ny+1);
   if(function_call) {
      for(int row = ny; row >= 0; --row) {
         for(int col = 0; col <= nx; ++col) {
            Vec2f pos(col*dx,row*dx);
            grid(col,row) = (*surface)(pos);
         }
      }
   }
   else {
      grid = grid_data;
      assert(nx < grid.nx);
      assert(ny < grid.ny);
   }
   
   //Grid offsets for convenience.
   Vec2f off_x(dx, 0);
   Vec2f off_y(0, dx);
   
   //Initialize the bottom row, and create any vertices 
   //lying on the bottom-most grid edge
   std::vector<int> below_indices(nx,-1);
   for(int col = 0; col < nx; ++col) {
      float bl = grid(col,0);
      float br = grid(col+1,0);
      if((bl <= 0 && br > 0) || (bl > 0 && br <= 0)) {
         Vec2f pos(col*dx,0);
         pos += min_point;
         below_indices[col] = (int)verts.size();
         float alpha = interpval(bl, br);
         verts.push_back( (1-alpha) * (pos) + alpha * (pos+off_x) );
      }
   }

   //We'll sweep through the grid, accumulating 
   //vertices and connectivity info as we go
   for(int row = 0; row < ny; ++row) {
      
      //initialize prevIndex
      int prevIndex = -1;
      
      //Check for a vertex at the leftmost edge
      float bl = grid(0,row);
      float tl = grid(0,row+1);
      if((bl <= 0 && tl > 0) || (bl > 0 && tl <= 0)) {
         Vec2f pos(0,row*dx);
         pos += min_point;
         prevIndex = (int)verts.size();
         float alpha = interpval(bl, tl);
         verts.push_back( (1-alpha) * (pos) + alpha * (pos+off_y) );
      }

      for(int col = 0; col < nx; ++col) {
         Vec2f pos(col*dx,row*dx);
         pos += min_point;

         //prevIndex is already set from previous cell
         int belowIndex = below_indices[col];
        
         //these are computed in the process,may generate new vertices
         int nextIndex = 0;
         int aboveIndex = 0;

         //get corner data
         float bl = grid(col,row);
         float br = grid(col+1,row);
         float tl = grid(col,row+1);
         float tr = grid(col+1,row+1);
         
         //Find the correct case 
         Vec2ui edge0,edge1;
         solve_square(bl,br,tl,tr,edge0,edge1);
         
         //determine if we need to create any verts/edges
         //all vertices are created on the top/right cell
         //edges, so we never duplicate any vertices
         //as we sweep through the grid.
         if(edge0[0]!=-1) {
            if(edge0[0] == RIGHT || edge0[1] == RIGHT) {
               float alpha = interpval(br, tr);
               nextIndex = (int)verts.size();
               verts.push_back( (1-alpha) * (pos + off_x) + alpha * (pos+off_x+off_y) );
            }
            if(edge0[0] == TOP || edge0[1] == TOP) {
               float alpha = interpval(tl, tr);
               aboveIndex = (int)verts.size();
               verts.push_back( (1-alpha) * (pos + off_y) + alpha * (pos+off_x+off_y) );
            }
            Vec2ui edge(
               pick_edge(edge0[0],prevIndex,nextIndex,aboveIndex,belowIndex),
               pick_edge(edge0[1],prevIndex,nextIndex,aboveIndex,belowIndex));
            edges.push_back(edge);
         }
         if(edge1[0]!=-1) {
            if(edge1[0] == RIGHT || edge1[1] == RIGHT) {
               float alpha = interpval(br, tr);
               nextIndex = (int)verts.size();
               verts.push_back( (1-alpha) * (pos + off_x) + alpha * (pos+off_x+off_y) );
            }
            if(edge1[0] == TOP || edge1[1] == TOP) {
               float alpha = interpval(tl, tr);
               aboveIndex = (int)verts.size();
               verts.push_back( (1-alpha) * (pos + off_y) + alpha * (pos+off_x+off_y) );
            }
            Vec2ui edge(
               pick_edge(edge1[0],prevIndex,nextIndex,aboveIndex,belowIndex),
               pick_edge(edge1[1],prevIndex,nextIndex,aboveIndex,belowIndex));
            edges.push_back(edge);
         }
        
         //shift over, and adjust adjacent vert index data
         prevIndex = nextIndex;
         below_indices[col] = aboveIndex;
      }
   }
}

void MarchingSquares::
march_polys() {
   static Vec2f squareCorners[4];
   static float squarePhi[4];
   polys.clear();
   static std::vector<Vec2f> points;
   for(int j = 0; j < ny; ++j) {
      for(int i = 0; i < nx; ++i) {
         
         //determine 4 surrounding points
         float x = min_point[0] + i*dx;
         float y = min_point[1] + j*dx;

         squareCorners[0] = Vec2f(x,y);
         squareCorners[1] = Vec2f(x+dx,y);
         squareCorners[2] = Vec2f(x+dx,y+dx);
         squareCorners[3] = Vec2f(x,y+dx);

         squarePhi[0] = (*surface)(squareCorners[0]);
         squarePhi[1] = (*surface)(squareCorners[1]);
         squarePhi[2] = (*surface)(squareCorners[2]);
         squarePhi[3] = (*surface)(squareCorners[3]);
         
         points.clear();
         int startVert = 0;
         while(squarePhi[startVert] > 0 && startVert < 4) {
            startVert++;
         }
         if(startVert < 4) { //Indicates there is some surface in the cell
            //Loop invariant - current vertex is already added, if necessary
            int count = 1;
            points.push_back(squareCorners[startVert]);
            int curVert = startVert;
            int nextVert = startVert+1;
            while(nextVert != startVert) {
               nextVert = (curVert + 1)%4;
               float phiCur = squarePhi[curVert];
               float phiNext = squarePhi[nextVert];
               if(phiCur <= 0 && phiNext <= 0) {
                  points.push_back(squareCorners[nextVert]);
                  count++;
               }
               else if(phiCur <= 0 && phiNext > 0) {
                  float alpha = interpval(phiCur, phiNext);
                  points.push_back((1-alpha) * squareCorners[curVert] + alpha * squareCorners[nextVert]);
                  count++;
               }
               else if(phiCur > 0 && phiNext <= 0) {
                  float alpha = interpval(phiCur, phiNext);
                  points.push_back((1-alpha) * squareCorners[curVert] + alpha * squareCorners[nextVert]);
                  points.push_back(squareCorners[nextVert]);
                  count+=2;
               }
               curVert = nextVert;
            }
         }
         if(points.size() > 0)
            polys.push_back(points);
      }
   }
}
