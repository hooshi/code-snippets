#include "collisionqueries.h"

double signed_volume(const Vec3d &x0, const Vec3d &x1, const Vec3d &x2, const Vec3d &x3)
{
   // Equivalent to triple(x1-x0, x2-x0, x3-x0), six times the signed volume of the tetrahedron.
   // But, for robustness, we want the result (up to sign) to be independent of the ordering.
   // And want it as accurate as possible...
   // But all that stuff is hard, so let's just use the common assumption that all coordinates are >0,
   // and do something reasonably accurate in fp.

   // This formula does almost four times too much multiplication, but if the coordinates are non-negative
   // it suffers in a minimal way from cancellation error.
   return ( x0[0]*(x1[1]*x3[2]+x3[1]*x2[2]+x2[1]*x1[2])
           +x1[0]*(x2[1]*x3[2]+x3[1]*x0[2]+x0[1]*x2[2])
           +x2[0]*(x3[1]*x1[2]+x1[1]*x0[2]+x0[1]*x3[2])
           +x3[0]*(x1[1]*x2[2]+x2[1]*x0[2]+x0[1]*x1[2]) )

        - ( x0[0]*(x2[1]*x3[2]+x3[1]*x1[2]+x1[1]*x2[2])
           +x1[0]*(x3[1]*x2[2]+x2[1]*x0[2]+x0[1]*x3[2])
           +x2[0]*(x1[1]*x3[2]+x3[1]*x0[2]+x0[1]*x1[2])
           +x3[0]*(x2[1]*x1[2]+x1[1]*x0[2]+x0[1]*x2[2]) );
}

bool check_edge_triangle_intersection(const Vec3d &xedge0, const Vec3d &xedge1,
                                      const Vec3d &xtri0, const Vec3d &xtri1, const Vec3d &xtri2)
{
   // check segment against plane of triangle
   double s0_012=signed_volume(xedge0, xtri0, xtri1, xtri2);
   double s1_012=signed_volume(xedge1, xtri0, xtri1, xtri2);
   if((s0_012<0 && s1_012<0) || (s0_012>0 && s1_012>0))
      return false;
   // check line of segment against edges of triangle
   double s01_01=signed_volume(xedge0, xedge1, xtri0, xtri1);
   double s01_12=signed_volume(xedge0, xedge1, xtri1, xtri2);
   double s01_20=signed_volume(xedge0, xedge1, xtri2, xtri0);
   if(s01_01<0){
      return s01_12<=0 && s01_20<=0;
   }else if(s01_01>0){
      return s01_12>=0 && s01_20>=0;
   }else if(s01_12<0){
      return s01_20<=0;
   }else if(s01_12>0){
      return s01_20>=0;
   }else{
      return true;
   }
}

void check_point_edge_proximity(bool update, const Vec3d &x0, const Vec3d &x1, const Vec3d &x2,
                                double &distance)
{
   Vec3d dx(x2-x1);
   double m2=mag2(dx);
   // find parameter value of closest point on segment
   double s=clamp(dot(x2-x0, dx)/m2, 0., 1.);
   // and find the distance
   if(update){
      distance=min(distance, dist(x0,s*x1+(1-s)*x2));
   }else{
      distance=dist(x0,s*x1+(1-s)*x2);
   }
}

// normal is from 1-2 towards 0, unless normal_multiplier<0
void check_point_edge_proximity(bool update, const Vec3d &x0, const Vec3d &x1, const Vec3d &x2,
                                double &distance, double &s, Vec3d &normal, double normal_multiplier)
{
   Vec3d dx(x2-x1);
   double m2=mag2(dx);
   if(update){
      // find parameter value of closest point on segment
      double this_s=clamp(dot(x2-x0, dx)/m2, 0., 1.);
      // and find the distance
      Vec3d this_normal=x0-(this_s*x1+(1-this_s)*x2);
      double this_distance=mag(this_normal);
      if(this_distance<distance){
         s=this_s;
         distance=this_distance;
         normal=(normal_multiplier/(this_distance+1e-30))*this_normal;
      }
   }else{
      // find parameter value of closest point on segment
      s=clamp(dot(x2-x0, dx)/m2, 0., 1.);
      // and find the distance
      normal=x0-(s*x1+(1-s)*x2);
      distance=mag(normal);
      normal*=normal_multiplier/(distance+1e-30);
   }
}

void check_point_edge_proximity(bool update, const Vec2d &x0, const Vec2d &x1, const Vec2d &x2,
                                double &distance)
{
   Vec2d dx(x2-x1);
   double m2=mag2(dx);
   // find parameter value of closest point on segment
   double s=clamp(dot(x2-x0, dx)/m2, 0., 1.);
   // and find the distance
   if(update){
      distance=min(distance, dist(x0,s*x1+(1-s)*x2));
   }else{
      distance=dist(x0,s*x1+(1-s)*x2);
   }
}

// normal is from 1-2 towards 0, unless normal_multiplier<0
void check_point_edge_proximity(bool update, const Vec2d &x0, const Vec2d &x1, const Vec2d &x2,
                                double &distance, double &s, Vec2d &normal, double normal_multiplier)
{
   Vec2d dx(x2-x1);
   double m2=mag2(dx);
   if(update){
      // find parameter value of closest point on segment
      double this_s=clamp(dot(x2-x0, dx)/m2, 0., 1.);
      // and find the distance
      Vec2d this_normal=x0-(this_s*x1+(1-this_s)*x2);
      double this_distance=mag(this_normal);
      if(this_distance<distance){
         s=this_s;
         distance=this_distance;
         normal=(normal_multiplier/(this_distance+1e-30))*this_normal;
      }
   }else{
      // find parameter value of closest point on segment
      s=clamp(dot(x2-x0, dx)/m2, 0., 1.);
      // and find the distance
      normal=x0-(s*x1+(1-s)*x2);
      distance=mag(normal);
      normal*=normal_multiplier/(distance+1e-30);
   }
}

void check_edge_edge_proximity(const Vec3d &x0, const Vec3d &x1, const Vec3d &x2, const Vec3d &x3, double &distance)
{
   // let's do it the QR way for added robustness
   Vec3d x01=x0-x1;
   double r00=mag(x01)+1e-30;
   x01/=r00;
   Vec3d x32=x3-x2;
   double r01=dot(x32,x01);
   x32-=r01*x01;
   double r11=mag(x32)+1e-30;
   x32/=r11;
   Vec3d x31=x3-x1;
   double s2=dot(x32,x31)/r11;
   double s0=(dot(x01,x31)-r01*s2)/r00;
   // check if we're in range
   if(s0<0){
      if(s2<0){
         // check both x1 against 2-3 and 3 against 0-1
         check_point_edge_proximity(false, x1, x2, x3, distance);
         check_point_edge_proximity(true, x3, x0, x1, distance);
      }else if(s2>1){
         // check both x1 against 2-3 and 2 against 0-1
         check_point_edge_proximity(false, x1, x2, x3, distance);
         check_point_edge_proximity(true, x2, x0, x1, distance);
      }else{
         s0=0;
         // check x1 against 2-3
         check_point_edge_proximity(false, x1, x2, x3, distance);
      }
   }else if(s0>1){
      if(s2<0){
         // check both x0 against 2-3 and 3 against 0-1
         check_point_edge_proximity(false, x0, x2, x3, distance);
         check_point_edge_proximity(true, x3, x0, x1, distance);
      }else if(s2>1){
         // check both x0 against 2-3 and 2 against 0-1
         check_point_edge_proximity(false, x0, x2, x3, distance);
         check_point_edge_proximity(true, x2, x0, x1, distance);
      }else{
         s0=1;
         // check x0 against 2-3
         check_point_edge_proximity(false, x0, x2, x3, distance);
      }
   }else{
      if(s2<0){
         s2=0;
         // check x3 against 0-1
         check_point_edge_proximity(false, x3, x0, x1, distance);
      }else if(s2>1){
         s2=1;
         // check x2 against 0-1
         check_point_edge_proximity(false, x2, x0, x1, distance);
      }else{ // we already got the closest points!
         distance=dist(s2*x2+(1-s2)*x3, s0*x0+(1-s0)*x1);
      }
   }
}

// find distance between 0-1 and 2-3, with barycentric coordinates for closest points, and
// a normal that points from 0-1 towards 2-3 (unreliable if distance==0 or very small)
void check_edge_edge_proximity(const Vec3d &x0, const Vec3d &x1, const Vec3d &x2, const Vec3d &x3,
                               double &distance, double &s0, double &s2, Vec3d &normal)
{
   // let's do it the QR way for added robustness
   Vec3d x01=x0-x1;
   double r00=mag(x01)+1e-30;
   x01/=r00;
   Vec3d x32=x3-x2;
   double r01=dot(x32,x01);
   x32-=r01*x01;
   double r11=mag(x32)+1e-30;
   x32/=r11;
   Vec3d x31=x3-x1;
   s2=dot(x32,x31)/r11;
   s0=(dot(x01,x31)-r01*s2)/r00;
   // check if we're in range
   if(s0<0){
      if(s2<0){
         // check both x1 against 2-3 and 3 against 0-1
         check_point_edge_proximity(false, x1, x2, x3, distance, s2, normal, -1.);
         check_point_edge_proximity(true, x3, x0, x1, distance, s0, normal, 1.);
      }else if(s2>1){
         // check both x1 against 2-3 and 2 against 0-1
         check_point_edge_proximity(false, x1, x2, x3, distance, s2, normal, -1.);
         check_point_edge_proximity(true, x2, x0, x1, distance, s0, normal, 1.);
      }else{
         s0=0;
         // check x1 against 2-3
         check_point_edge_proximity(false, x1, x2, x3, distance, s2, normal, -1.);
      }
   }else if(s0>1){
      if(s2<0){
         // check both x0 against 2-3 and 3 against 0-1
         check_point_edge_proximity(false, x0, x2, x3, distance, s2, normal, -1.);
         check_point_edge_proximity(true, x3, x0, x1, distance, s0, normal, 1.);
      }else if(s2>1){
         // check both x0 against 2-3 and 2 against 0-1
         check_point_edge_proximity(false, x0, x2, x3, distance, s2, normal, -1.);
         check_point_edge_proximity(true, x2, x0, x1, distance, s0, normal, 1.);
      }else{
         s0=1;
         // check x0 against 2-3
         check_point_edge_proximity(false, x0, x2, x3, distance, s2, normal, -1.);
      }
   }else{
      if(s2<0){
         s2=0;
         // check x3 against 0-1
         check_point_edge_proximity(false, x3, x0, x1, distance, s0, normal, 1.);
      }else if(s2>1){
         s2=1;
         // check x2 against 0-1
         check_point_edge_proximity(false, x2, x0, x1, distance, s0, normal, 1.);
      }else{ // we already got the closest points!
         normal=(s2*x2+(1-s2)*x3)-(s0*x0+(1-s0)*x1);
         distance=mag(normal);
         if(distance>0) normal/=distance;
         else{
            normal=cross(x1-x0, x3-x2);
            normal/=mag(normal)+1e-300;
         }
      }
   }
}

void check_point_triangle_proximity(const Vec3d &x0, const Vec3d &x1, const Vec3d &x2, const Vec3d &x3,
                                    double &distance)
{
   // do it the QR way for added robustness
   Vec3d x13=x1-x3;
   double r00=mag(x13)+1e-30;
   x13/=r00;
   Vec3d x23=x2-x3;
   double r01=dot(x23,x13);
   x23-=r01*x13;
   double r11=mag(x23)+1e-30;
   x23/=r11;
   Vec3d x03=x0-x3;
   double s2=dot(x23,x03)/r11;
   double s1=(dot(x13,x03)-r01*s2)/r00;
   double s3=1-s1-s2;
   // check if we are in range
   if(s1>=0 && s2>=0 && s3>=0){
      distance=dist(x0, s1*x1+s2*x2+s3*x3);
   }else{
      if(s1>0){ // rules out edge 2-3
         check_point_edge_proximity(false, x0, x1, x2, distance);
         check_point_edge_proximity(true, x0, x1, x3, distance);
      }else if(s2>0){ // rules out edge 1-3
         check_point_edge_proximity(false, x0, x1, x2, distance);
         check_point_edge_proximity(true, x0, x2, x3, distance);
      }else{ // s3>0: rules out edge 1-2
         check_point_edge_proximity(false, x0, x2, x3, distance);
         check_point_edge_proximity(true, x0, x1, x3, distance);
      }
   }
}

// find distance between 0 and 1-2-3, with barycentric coordinates for closest point, and
// a normal that points from 1-2-3 towards 0 (unreliable if distance==0 or very small)
void check_point_triangle_proximity(const Vec3d &x0, const Vec3d &x1, const Vec3d &x2, const Vec3d &x3,
                                    double &distance, double &s1, double &s2, double &s3, Vec3d &normal)
{
   // do it the QR way for added robustness
   Vec3d x13=x1-x3;
   double r00=mag(x13)+1e-30;
   x13/=r00;
   Vec3d x23=x2-x3;
   double r01=dot(x23,x13);
   x23-=r01*x13;
   double r11=mag(x23)+1e-30;
   x23/=r11;
   Vec3d x03=x0-x3;
   s2=dot(x23,x03)/r11;
   s1=(dot(x13,x03)-r01*s2)/r00;
   s3=1-s1-s2;
   // check if we are in range
   if(s1>=0 && s2>=0 && s3>=0){
      normal=x0-(s1*x1+s2*x2+s3*x3);
      distance=mag(normal);
      if(distance>0) normal/=distance;
      else{
         normal=cross(x2-x1, x3-x1);
         normal/=mag(normal)+1e-300;
      }
   }else{
      double s, d;
      if(s1>0){ // rules out edge 2-3
         check_point_edge_proximity(false, x0, x1, x2, distance, s, normal, 1.);
         s1=s; s2=1-s; s3=0; d=distance;
         check_point_edge_proximity(true, x0, x1, x3, distance, s, normal, 1.);
         if(distance<d){
            s1=s; s2=0; s3=1-s;
         }
      }else if(s2>0){ // rules out edge 1-3
         check_point_edge_proximity(false, x0, x1, x2, distance, s, normal, 1.);
         s1=s; s2=1-s; s3=0; d=distance;
         check_point_edge_proximity(true, x0, x2, x3, distance, s, normal, 1.);
         if(distance<d){
            s1=0; s2=s; s3=1-s; d=distance;
         }
      }else{ // s3>0: rules out edge 1-2
         check_point_edge_proximity(false, x0, x2, x3, distance, s, normal, 1.);
         s1=0; s2=s; s3=1-s; d=distance;
         check_point_edge_proximity(true, x0, x1, x3, distance, s, normal, 1.);
         if(distance<d){
            s1=s; s2=0; s3=1-s;
         }
      }
   }
}

void find_coplanarity_times(const Vec3d &x0, const Vec3d &x1, const Vec3d &x2, const Vec3d &x3,
                            const Vec3d &xnew0, const Vec3d &xnew1, const Vec3d &xnew2, const Vec3d &xnew3,
                            std::vector<double> &possible_times)
{
   const double tol=1e-8;
   possible_times.clear();
   // cubic coefficients, A*t^3+B*t^2+C*t+D (for t in [0,1])
   Vec3d x03=x0-x3, x13=x1-x3, x23=x2-x3;
   Vec3d v03=(xnew0-xnew3)-x03, v13=(xnew1-xnew3)-x13, v23=(xnew2-xnew3)-x23;
   double A=triple(v03,v13,v23),
          B=triple(x03,v13,v23)+triple(v03,x13,v23)+triple(v03,v13,x23),
          C=triple(x03,x13,v23)+triple(x03,v13,x23)+triple(v03,x13,x23),
          D=triple(x03,x13,x23);
   const double convergence_tol=tol*(std::fabs(A)+std::fabs(B)+std::fabs(C)+std::fabs(D));

   // find intervals to check, or just solve it if it reduces to a quadratic =============================
   std::vector<double> interval_times;
   double discriminant=B*B-3*A*C; // of derivative of cubic, 3*A*t^2+2*B*t+C, divided by 4 for convenience
   if(discriminant<=0){ // monotone cubic: only one root in [0,1] possible
      // so we just 
      interval_times.push_back(0);
      interval_times.push_back(1);
   }else{ // positive discriminant, B!=0
      if(A==0){ // the cubic is just a quadratic, B*t^2+C*t+D ========================================
         discriminant=C*C-4*B*D; // of the quadratic
         if(discriminant<=0){
            double t=-C/(2*B);
            if(t>=-tol && t<=1+tol){
               t=clamp(t, 0., 1.);
               if(std::fabs(signed_volume((1-t)*x0+t*xnew0, (1-t)*x1+t*xnew1, (1-t)*x2+t*xnew2, (1-t)*x3+t*xnew3))<convergence_tol)
                  possible_times.push_back(t);
            }
         }else{ // two separate real roots
            double t0, t1;
            if(C>0) t0=(-C-std::sqrt(discriminant))/(2*B);
            else    t0=(-C+std::sqrt(discriminant))/(2*B);
            t1=D/(B*t0);
            if(t1<t0) swap(t0,t1);
            if(t0>=-tol && t0<=1+tol) possible_times.push_back(clamp(t0, 0., 1.));
            if(t1>=-tol && t1<=1+tol) add_unique(possible_times, clamp(t1, 0., 1.));
         }
         return;
      }else{ // cubic is not monotone: divide up [0,1] accordingly =====================================
         double t0, t1;
         if(B>0) t0=(-B-std::sqrt(discriminant))/(3*A);
         else    t0=(-B+std::sqrt(discriminant))/(3*A);
         t1=C/(3*A*t0);
         if(t1<t0) swap(t0,t1);
         interval_times.push_back(0);
         if(t0>0 && t0<1)
            interval_times.push_back(t0);
         if(t1>0 && t1<1)
            interval_times.push_back(t1);
         interval_times.push_back(1);
      }
   }

   // look for roots in indicated intervals ==============================================================
   // evaluate coplanarity more accurately at each endpoint of the intervals
   std::vector<double> interval_values(interval_times.size());
   for(unsigned int i=0; i<interval_times.size(); ++i){
      double t=interval_times[i];
      interval_values[i]=signed_volume((1-t)*x0+t*xnew0, (1-t)*x1+t*xnew1, (1-t)*x2+t*xnew2, (1-t)*x3+t*xnew3);
   }
   // first look for interval endpoints that are close enough to zero, without a sign change
   for(unsigned int i=0; i<interval_times.size(); ++i){
      if(interval_values[i]==0){
         possible_times.push_back(interval_times[i]);
      }else if(std::fabs(interval_values[i])<convergence_tol){
         if((i==0 || (interval_values[i-1]>=0 && interval_values[i]>=0) || (interval_values[i-1]<=0 && interval_values[i]<=0))    
          &&(i==interval_times.size()-1 || (interval_values[i+1]>=0 && interval_values[i]>=0) || (interval_values[i+1]<=0 && interval_values[i]<=0))){
            possible_times.push_back(interval_times[i]);
         }
      }
   }
   // and then search in intervals with a sign change
   for(unsigned int i=1; i<interval_times.size(); ++i){
      double tlo=interval_times[i-1], thi=interval_times[i], tmid;
      double vlo=interval_values[i-1], vhi=interval_values[i], vmid;
      if((vlo<0 && vhi>0) || (vlo>0 && vhi<0)){
         // start off with secant approximation (in case the cubic is actually linear)
         double alpha=vhi/(vhi-vlo);
         tmid=alpha*tlo+(1-alpha)*thi;
         for(int iteration=0; iteration<50; ++iteration){
            vmid=signed_volume((1-tmid)*x0+tmid*xnew0, (1-tmid)*x1+tmid*xnew1,
                               (1-tmid)*x2+tmid*xnew2, (1-tmid)*x3+tmid*xnew3);
            if(std::fabs(vmid)<1e-2*convergence_tol) break;
            if((vlo<0 && vmid>0) || (vlo>0 && vmid<0)){ // if sign change between lo and mid
               thi=tmid;
               vhi=vmid;
            }else{ // otherwise sign change between hi and mid
               tlo=tmid;
               vlo=vmid;
            }
            if(iteration%2) alpha=0.5; // sometimes go with bisection to guarantee we make progress
            else alpha=vhi/(vhi-vlo); // other times go with secant to hopefully get there fast
            tmid=alpha*tlo+(1-alpha)*thi;
         }
         possible_times.push_back(tmid);
      }
   }
   sort(possible_times.begin(), possible_times.end());
}

bool check_edge_edge_collision(const Vec3d &x0, const Vec3d &x1, const Vec3d &x2, const Vec3d &x3,
                               const Vec3d &xnew0, const Vec3d &xnew1, const Vec3d &xnew2, const Vec3d &xnew3,
                               double collision_epsilon)
{
   std::vector<double> possible_times;
   find_coplanarity_times(x0, x1, x2, x3, xnew0, xnew1, xnew2, xnew3, possible_times);
   for(unsigned int a=0; a<possible_times.size(); ++a){
      double t=possible_times[a];
      Vec3d xt0=(1-t)*x0+t*xnew0, xt1=(1-t)*x1+t*xnew1, xt2=(1-t)*x2+t*xnew2, xt3=(1-t)*x3+t*xnew3;
      double distance;
      check_edge_edge_proximity(xt0, xt1, xt2, xt3, distance);
      if(distance<collision_epsilon)
         return true;
   }
   return false;
}

bool check_edge_edge_collision(const Vec3d &x0, const Vec3d &x1, const Vec3d &x2, const Vec3d &x3,
                               const Vec3d &xnew0, const Vec3d &xnew1, const Vec3d &xnew2, const Vec3d &xnew3,
                               double &s0, double &s2, Vec3d &normal, double &t, double collision_epsilon)
{
   std::vector<double> possible_times;
   find_coplanarity_times(x0, x1, x2, x3, xnew0, xnew1, xnew2, xnew3, possible_times);
   for(unsigned int a=0; a<possible_times.size(); ++a){
      t=possible_times[a];
      Vec3d xt0=(1-t)*x0+t*xnew0, xt1=(1-t)*x1+t*xnew1, xt2=(1-t)*x2+t*xnew2, xt3=(1-t)*x3+t*xnew3;
      double distance;
      check_edge_edge_proximity(xt0, xt1, xt2, xt3, distance, s0, s2, normal);
      if(distance<collision_epsilon){
         // now figure out a decent normal
         if(distance<1e-2*collision_epsilon){ // if we don't trust the normal...
            // first try the cross-product of edges at collision time
            normal=cross(xt1-xt0, xt3-xt2);
            double m=mag(normal);
            if(m>sqr(collision_epsilon)){
               normal/=m;
            }else{
               // if that didn't work, try cross-product of edges at the start
               normal=cross(x1-x0, x3-x2);
               m=mag(normal);
               if(m>sqr(collision_epsilon)){
                  normal/=m;
               }else{
                  // if that didn't work, try vector between points at the start
                  normal=(s2*x2+(1-s2)*x3)-(s0*x0+(1-s0)*x1);
                  m=mag(normal);
                  if(m>collision_epsilon){
                     normal/=m;
                  }else{
                     // if that didn't work, boy are we in trouble; just get any non-parallel vector
                     Vec3d dx=xt1-xt0;
                     if(dx[0]!=0 || dx[1]!=0){
                        normal=Vec3d(dx[1], -dx[0], 0);
                        normal/=mag(normal);
                     }else{
                        dx=xt3-xt2;
                        if(dx[0]!=0 || dx[1]!=0){
                           normal=Vec3d(dx[1], -dx[0], 0);
                           normal/=mag(normal);
                        }else{
                           normal=Vec3d(0, 1, 0); // the last resort
                        }
                     }
                  }
               }
            }
         }
         return true;
      }
   }
   return false;
}

bool check_point_triangle_collision(const Vec3d &x0, const Vec3d &x1, const Vec3d &x2, const Vec3d &x3,
                                    const Vec3d &xnew0, const Vec3d &xnew1, const Vec3d &xnew2, const Vec3d &xnew3,
                                    double collision_epsilon)
{
   std::vector<double> possible_times;
   find_coplanarity_times(x0, x1, x2, x3, xnew0, xnew1, xnew2, xnew3, possible_times);
   for(unsigned int a=0; a<possible_times.size(); ++a){
      double t=possible_times[a];
      Vec3d xt0=(1-t)*x0+t*xnew0, xt1=(1-t)*x1+t*xnew1, xt2=(1-t)*x2+t*xnew2, xt3=(1-t)*x3+t*xnew3;
      double distance;
      check_point_triangle_proximity(xt0, xt1, xt2, xt3, distance);
      if(distance<collision_epsilon)
         return true;
   }
   return false;
}

bool check_point_triangle_collision(const Vec3d &x0, const Vec3d &x1, const Vec3d &x2, const Vec3d &x3,
                                    const Vec3d &xnew0, const Vec3d &xnew1, const Vec3d &xnew2, const Vec3d &xnew3,
                                    double &s1, double &s2, double &s3, Vec3d &normal, double &t, double collision_epsilon)
{
   std::vector<double> possible_times;
   find_coplanarity_times(x0, x1, x2, x3, xnew0, xnew1, xnew2, xnew3, possible_times);
   for(unsigned int a=0; a<possible_times.size(); ++a){
      t=possible_times[a];
      Vec3d xt0=(1-t)*x0+t*xnew0, xt1=(1-t)*x1+t*xnew1, xt2=(1-t)*x2+t*xnew2, xt3=(1-t)*x3+t*xnew3;
      double distance;
      check_point_triangle_proximity(xt0, xt1, xt2, xt3, distance, s1, s2, s3, normal);
      if(distance<collision_epsilon){
         // now figure out a decent normal
         if(distance<1e-2*collision_epsilon){ // if we don't trust the normal...
            // first try the triangle normal at collision time
            normal=cross(xt2-xt1, xt3-xt1);
            double m=mag(normal);
            if(m>sqr(collision_epsilon)){
               normal/=m;
            }else{
               // if that didn't work, try triangle normal at start
               normal=cross(x2-x1, x3-x1);
               m=mag(normal);
               if(m>sqr(collision_epsilon)){
                  normal/=m;
               }else{
                  // if that didn't work, try vector between points at the start
                  normal=(s1*x1+s2*x2+s3*x3)-x0;
                  m=mag(normal);
                  if(m>collision_epsilon){
                     normal/=m;
                  }else{
                     // if that didn't work, boy are we in trouble; just get any non-parallel vector
                     Vec3d dx=xt2-xt1;
                     if(dx[0]!=0 || dx[1]!=0){
                        normal=Vec3d(dx[1], -dx[0], 0);
                        normal/=mag(normal);
                     }else{
                        dx=xt3-xt1;
                        if(dx[0]!=0 || dx[1]!=0){
                           normal=Vec3d(dx[1], -dx[0], 0);
                           normal/=mag(normal);
                        }else{
                           normal=Vec3d(0, 1, 0); // the last resort
                        }
                     }
                  }
               }
            }
         }
         return true;
      }
   }
   return false;
}

