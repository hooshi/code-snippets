#ifndef MARCHINGSQUARES_H
#define MARCHINGSQUARES_H
#include "vec.h"
#include "array2.h"

//TODO Fix cases to ensure consistent orientation of edges
//so we know the outward normal automatically.

typedef float (*implicit_surface)(const Vec2f& position);

class MarchingSquares {

public:
   MarchingSquares(Vec2f min_point_, float dx_, int nx_, int ny_, implicit_surface surface_);
   MarchingSquares(Vec2f min_point_, float dx_, int nx_, int ny_, Array2f& grid_data_);
      
   void march_lines();
   void march_polys();
   
   //each poly consists of several points   
   std::vector< std::vector<Vec2f> > polys;

   //list of verts and edges for closed meshes
   std::vector<Vec2f> verts;
   std::vector<Vec2ui> edges;

private:
   bool function_call;

   Array2f grid_data;
   implicit_surface surface;
   Vec2f min_point;
   float dx;
   int nx,ny;
};

#endif

