// std
#include <memory>
#include <type_traits>
#include <utility>
#include <string>

// Eigen
#include <Eigen/Core>

// CGAL
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/Surface_mesh.h>


// ==================================================
//   eigen_utils
// ==================================================
namespace eigen_utils {
template<typename Derived>
using IsEigenType = std::is_base_of<Eigen::PlainObjectBase<Derived>, Derived>;
 
template<typename Derived>
using AssertEigenType = typename std::enable_if< IsEigenType<Derived>::value, Derived >::type;

  
template<typename Derived>
using Ref = Eigen::Ref< AssertEigenType<Derived> >;

template<typename Derived>
using ConstRef = const Eigen::Ref< const AssertEigenType<Derived> >;
}

// ==================================================
//   cgal_utils
// ==================================================
namespace cgal_utils {

// Kernels
namespace kernels {
template<typename Scalar>
using EPIC = CGAL::Exact_predicates_inexact_constructions_kernel;
template<typename Scalar>
using EPEC = CGAL::Exact_predicates_exact_constructions_kernel;
}

// Default CGAL types
using Scalar = double;
using Kernel = kernels::EPIC<Scalar>;
using Point3D = Kernel::Point_3;
using SurfaceMesh3D = CGAL::Surface_mesh<Point3D>;

// Some common mesh operations
std::unique_ptr<SurfaceMesh3D> create_mesh(
    eigen_utils::ConstRef<Eigen::Matrix3Xd> vertices,
    eigen_utils::ConstRef<Eigen::Matrix3Xi> facets
);

Eigen::Matrix3Xd get_vertices(const SurfaceMesh3D&);
Eigen::Matrix3Xi get_facets(const SurfaceMesh3D&);

void save_mesh_OFF(const std::string fname, const SurfaceMesh3D&);


}


