#include <cassert>
#include <fstream>

#include "./utils.h"

// Range
namespace {
template <typename T>
class Range;
template <typename T>
Range<T> range(T end) {
    return Range<T>(T(0), end);
}
template <typename T>
Range<T> range(T begin, T end) {
    return Range<T>(begin, end);
}
template <typename T>
class Range {
public:
    Range(T begin, T end)
    : m_begin(begin)
    , m_end(end) {}

    class iterator : std::iterator<std::input_iterator_tag, T> {
    public:
        explicit iterator(T val, const Range<T>& c)
        : m_value(val)
        , m_context(c) {}

        const T& operator*() const { return m_value; }

        iterator& operator++() {
            ++m_value;
            return *this;
        }

        bool operator!=(const iterator other) const {
            if (m_context == other.m_context) {
                return m_value != other.m_value;
            } else {
                return true;
            }
        }

    private:
        T m_value;
        const Range<T>& m_context;
    };

    iterator begin() const { return iterator(m_begin, *this); }

    iterator end() const { return iterator(m_end, *this); }

    bool operator==(const Range<T>& other) const { return m_begin == other.m_begin && m_end == other.m_end; }

private:
    T m_begin;
    T m_end;
};
} // namespace



// Short hand
namespace eu = eigen_utils;


namespace cgal_utils {

std::unique_ptr<SurfaceMesh3D> create_mesh(eu::ConstRef<Eigen::Matrix3Xd> vertices,
    eu::ConstRef<Eigen::Matrix3Xi> facets) {
    using VertexIndex = SurfaceMesh3D::Vertex_index;
    using FacetIndex = SurfaceMesh3D::Face_index;

    auto mesh = std::make_unique<SurfaceMesh3D>();

    for (const auto i : range(vertices.cols())) {
        Point3D vertex(vertices(0, i), vertices(1, i), vertices(2, i));
        VertexIndex vertex_id = mesh->add_vertex(vertex);
        assert((VertexIndex)i == vertex_id);
    }

    for (const auto i : range(facets.cols())) {
        VertexIndex v0(facets(0, i));
        VertexIndex v1(facets(1, i));
        VertexIndex v2(facets(2, i));
        FacetIndex facet_id = mesh->add_face(v0, v1, v2);
        assert((FacetIndex)i == facet_id);
    }

    return mesh;
} // create_mesh()

Eigen::Matrix3Xd get_vertices(const SurfaceMesh3D& mesh) {
    using Index = Eigen::Matrix3Xi::Scalar;
    using Scalar = Eigen::Matrix3Xd::Scalar;
    const auto num_vertices = mesh.number_of_vertices();
    Eigen::Matrix3Xd vertices(3, num_vertices);
    Index vertex_offset = 0;
    for (auto vertex_iter : range(mesh.vertices_begin(), mesh.vertices_end())) {
        const auto vertex_id = *vertex_iter;
        const auto& position = mesh.point(vertex_id);
        vertices.col(vertex_offset) << position.x(), position.y(), position.z();
        ++vertex_offset;
    }
    return vertices;
}

Eigen::Matrix3Xi get_facets(const SurfaceMesh3D& mesh) {
    using Index = Eigen::Matrix3Xi::Scalar;
    const auto num_facets = mesh.number_of_faces();
    Eigen::Matrix3Xi facets(3, num_facets);
    Index facet_offset = 0;
    for (auto facet_iter : range(mesh.faces_begin(), mesh.faces_end())) {
        const auto facet_id = *facet_iter;
        const auto half_edge_id = mesh.halfedge(facet_id);
        const auto vertex_range = mesh.vertices_around_face(half_edge_id);
        Index vertex_offset = 0;
        for (auto vertex_iter : range(vertex_range.begin(), vertex_range.end())) {
            const auto vertex_id = *vertex_iter;
            assert(vertex_offset < 3);
            facets(vertex_offset, facet_offset) = Index(vertex_id);
            ++vertex_offset;
        }
        ++facet_offset;
    }
    return facets;
}


void save_mesh_OFF(const std::string fname, const SurfaceMesh3D& mesh) {
    std::ofstream fstr(fname);
    fstr << mesh;
} // save_mesh

} // namespace cgal_utils

//========================================
#define SHOULD_COMPILE_TEST
#if defined(SHOULD_COMPILE_TEST)

#include <cstdlib>

int main() {
    Eigen::Matrix3Xd vertices(3, 3);
    Eigen::Matrix3Xi facets(3, 1);

    vertices.col(0) << 0, 0, 0;
    vertices.col(1) << 1, 0, 0;
    vertices.col(2) << 0, 1, 0;

    facets.col(0) << 0, 1, 2;

    auto mesh = cgal_utils::create_mesh(vertices, facets);
    std::cout << " num_vertices: " << mesh->number_of_vertices() << "\n";
    std::cout << " num_facets: " << mesh->number_of_faces() << "\n";
    cgal_utils::save_mesh_OFF("test.off", *mesh);
    std::cout << " F: \n" << cgal_utils::get_facets(*mesh) << "\n";
    std::cout << " V: \n" << cgal_utils::get_vertices(*mesh) << "\n";
    mesh->property_stats();

    return EXIT_SUCCESS;
}

#endif
