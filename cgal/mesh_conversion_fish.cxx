/*
 * mesh_conversion_fish.cxx
 *
 * Create a mesh with the Surface Mesh module (it has a logo of a fish)
 */


#include <CGAL/Simple_cartesian.h>
#include <CGAL/Surface_mesh.h>

#include <cstdio>
#include <cassert>
#include <vector>


using namespace std;

typedef CGAL::Simple_cartesian<double> CGAL_Kernel;
typedef CGAL::Surface_mesh<CGAL_Kernel::Point_3> CGAL_SurfaceMesh;

void
CGAL_mesh2cgalsurfacemesh(Mesh & mesh, CGAL_SurfaceMesh & cgal_mesh)
{
  // https://stackoverflow.com/questions/45652756/cgal-surface-mesh-connectivity

  // vertices
  for(int i = 0; i < mesh.verts.n_mems(); i++)
  {
    Vertex * vert = mesh.verts.get_member_ptr(i);
    CGAL_SurfaceMesh::Vertex_index vh =
        cgal_mesh.add_vertex(CGAL_Kernel::Point_3(vert->xyz(0), vert->xyz(1), vert->xyz(2)));
    assert(i == (int)vh);
  }

  // triangles
  for(int i = 0; i < mesh.faces.n_mems(); i++)
  {
    Face * face = mesh.faces.get_member_ptr(i);
    CGAL_SurfaceMesh::Vertex_index v0(face->he->vert->idx());
    CGAL_SurfaceMesh::Vertex_index v1(face->he->next->vert->idx());
    CGAL_SurfaceMesh::Vertex_index v2(face->he->next->next->vert->idx());
    CGAL_SurfaceMesh::Face_index fh = cgal_mesh.add_face(v0, v1, v2);
    assert(i == (int)fh);
  }
}

void
CGAL_cgalsurfacemesh2mesh(CGAL_SurfaceMesh & cgal_mesh, Mesh & mesh)
{
  std::vector<double> coords;
  std::vector<int> conn;

  for(CGAL_SurfaceMesh::Vertex_iterator it = cgal_mesh.vertices_begin(); it != cgal_mesh.vertices_end(); ++it)
  {
    coords.push_back(cgal_mesh.point(*it).x());
    coords.push_back(cgal_mesh.point(*it).y());
    coords.push_back(cgal_mesh.point(*it).z());
  }

  for(CGAL_SurfaceMesh::Face_iterator it = cgal_mesh.faces_begin(); it != cgal_mesh.faces_end(); ++it)
  {
    CGAL_SurfaceMesh::Halfedge_index he = cgal_mesh.halfedge(*it);
    CGAL_SurfaceMesh::Vertex_around_face_range iter_range = cgal_mesh.vertices_around_face(he);
    for(CGAL_SurfaceMesh::Vertex_around_face_iterator iter = iter_range.begin(); iter != iter_range.end(); ++iter)
    {
      conn.push_back(int(*iter));
    }
  }

  mesh.init(coords, conn);
}


#define TEST_ME_PLEASE
#if     defined(TEST_ME_PLEASE)

// int main()
// {
//   // Let's create a dummy mesh
//   std::vector<double> verts =
//     {
//       0,0,0, // vert 0
//       1,0,0, // vert 1
//       1,1,0, // vert 2
//       0,1,0, // vert 3      
//     };
//   std::vector<int>   face2vert =
//     {
//       0, 1, 3, // face 0
//       1, 2, 3, // face 1
//     };

//   // Let's convert this to CGAL
//   CGAL_polyhedron_with_id poly;
//   CGAL_polyhedron_builder<CGAL_polyhedron_with_id> builder(verts, face2vert);
//   poly.delegate(builder);

//   // Now let's print poly's information
//   print_cgal_polyhedron_with_id(poly);
				  
//   return 0;
// }

#endif
