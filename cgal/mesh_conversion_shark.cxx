/*
 * mesh_conversion.cxx
 *
 * Code for creating a CGAL mesh with control over vertex and face
 * numbers.  And also sample code for accessing the mesh to convert
 * the mesh back to your own data structure.
 *
 * References:
 *
 * id: http://cgal-discuss.949826.n4.nabble.com/Getting-facet-indexes-from-polyhedron-3-td4553195.html
 *
 * builder: cgal-discuss.949826.n4.nabble.com/Getting-facet-indexes-from-polyhedron-3-td4553195.html
 *
 * Create a mesh with the 3D Polyhedral Surface module (it has a logo of a hammer head shark)
 */


#include <CGAL/Simple_cartesian.h>
#include <CGAL/Polyhedron_incremental_builder_3.h>
#include <CGAL/Polyhedron_items_with_id_3.h>
#include <CGAL/Polyhedron_3.h>

#include <cstdio>
#include <cassert>
#include <vector>


using namespace std;


// CGAL is dependant upon some notion of kernel. Let's use the
// CGAL::Simple_cartesian<double>. I have no idea how it defers from
// the other kernels.
typedef CGAL::Simple_cartesian<double>     CGAL_kernel;


// A surface mesh is a polyhedron in cgal. Depending what polyhedron
// items (classes for halfedge, vertex and face), and what kernel you
// want to use, you can create different polyhedrons.
//
// Here I will use CGAL::Polyhedron_items_with_id_3 (which allows an
// integer id to be saved for each vertex, face and halfedge) and the
// previously typedefed kernel.
typedef CGAL::Polyhedron_3<CGAL_kernel, CGAL::Polyhedron_items_with_id_3>  CGAL_polyhedron_with_id;

// You can also create your custom data structres. This would be involved
// look at  https://doc.cgal.org/latest/Polyhedron/index.html
//
// Somehow define CGAL_polyhedron_items_custom.
// typedef CGAL::Polyhedron_3<CGAL_Kernel, CGAL_polyhedron_items_custom>  CGAL_polyhedron_custom;



// A class to create a CGAL_Polyhedron
// Usage:
// CGAL_polyhedron_with_id poly; (or any other polyhedron)
// CGAL_polyhedron_builder<CGAL_polyhedron_with_id> builder(vert_pos, face2vert);
// poly.delegate(builder);
template <class Polyhedron>
class CGAL_polyhedron_builder : public CGAL::Modifier_base<typename Polyhedron::HalfedgeDS>
{
public:

  // Some typedefs for convenience.
  typedef typename Polyhedron::HalfedgeDS Mesh_data_structure;
  //
  typedef typename Mesh_data_structure::Vertex_handle  Vertex_handle;
  typedef typename Mesh_data_structure::Vertex  Vertex;
  typedef typename Mesh_data_structure::Vertex_iterator  Vertex_iterator;
  typedef typename Vertex::Point                Point;
  //
  typedef typename Mesh_data_structure::Face_handle      Face_handle;
  typedef typename Mesh_data_structure::Face_iterator    Face_iterator;
  //
  typedef typename Mesh_data_structure::Halfedge_handle      Halfedge_handle;
  typedef typename Mesh_data_structure::Halfedge_iterator    Halfedge_iterator;

  // The mesh builder needs to know the mesh structure
  // vertex_pos, a nvert*3 vector of positions of vertices
  // face2vert a nface*3 vector of indices of vertices on each face
  CGAL_polyhedron_builder(const std::vector<double>& vertex_pos,
		    const std::vector<int>& face2vert)
    : _vertex_pos(vertex_pos)
    , _face2vert(face2vert)
  {}

  // This function will be internally called by CGAL's polygon.deletegate()
  // The polygon(mesh) should be built here.
  void operator()(Mesh_data_structure& cgal_mesh_ds)
  {
    assert( _vertex_pos.size() % 3 == 0 );
    assert( _face2vert.size() % 3 == 0 );
    int n_vertices = _vertex_pos.size() / 3;
    int n_faces =    _face2vert.size() / 3;

    // A builder object that creates the mesh.
    // Input: cgal_mesh, mesh data structure to be build, true, be verbose
    //        and prin
    CGAL::Polyhedron_incremental_builder_3<Mesh_data_structure> builder(cgal_mesh_ds, true);

    // Start creating the surfaces
    builder.begin_surface(n_vertices, n_faces);

    // Add the vertices
    for ( int i = 0 ; i < n_vertices ; ++i )
      {
	// Create the vertex
	const double x = _vertex_pos[3*i + 0];
	const double y = _vertex_pos[3*i + 1];
	const double z = _vertex_pos[3*i + 2];
        Vertex_handle vh = builder.add_vertex( Point(x,y,z) );

	// Set its id.
	vh->id() = i;
      }

    // Add the faces
    for ( int i = 0 ; i < n_faces ; ++i )
      {
	// Create the face
        const int vertid0 = _face2vert[3*i+0];
	const int vertid1 = _face2vert[3*i+1];
	const int vertid2 = _face2vert[3*i+2];
	
	Face_handle fh = builder.begin_facet();
	builder.add_vertex_to_facet(vertid0);
	builder.add_vertex_to_facet(vertid1);
	builder.add_vertex_to_facet(vertid2);
	builder.end_facet();

	// Append the face number to it.
	fh->id() = i;
      }

   // end surface
    builder.end_surface();

    // Optionally you can also give id's to the half edges too.
    // {
    //   Haldedge_iterator heit = cgal_mesh_ds.halfedges_begin();
    //   Haldedge_iterator heit_end = cgal_mesh_ds.halfedges_begin();
      
    //   for(; heit != heit_end ; ++heit)
    // 	{
    // 	  if( (heit->vertex()->id() == 1)
    // 	      &&  (heit->twin()->vertex->id() == 2)
    // 	    {
    // 	      heit->id() = ... ;
    // 	    }
    // 	}
    // }

  }

private:
  // Position of vertices
  std::vector<double> _vertex_pos;
  // Indices of vertices corresponding to every triangle.
  std::vector<int>    _face2vert;
};

// This function shows how to access the data within a CGAL
// polyhedron.  It just prints everything but it can be used as an
// example for any job that needs access to the mesh.
void print_cgal_polyhedron_with_id(CGAL_polyhedron_with_id &poly)
{
  typedef CGAL_polyhedron_with_id::Vertex_iterator    Vertex_iterator;
  typedef CGAL_polyhedron_with_id::Face_iterator      Face_iterator;
  typedef CGAL_polyhedron_with_id::Halfedge_iterator  Halfedge_iterator;
  typedef CGAL_polyhedron_with_id::Halfedge_around_facet_circulator Halfedge_around_facet_circulator;

  // Print the vertex id's and their 
  for(Vertex_iterator it = poly.vertices_begin(); it != poly.vertices_end() ; ++it)
    {
      printf("Vertex id %d: %f %f %f \n", (int)it->id(), it->point().x(), it->point().y(), it->point().z() );
    }

  // Print vertices on a face
  for(Face_iterator it = poly.facets_begin(); it != poly.facets_end() ; ++it)
    {
      printf("Face %d verts: ", (int)it->id());
      Halfedge_around_facet_circulator circ = it->facet_begin();
      do
	{
	  printf("%d, ", (int)circ->vertex()->id() );
	} while ( ++circ != it->facet_begin());
      printf("\n");
    }
}


#define TEST_ME_PLEASE
#if     defined(TEST_ME_PLEASE)

int main()
{
  // Let's create a dummy mesh
  std::vector<double> verts =
    {
      0,0,0, // vert 0
      1,0,0, // vert 1
      1,1,0, // vert 2
      0,1,0, // vert 3      
    };
  std::vector<int>   face2vert =
    {
      0, 1, 3, // face 0
      1, 2, 3, // face 1
    };

  // Let's convert this to CGAL
  CGAL_polyhedron_with_id poly;
  CGAL_polyhedron_builder<CGAL_polyhedron_with_id> builder(verts, face2vert);
  poly.delegate(builder);

  // Now let's print poly's information
  print_cgal_polyhedron_with_id(poly);
				  
  return 0;
}

#endif
