#include <unsupported/Eigen/NonLinearOptimization>

#include "bezier.hpp"

// ============= EIGEN INTERFACE FOR MARQUART_LEVENBERG

int
BezierFitter::inputs()
{
  return n_params();
}

int
BezierFitter::values()
{
  return n_equations();
}

int
BezierFitter::operator()(const Eigen::VectorXd & x, Eigen::VectorXd & fvec)
{
  const double tol = 1e-10;

  if((get_params() - x).norm() > tol)
  {
    set_params(x);
    compute_fitting_objective_and_jacobian(_cached_objective, _cached_jacobian);
  }

  fvec = _cached_objective;
  return 0;
}

int
BezierFitter::df(const Eigen::VectorXd & x, Eigen::MatrixXd & fjac)
{
  const double tol = 1e-10;

  if((get_params() - x).norm() > tol)
  {
    set_params(x);
    compute_fitting_objective_and_jacobian(_cached_objective, _cached_jacobian);
  }

  fjac = _cached_jacobian;
  return 0;
}

// Separate file as it takes ages to compile.
void
BezierFitter::run_fitter(bool verbose, std::function<void(int)> callback)
{
  Eigen::VectorXd x0 = get_params();
  Eigen::VectorXd x = x0;

  // Init Eigen
  Eigen::LevenbergMarquardt<BezierFitter> lm(*this);
  using Eigen::LevenbergMarquardtSpace::Running;
  using Eigen::LevenbergMarquardtSpace::Status;

  // Init the jacobian and obj.
  compute_fitting_objective_and_jacobian(_cached_objective, _cached_jacobian);

  Status status = lm.minimizeInit(x);
  int iter_id = 0;
  printf(" ==== MARQURT-LEVENBERG for Bezier Fitting ===== \n");
  printf(" %10s %20s %20s \n", "ITER", "OBJ", "GRAD");
  fflush(stdout);
  do
  {
    status = lm.minimizeOneStep(x);
    if(verbose)
    {
      if((iter_id % 10 == 0) || (status != Running))
      {
        printf(" %10d %20.8e %20.8e \n",
            iter_id,
            _cached_objective.norm(),
            (_cached_jacobian.transpose() * _cached_objective).norm());
        fflush(stdout);
      }
    }
    ++iter_id;
    callback(iter_id);
  } while(status == Running);

  switch(status)
  {
#define TAKECAREOF(XX) \
  case(Eigen::LevenbergMarquardtSpace::XX): printf("FINISHED, Status: %s \n", #XX); break

    TAKECAREOF(NotStarted);
    TAKECAREOF(Running);
    TAKECAREOF(ImproperInputParameters);
    TAKECAREOF(RelativeReductionTooSmall);
    TAKECAREOF(RelativeErrorTooSmall);
    TAKECAREOF(RelativeErrorAndReductionTooSmall);
    TAKECAREOF(CosinusTooSmall);
    TAKECAREOF(TooManyFunctionEvaluation);
    TAKECAREOF(FtolTooSmall);
    TAKECAREOF(XtolTooSmall);
    TAKECAREOF(GtolTooSmall);
    TAKECAREOF(UserAsked);

#undef TAKECAREOF
  };

  set_params(x);
  fflush(stdout);
}
