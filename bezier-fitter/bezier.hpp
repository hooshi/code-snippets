#ifndef PIXVEC_BEZIER_FITTER_IS_INCLUDED
#define PIXVEC_BEZIER_FITTER_IS_INCLUDED

#include <functional>
#include <vector>

#include <Eigen/Core>


class BezierCurve
{

public:
  BezierCurve() = default;

  Eigen::Vector2d pos(const double t) const;
  Eigen::Vector2d dposdt(const double t) const;
  Eigen::Vector2d dposdtdt(const double t) const;

  Eigen::Matrix2Xd dposdparams(const double t) const;
  Eigen::Matrix2Xd dposdtdparams(const double t) const;

  double project(const Eigen::Vector2d & point) const;
  Eigen::VectorXd dtprojectdparams(const double t, const Eigen::Vector2d & point);
  Eigen::Matrix2Xd dposprojectdparams(const double t, const Eigen::VectorXd& dtprojectdparams);

  void set_control_points(const Eigen::Matrix2Xd& control_points_d0_in);
  const Eigen::Matrix2Xd& get_control_points();
  int n_control_points(){return 4;}

  const Eigen::Matrix3Xd& get_tesselation();


  double length();
  Eigen::VectorXd dlengthdparams();

private:
  constexpr static unsigned _n_tesselation = 1000;

  static double _bernstein_value_at(double t, double const * c_, unsigned n);
  static Eigen::Vector2d _bezier_value_at(const Eigen::Matrix2Xd& _control_points, const double t);
  static Eigen::Matrix2Xd _derivative_bezier_curve(const Eigen::Matrix2Xd& _control_points);
  static Eigen::Matrix3Xd _tesselate(const Eigen::Matrix2Xd &_control_points, const int n_sampling);

  Eigen::Matrix2Xd _control_points_d0;
  Eigen::Matrix2Xd _control_points_d1;
  Eigen::Matrix2Xd _control_points_d2;
  Eigen::Matrix3Xd _tesselation;

  const static std::vector<double>& _get_gauss_quad_weights();
  const static std::vector<double>& _get_gauss_quad_locs();
};

class BezierFitter
{
public:
  using PointWeightFeeder = std::function<void(int, Eigen::Vector2d &, double &)>;

  BezierFitter(PointWeightFeeder point_feeder, const int num_points, bool are_end_points_fixed = true);

  Eigen::Matrix2Xd get_control_points();
  void set_control_points(const Eigen::Matrix2Xd &);

  void tune_optimizer(double length_weight=0.);

  void run_fitter(bool verbose=true, std::function<void(int)> callback=[](int){});


  // Optimization
  int n_params();
  int n_equations();
  Eigen::VectorXd get_params();
  void set_params(const Eigen::VectorXd &);

  Eigen::MatrixXd compute_dbezierparams_dfitparams();
  void compute_fitting_objective_and_jacobian(Eigen::VectorXd & obj, Eigen::MatrixXd & jacobian);

  BezierCurve  & get_bezier();
  int get_n_points_to_fit();
  PointWeightFeeder get_point_feeder();

private:
  Eigen::Vector2d _beg_pt; // fixed or free
  Eigen::Vector2d _end_pt; // fixed or free
  Eigen::Vector2d _beg_tangent_dir; // fixed
  Eigen::Vector2d _end_tangent_dir; // fixed
  double _beg_tangent_len; // free
  double _end_tangent_len; // free

  BezierCurve _bezier_curve;
  Eigen::MatrixXd _cached_jacobian;
  Eigen::VectorXd _cached_objective;
  

  PointWeightFeeder _point_feeder;
  int _n_points_to_fit;
  bool _are_end_points_fixed;
  // bool _are_tangents_fixed; // for now always true


  double _length_weight;

  // ============== API needed by Eigen Levenberg-Marquart
public:
  enum
  {
    InputsAtCompileTime = Eigen::Dynamic,
    ValuesAtCompileTime = Eigen::Dynamic
  };
  typedef Eigen::VectorXd InputType;
  typedef Eigen::VectorXd ValueType;
  typedef Eigen::MatrixXd JacobianType;

  int inputs();
  int values();
  int operator()(const Eigen::VectorXd & x, Eigen::VectorXd & fvec);
  int df(const Eigen::VectorXd & x, Eigen::MatrixXd & fjac);
};

#endif