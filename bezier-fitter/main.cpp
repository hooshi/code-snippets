#include <cstdio>

extern int
test_vtk_writer(int argc, char ** argv);
extern int
test_bezier(int argc, char ** argv);
extern int
test_fitter(int argc, char ** argv);

int main(int argc, char ** argv)
{
  printf("========== TESTING VTK WRITER =========== \n");
  test_vtk_writer(argc, argv);
  printf("==========        DONE        =========== \n");

  printf("========== TESTING BEZIER     =========== \n");
  test_bezier(argc, argv);
  printf("==========        DONE        =========== \n");

  printf("========== TESTING BEZIER FIITER =========== \n");
  test_fitter(argc, argv);
  printf("==========        DONE        =========== \n");

  return 0;
}
