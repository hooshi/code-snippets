#ifndef TEST_TOOLS_IS_INCLUDED
#define TEST_TOOLS_IS_INCLUDED

#include <sstream>
#include <Eigen/Core>

#ifndef STR
#define STR(X) static_cast<std::ostringstream&>(std::ostringstream().flush() << X).str()
#endif


inline Eigen::MatrixXd
normal_dist(const Eigen::MatrixXd & in)
{
  Eigen::MatrixXd out(in.rows(), in.cols());
  const double sqrtpiinv = 1. / std::sqrt(2 * 3.14); // don't use for places where need accurate answer

  for(unsigned i = 0; i < in.rows(); ++i)
  {
    for(unsigned j = 0; j < in.cols(); ++j)
    {
      out(i, j) = sqrtpiinv * std::exp(-in(i, j) * in(i, j) / 2.);
    }
  }
  return out;
}

inline Eigen::MatrixXd
randn(unsigned int i, unsigned int j)
{
  return normal_dist(Eigen::MatrixXd::Random(i, j));
}


#endif //TEST_TOOLS_IS_INCLUDED