#include "test_tools.hpp"

#include "../bezier.hpp"
#include "../deriv_test.hpp"
#include "../vtk_curve_writer.hpp"

namespace
{
 
void
test_projection(BezierFitter bzf, bool test_derivative)
{

  printf("-- Testing Bezier Fitter \n");

  static int n_called = -1;
  ++n_called;

 //
 // A macro to write the bezier curve
 //
  auto write_all = [&](int n_iteration) {
    VtkCurveWriter writer;
    writer.add_polyline(bzf.get_bezier().get_control_points());
    writer.add_polyline(bzf.get_bezier().get_tesselation().bottomRows<2>());
    if(n_iteration == 0)
    {
      for(int i = 0; i < bzf.get_n_points_to_fit(); ++i)
      {
        Eigen::Vector2d pt;
        double w;
        bzf.get_point_feeder()(i, pt, w);
        writer.add_point(pt);
      }
    }
    writer.dump(STR("test_dump/fitter_case" << n_called << "_"
                                  << "iter" << n_iteration << ".vtk"));
  };

  // Write the initial situation.
  write_all(0);

  //
  // Test the derivative
  //
  if(test_derivative)
  {
    Eigen::VectorXd params0 = bzf.get_params();
    Eigen::VectorXd delta = randn(bzf.n_params(), 1);
    double h0 = 1;
    int n_halving = 5;
    Eigen::VectorXd obj;
    Eigen::MatrixXd jac;


    // Test change of variables for fit
    derivtest::run(params0,
        delta,
        [&](const Eigen::VectorXd & new_params) -> Eigen::VectorXd {
          bzf.set_params(new_params);
          return bzf.get_control_points().reshaped(8,1);
        },
        [&](const Eigen::VectorXd & hdelta) -> Eigen::VectorXd  { //
           return bzf.compute_dbezierparams_dfitparams()*hdelta; 
        },
        std::cout,
        n_halving,
        h0);
    
    // Test fit objective
    derivtest::run(params0,
        delta,
        [&](const Eigen::VectorXd & new_params) -> Eigen::VectorXd {
          bzf.set_params(new_params);
          bzf.compute_fitting_objective_and_jacobian(obj,jac);
          return obj;
        },
        [&](const Eigen::VectorXd & hdelta) -> Eigen::VectorXd  { //
           return jac*hdelta; 
        },
        std::cout,
        n_halving,
        h0);
  }

  printf("... running Eigen Levenberg-Marquart \n");
  bzf.run_fitter(true, [&](int i) { write_all(i); });

} // All done with param derivatives
 
}


int test_fitter(int argc, char **argv)
{
    Eigen::Matrix<double, 2, 4> control_points;
    control_points.row(0) << 0, 1, 2, 0;
    control_points.row(1) << 0, 0, 1, 1;
    Eigen::Matrix2Xd points_to_fit(2,4);
    points_to_fit.row(0) << 0+4, 1+4, 2+4, 0+4;
    points_to_fit.row(1) << 0, 0, 1, 1;
    auto feeder = [&](int i, Eigen::Vector2d& pt, double &w){
      w=1; pt=points_to_fit.col(i);
    };
    BezierFitter bzf(feeder, (int)points_to_fit.cols());
    bzf.set_control_points(control_points);
    bzf.tune_optimizer(0.1);
    test_projection(bzf, true);

  return 0;
}